<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mdl_cart extends CI_Model {

    public function __construct()
    {

    }

    public function getCartItemsFromDB($items)
    {
        // code...
        $this->db->where_in('AO.sub_attribute_id', array_keys($items));
        $this->db->join('attribute_options AO', 'AO.sub_attribute_id = sub_attributes.sub_attribute_id');
        $this->db->where_in('option_id', array_values($items));
        $query = $this->db->get('sub_attributes');
        return $query;
    }

    public function createCartFromDBItems($result)
    {
        $cartItems = [];

        // code...
        if (isset($result) && $result->num_rows() > 0 )
        {
            foreach($result->result() as $row)
            {
                $cartItems[] = array(
                    'option_value'=>$row->option_name,
                    'option' => $row->sub_attribute,
                    'price' => !empty($row->discount)?$row->discount: $row->price,
                );
            }
        }

        return $cartItems;

    }

    //retrieves all the names of measurement_ids submitted after customization for the cart
    public function getCustomMeasurementKeysFromDB($custom_measurements)
    {
        $measurement = [];
        if ( count($custom_measurements) > 0 )
        {
            $measurement_ids = array_keys($custom_measurements);
            $this->db->select('measurements.*, mt.measurement_type, units.unit');
            $this->db->join('measurement_types mt', 'mt.measurement_type_id = measurements.measurement_type_id');
            $this->db->join('units', 'units.unit_id = measurements.unit_id');
            $this->db->where_in('measurement_id', $measurement_ids);
            $result = $this->db->get('measurements');
            if ( $result->num_rows() > 0 )
            {
                foreach ( $result->result() as $k => $row)
                {
                    $measurement[] = array(
                        'title' =>  $row->measurement_type,
                        'id' => $row->measurement_id,
                        'value'=> $custom_measurements[$row->measurement_id],
                        'unit' => $row->unit
                    );
                }
                return $measurement;
            }
        }

        return $measurement;
    }

    public function getStandardMeasurement($standard_measurement, $standard_quantity)
    {
        $measurement = [];
        if ( isset($standard_quantity, $standard_measurement) && is_array($standard_quantity) && is_array($standard_measurement))
        {

            foreach ( $standard_quantity as $k => $v )
            {
                $measurement[]= array(
                    'size'=>  $standard_measurement[$k],
                    'quantity' => $v
                );
            }
        }

        return $measurement;
    }


}
