<div class="page-holder d-flex align-items-center">
      <div class="container">
        <div class="row align-items-center py-5">
          <div class="col-5 col-lg-7 mx-auto mb-5 mb-lg-0">
            <div class="pr-lg-5"><img src="<?=base_url()?>assets/img/BG.jpg" alt="" class="img-fluid"></div>
          </div>
          <div class="col-lg-5 px-lg-4">
            <h1 class="text-base text-primary text-uppercase mb-4"> <a href="/">Back to Swift bespoke</a> </h1>
            <h2 class="mb-4">Reset password</h2>
            <p class="text-muted">Please enter email address to reset password</p>

            <?php show_message($this->session->flashdata('success'), $this->session->flashdata('error'))?>

            <?=form_open('auth/reset');?>
              <div class="form-group mb-4">
                <input type="email" name="email" placeholder="Email address" class="form-control border-0 shadow form-control-lg">
              </div>
              <button type="submit" class="btn btn-primary shadow px-5">Reset password</button>
            </form>
          </div>
        </div>
        <p class="mt-5 mb-0 text-gray-400 text-center">Design by <a href="https://swiftbespoke.com" class="external text-gray-400"> </a></p>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)                 -->
      </div>
    </div>
