<section class="py-5" id="app">
    <?= show_message($this->session->flashdata('success'), $this->session->flashdata('error')); ?>
           <div class="row">
               <div class="col-lg-4 mb-5">
                 <div class="card">
                   <div class="card-body">
                     <form id="" @submit.prevent="submit">
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Attribute name </label>
                    <input v-model="Item.attribute" type="text" placeholder="Attribute" class="form-control input-md" required>
                    <input v-model="Item.attribute_id" type="hidden">
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="label-control col-sm-12">Discount </label>
                    <input v-model="Item.discount" type="text" placeholder="0.00" class="form-control input-md" required>
                    </div>

                    <div class="line"></div>
                    <!-- Select Basic -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Category </label>
                        <select id="Select" v-model="Item.category_id" class="form-control custom-select select-2">
                            <option :value="cat.category_id" v-for="cat in categories">{{ cat.category}}</option>
                        </select>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Is Active</label>
                        <select id="Select" v-model="Item.is_active" class="form-control custom-select">
                         <option value="1">Active</option>
                         <option value="0">In Active</option>
                        </select>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                    <button type="submit" name="button" class="btn btn-primary"> submit </button>
                    </div>
                     </form>
                   </div>
                 </div>
               </div>

             <div class="col-lg-8 mb-4">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0"><?= isset($title) ? $title : '' ?></h6>
                 </div>
                 <div class="card-body">
                   <table class="table card-text  table-sm">
                     <thead>
                       <tr>
                         <th>Attribute</th>
                         <th>Discount</th>
                         <th>Category</th>
                         <th>Status</th>
                         <th width="10%">Action</th>
                       </tr>
                     </thead>
                     <tbody>
                    <?php if( !empty($data) && $data->num_rows() > 0 ): $i = 1; foreach($data->result() as $row): ?>
                       <tr>
                         <td><?= isset($row->attribute) ? $row->attribute: '' ?></td>
                         <td><?= isset($row->discount) ? $row->discount: '' ?></td>
                         <td><?= isset($row->category) ? $row->category: '' ?></td>
                         <td><?= $row->is_active > 0 ? '<span class="badge badge-success">active</span>': '<span class="badge badge-danger">inactive</span>' ?></td>
                         <td width="10%">
                             <div class="btn-group btn-sm">
                                 <a href="javascript:void(0);" @click="editItem($event)" data-id="<?= $row->attribute_id ?>" class="btn btn-xs btn-success"><i class="fa fa-pen"></i> </a>
                                 <?php if ($row->is_active) : ?>
                                     <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->attribute_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-times"></i> </a>
                                 <?php else: ?>
                                     <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->attribute_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> </a>
                                <?php endif; ?>
                                    <a href="javascript:void(0);" @click="deleteItem($event)" data-id="<?= $row->attribute_id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                             </div>
                         </td>
                       </tr>
                   <?php endforeach; else: ?>
                       <tr>
                         <td colspan="7"> No attribute exist at the moment </td>
                       </tr>
                   <?php endif;?>
                     </tbody>
                   </table>
                 </div>
               </div>
             </div>
           </div>
         </section>
<script src="<?= base_url() ?>assets/js/vue.min.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript">
var app = new Vue({
    el : '#app',
    data: {
        Item: {
            attribute:'',
            category_id: 0,
            discount: 0,
            is_active: 1
        },
        categories:[]
    },
    created(){
        this.init();
    },
    methods: {
        doAction()
        {
            console.log( this.Item );
        },
        init(){
            app = this;
            axios.get('<?= base_url() ?>/categories/get_json').then((response) => {
                app.categories=response.data.data;
            }).catch(e => console.log(e));
        },
        reset()
        {
            return {
                attribute:'',
                category_id: 0,
                discount: 0,
                is_active: 1
            }
        },

        submit()
        {
            let app = this;
            formRequest  = new FormData();
            console.log(app.Item.category_id);
            formRequest.append('attribute', app.Item.attribute);
            formRequest.append('category_id', app.Item.category_id);
            formRequest.append('is_active', app.Item.is_active);
            formRequest.append('discount', app.Item.discount);

            if ( app.Item.attribute_id > 0 )
            {
                axios.post('<?= site_url() ?>attributes/put_attribute/'+ app.Item.attribute_id, formRequest).then((response) => {
                    console.log(response);
                    $.growl.notice({ 'message': "item was updated successfully."});
                    app.Item = this.reset();
                    location.reload();
                }).catch(e => {
                    console.log(e);
                    $.growl.error({ 'message': "All fields are required"});
                });
            }else{
                axios.post('<?= site_url() ?>attributes/post_attribute', formRequest ).then((response) => {
                    console.log(response);
                    $.growl.notice({ 'message': "item was created successfully."});
                    app.Item = this.reset();
                    location.reload();
                }).catch(e => {
                    console.log(e);
                    $.growl.error({ 'message': "All fields are required"});
                });
            }

        },

        editItem(e)
        {
            let id;
            let app = this;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.get('<?= base_url() ?>attributes/single/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    this.Item = response.data.data;
                }

            }).catch(e => {
                console.log(e)
                $.growl.error({ 'message': "All fields are required"});
            });
        },

        deleteItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>attributes/trash/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was deleted successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        },

        toggleActivation(e)
        {
            let status = $(e.target).closest('tr').children()[3];
            console.log($(status).find('span').text());
            if ( $(status).find('span').text() == 'active' )
            {
                this.deactivateItem(e);
            }else{
                this.activateItem(e);
            }
        },

        deactivateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>attributes/deactivate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[3];
                    $.growl.notice({ 'message': "item was deactivated successfully."});
                    $(badge_parent).find('span').removeClass("badge-success").addClass("badge-danger").text('inactive');
                    $(e.target).closest('tr').children().find('i.fa-times').removeClass('fa-times').addClass('fa-check');
                }
            }).catch(e => console.log(e));
        },

        activateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>attributes/activate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[3];
                    $.growl.notice({ 'message': "item was activated successfully."});
                    $(badge_parent).find('span').removeClass("badge-danger").addClass("badge-success").text('active');
                    $(e.target).closest('tr').children().find('i.fa-check').removeClass('fa-check').addClass('fa-times');
                }
            }).catch(e => console.log(e));
        },

        toggleFeatured(e)
        {
            let status = $(e.target).closest('tr').children()[3];
            if ( $(status).find('span').text() == "Yes" )
            {
                this.unfeatureItem(e);
            }else{
                this.featureItem(e);
            }
        },

        unfeatureItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>attributes/unfeature/'+id).then((response) => {
                 badge_parent = $(e.target).closest('tr').children()[3];
                $.growl.notice({ 'message': "item was unfeatured successfully."});
                $(badge_parent).find('span').removeClass("badge-success").addClass("badge-secondary").text('No');
                $(e.target).closest('tr').children().find('i.fa-thumbs-up').removeClass('fa-thumbs-up').addClass('fa-thumbs-down');

            }).catch(e => console.log(e));
        },

        featureItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>attributes/feature/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[3];
                    console.log(badge_parent)
                    $.growl.notice({ 'message': "item was featured successfully."});
                    $(badge_parent).find('span').removeClass("badge-secondary").addClass("badge-success").text('Yes');
                    $(e.target).closest('tr').children().find('i.fa-thumbs-down').removeClass('fa-thumbs-down').addClass('fa-thumbs-up');
                }
            }).catch(e => console.log(e));
        }
    }
})
</script>
<style>
.select2-container--default .select2-selection--single, .v-select .dropdown-toggle{
    border: 1px solid #ced4da;
    border-radius: 2px;
    height: calc(2.15rem + 2px);
}

.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: calc(2.15rem + 2px);
}

</style>
