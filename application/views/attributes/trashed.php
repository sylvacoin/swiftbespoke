<section class="py-5" id="app">
    <?= show_message($this->session->flashdata('success'), $this->session->flashdata('error')); ?>
           <div class="row">
             <div class="col-lg-12 mb-4">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0"><?= isset($title) ? $title : '' ?></h6>
                 </div>
                 <div class="card-body">
                   <table class="table card-text  table-sm">
                     <thead>
                       <tr>
                         <th>Attribute</th>
                         <th>Discount</th>
                         <th>Category</th>
                         <th>Status</th>
                         <th width="10%">Action</th>
                       </tr>
                     </thead>
                     <tbody>
                    <?php if( !empty($data) && $data->num_rows() > 0 ): $i = 1; foreach($data->result() as $row): ?>
                       <tr>
                         <td><?= isset($row->attribute) ? $row->attribute: '' ?></td>
                         <td><?= isset($row->discount) ? $row->discount: '' ?></td>
                         <td><?= isset($row->category) ? $row->category: '' ?></td>
                         <td><?= $row->is_active > 0 ? '<span class="badge badge-success">active</span>': '<span class="badge badge-danger">inactive</span>' ?></td>
                         <td width="10%">
                             <div class="btn-group btn-sm">
                                 <a href="javascript:void(0);" @click="restoreItem($event)" data-id="<?= $row->attribute_id ?>" class="btn btn-xs btn-success"><i class="fa fa-undo"></i> </a>
                                    <a href="javascript:void(0);" @click="deleteItem($event)" data-id="<?= $row->attribute_id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                             </div>
                         </td>
                       </tr>
                   <?php endforeach; else: ?>
                       <tr>
                         <td colspan="7"> Trash can is clean </td>
                       </tr>
                   <?php endif;?>
                     </tbody>
                   </table>
                 </div>
               </div>
             </div>
           </div>
         </section>
<script src="<?= base_url() ?>assets/js/vue.min.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript">
var app = new Vue({
    el : '#app',
    methods: {

        restoreItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>attributes/restore/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was reverted successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        },

        deleteItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>attributes/delete/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was deleted successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        },

    }
})
</script>
<style>
.select2-container--default .select2-selection--single, .v-select .dropdown-toggle{
    border: 1px solid #ced4da;
    border-radius: 2px;
    height: calc(2.15rem + 2px);
}

.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: calc(2.15rem + 2px);
}

</style>
