
<div class="contact-clean">
    <div class=" container">

        <div class="row">
            <div class="col-lg-6">
                <form method="post" data-aos="fade-up" data-aos-duration="1000">
                    <h2 class="text-center" data-aos="fade-down" data-aos-duration="1000">Contact us</h2>
                    <div class="form-group">
                        <input class="form-control" type="text" name="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="email" name="email" placeholder="Email">
                        <small class="form-text text-danger hidden">Please enter a correct email address.</small>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="14" name="message" placeholder="Message"></textarea>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-primary" type="submit">send Message</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-6">
                <div>
                    <h1 data-aos="fade-right" data-aos-duration="1000">Our mission</h1>
                    <p class="lead" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="150">
                        To implement effective and sustainable projects towards improving the health of pregnant women and children under five years,
                        ultimately creating an enabling environment for the acceleration of zero tolerance to maternal and child
                        mortality in Africa.</p>
                </div>
                <div>
                    <h1 data-aos="fade-right" data-aos-duration="750" data-aos-delay="100">Our vision</h1>
                    <p class="lead" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="250">
                        To create a continent where every woman survives child birth and each child given birth to lives a healthy life.
                    </p>
                </div>
                <a class="text-light bg-danger btn btn-primary" href="<?= base_url() ?>about.html">Learn more</a>
            </div>
        </div>
    </div>

</div>
