
<section id="services" style="padding-top:50px;padding-bottom:50px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div style="text-align:center;">
                    <h2 class="divider-style"><span>About us</span></h2>
                </div>
            </div>
            <div class="col-lg-7">
                <p class="text-left">In view of the foregoing, Africa Save the-Mother and Child Foundation is a non-governmental organization formed by well-meaning patriotic&nbsp;and passionate Nigerians devoted to improving the health of pregnant women and children
                    under five years through promoting the utilization of Maternal Neonatal and Child Health services at various health facilities, supporting also the facilities with trained Skilled Birth Attendants (SBAs) and trained Volunteer
                    Health workers, training Traditional Birth Attendants (TBAs) within our communities on danger signs and good hygienic practices and supporting with some live saving commodities and drugs. Furthermore, Africa Save the mother child
                    work assiduously with communities through Participatory Learning Action (PLA) by volunteering, taking our service&nbsp;promotion and counseling to household level.<br></p>
            </div>
            <div class="col-lg-5"><img src="<?= base_url() ?>assets/img/ASave LOGO Red.png" class="img-fluid d-block"></div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h3 style="font-family:Dosis, sans-serif;font-weight:bold;">Vision</h3>
                <p>To create a continent where every woman survives child birth and each child given birth to lives a healthy life.</p>
            </div>
            <div class="col-lg-4">
                <h3 style="font-family:Dosis, sans-serif;font-weight:bold;">Mission</h3>
                <p>To implement effective and sustainable projects towards improving the health of pregnant women and children under five years, ultimately creating an enabling environment for the acceleration of zero tolerance to maternal and child
                    mortality in Africa.&nbsp;<br></p>
            </div>
            <div class="col-lg-4">
                <h3 style="font-family:Dosis, sans-serif;font-weight:bold;">Goal</h3>
                <p>Contribute to the reduction of maternal and child mortality rate in Africa.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div style="text-align:center;">
                    <h2 class="divider-style"><span>Objectives</span></h2>
                </div>
            </div>
            <div class="col-12 col-lg-12">
                <ul class="mod-list">
                    <li>Awareness creation of the need to take up family commodity by African women of child bearing age.</li>
                    <li>Encourage user friendly family planning services, in terms of removal of user fees.</li>
                    <li>Supporting AfricaSavethe-MCh selected Primary Health Centres (PHC) with Skilled Birth Attendants.</li>
                    <li>Supporting selected PHCs with some essential Live Saving drugs and commodities.</li>
                    <li>Collaborate and train Traditional Birth Attendants (TBA) on live saving skills and good hygienic practices</li>
                    <li>Financial incentives for each TBAs referral </li>
                    <li>Time Targeted Counseling within communities for proper understanding of the role of family members </li>
                    <li>Economic empowerment of indigent women. </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div style="text-align:center;">
                    <h2 class="divider-style"><span>Approach and Strategy</span></h2>
                </div>
            </div>
            <div class="col-12 col-lg-12">
                <ul class="mod-list">
                    <li>Collaborate with schools of Nursing and Midwifery for deployment of graduating students for the mandatory service.</li>
                    <li>Involvement of all stakeholders in the communities; Ward Development Committee (WDC) members, women’s group, religious bodies and traditional rulers for promotion of activities.</li>
                    <li>Capacity building and awareness creation of women of child bearing age.</li>
                    <li>Liaising with various health parastatals in Local and State government for effective collaboration and service delivery.</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div style="text-align:center;">
                    <h2 class="divider-style"><span>Principles</span></h2>
                </div>
            </div>
            <div class="col-12 col-lg-12">
                <ul class="mod-list">
                    <li>Collaborate with schools of Nursing and Midwifery for deployment of graduating students for the mandatory service.</li>
                    <li>Active community participation.</li>
                    <li>Economic empowerment of women.</li>
                    <li>Build/strengthen relationship with State and Local Government.</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div style="text-align:center;">
                    <h2 class="divider-style"><span>Core Values</span></h2>
                </div>
            </div>
            <div class="col-12 col-lg-12">
                <ul class="mod-list">
                    <li>Equity</li>
                    <li>Transparency and accountability</li>
                    <li>Efficiency</li>
                    <li>Integrity</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div style="text-align:center;">
                    <h2 class="divider-style"><span>Core Competences</span></h2>
                </div>
            </div>
            <div class="col-12 col-lg-12">
                <ul class="mod-list">
                    <li>Deploying SBAs to most needed Primary Health Centres</li>
                    <li>Training of TBAs on how to detect danger signs, how and when to refer to facilities and good hygienic practices</li>
                    <li>Training and retraining health personnel’s on Live Saving Skills</li>
                    <li>Counseling of family members on their role in the life of their wives and daughters in having a healthy and safe Sexual and reproductive life.</li>
                    <li>Family planning awareness campaign in the communities.</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">
                <div style="text-align:center;">
                    <h2 class="divider-style"><span>Sources of Funding</span></h2>
                </div>
                <ul class="mod-list">
                    <li>Board members </li>
                    <li>Voluntary contributions from individuals</li>
                    <li>Funding from partner organizations (INGOs &amp; NGOs)</li>
                </ul>
            </div>
            <div class="col-12 col-lg-6">
                <div style="text-align:center;">
                    <h2 class="divider-style"><span>Key Resources</span></h2>
                </div>
                <ul class="mod-list">
                    <li>Skilled Manpowers</li>
                    <li>Adequate volunteer base</li>
                    <li>Partners
                        <ul>
                            <li>Rekindle Hope Initiative</li>
                            <li>School of Midwifery Asaba, Delta State</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
