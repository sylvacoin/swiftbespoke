<section class="py-5" id="app">
    <?= show_message($this->session->flashdata('success'), $this->session->flashdata('error')); ?>
           <div class="row">
               <div class="col-lg-4 mb-5">
                 <div class="card">
                   <div class="card-body">
                     <form id="" @submit.prevent="submit">
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Option name </label>
                    <input v-model="Item.option_name" type="text" placeholder="Option name" class="form-control input-md" required>
                    <input v-model="Item.option_id" type="hidden">
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="label-control col-sm-12">Description </label>
                        <textarea v-model="Item.description" rows="8" class="form-control input-md" cols="80"></textarea>

                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <image-input  v-model="Image.thumb" name="file">
                            <div slot="activator">
                                <div class="card w-50">
                                    <div class="card-body" v-if="!Image.thumb" style="width:100%; height:100%; padding: 0;" >
                                        <img class="img-fluid" src="<?= base_url() ?>assets/img/placeholder.jpg" alt="Alternate Text" />
                                    </div>
                                    <div class="card-image" v-else>
                                        <img class="img-fluid" :src="Image.thumb.imageURL" alt="Alternate Text" />
                                    </div>
                                </div>
                                <small>Option Image</small>
                            </div>
                        </image-input>
                    </div>

                    <div class="line"></div>


                    <div class="form-group row">
                        <label class="label-control col-sm-12">Price </label>
                    <input v-model.number="Item.price" type="number" placeholder="0.00" class="form-control input-md" required>
                    </div>

                    <div class="line"></div>
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Discount Price</label>
                    <input v-model.number="Item.discount" type="number" placeholder="0.00" class="form-control input-md" required>
                    </div>

                    <div class="line"></div>
                    <!-- Select Basic -->
                    <div class="form-group row" v-if="Item.pocket_side_display == 1">
                        <div class="col-sm-6">
                            <image-input  v-model="Image.left" name="file">
                                <div slot="activator">
                                    <div class="card w-100">
                                        <div class="card-body" v-if="!Image.left" style="width:100%; height:100%; padding: 0;" >
                                            <img class="img-fluid" src="<?= base_url() ?>assets/img/placeholder.jpg" alt="Alternate Text" />
                                        </div>
                                        <div class="card-image" v-else>
                                            <img class="img-fluid" :src="Image.left.imageURL" alt="Alternate Text" />
                                        </div>
                                    </div>
                                    <small>Left Pocket</small>
                                </div>
                            </image-input>
                        </div>
                        <div class="col-sm-6">
                            <image-input  v-model="Image.right" name="file">
                                <div slot="activator">
                                    <div class="card w-100">
                                        <div class="card-body" v-if="!Image.right" style="width:100%; height:100%; padding: 0;" >
                                            <img class="img-fluid" src="<?= base_url() ?>assets/img/placeholder.jpg" alt="Alternate Text" />
                                        </div>
                                        <div class="card-image" v-else>
                                            <img class="img-fluid" :src="Image.right.imageURL" alt="Alternate Text" />
                                        </div>
                                    </div>
                                    <small>Right Pocket</small>
                                </div>
                            </image-input>
                        </div>
                        <div class="col-sm-6">
                            <image-input  v-model="Image.both" name="file">
                                <div slot="activator">
                                    <div class="card w-100">
                                        <div class="card-body" v-if="!Image.both" style="width:100%; height:100%; padding: 0;" >
                                            <img class="img-fluid" src="<?= base_url() ?>assets/img/placeholder.jpg" alt="Alternate Text" />
                                        </div>
                                        <div class="card-image" v-else>
                                            <img class="img-fluid" :src="Image.both.imageURL" alt="Alternate Text" />
                                        </div>
                                    </div>
                                    <small>Both Pocket</small>
                                </div>
                            </image-input>
                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group row" v-else >
                            <image-input  v-model="Image.background" name="file">
                                <div slot="activator">
                                    <div class="card w-50">
                                        <div class="card-body" v-if="!Image.background" style="width:100%; height:100%; padding: 0;" >
                                            <img class="img-fluid" src="<?= base_url() ?>assets/img/placeholder.jpg" alt="Alternate Text" />
                                        </div>
                                        <div class="card-image" v-else>
                                            <img class="img-fluid" :src="Image.background.imageURL" alt="Alternate Text" />
                                        </div>
                                    </div>
                                    <small>Option Background Image</small>
                                </div>
                            </image-input>
                    </div>
                    <div class="line"></div>
                    <!-- Select Basic -->
                    <!-- Select Basic -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Is Active</label>
                        <select id="Select" v-model="Item.is_active" class="form-control custom-select">
                         <option value="1">Active</option>
                         <option value="0">In Active</option>
                        </select>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                    <button type="submit" name="button" class="btn btn-primary"> submit </button>
                    </div>
                     </form>
                   </div>
                 </div>
               </div>

             <div class="col-lg-8 mb-4">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0"><?= isset($title) ? $title : '' ?></h6>
                 </div>
                 <div class="card-body">
                   <table class="table card-text  table-sm">
                     <thead>
                       <tr>
                         <th>thumbnails</th>
                         <th>option BG Image</th>
                         <th>Option Name</th>
                         <th>Price</th>
                         <th>Status</th>
                         <th width="10%">Action</th>
                       </tr>
                     </thead>
                     <tbody>
                    <?php if( !empty($data) && $data->num_rows() > 0 ): $i = 1; foreach($data->result() as $row): ?>
                       <tr>
                         <td><img src="<?= base_url(). (!empty($row->image_thumb) ? $row->image_thumb: $row->image_background) ?>" width="40" height="40"/></td>
                         <td><img src="<?= base_url(). (!empty($row->image_background) ? $row->image_background: $row->image_background) ?>" width="60" height="60"/></td>
                         <td><?= isset($row->option_name) ? $row->option_name: '' ?></td>
                         <td><?= isset($row->price) ? $row->price: '' ?></td>
                         <td><?= $row->is_active > 0 ? '<span class="badge badge-success">active</span>': '<span class="badge badge-danger">inactive</span>' ?></td>
                         <td width="10%">
                             <div class="btn-group btn-sm">
                                 <a href="javascript:void(0);" @click="editItem($event)" data-id="<?= $row->option_id ?>" class="btn btn-xs btn-success"><i class="fa fa-pen"></i> </a>
                                 <?php if ($row->is_active) : ?>
                                     <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->option_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-times"></i> </a>
                                 <?php else: ?>
                                     <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->option_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> </a>
                                <?php endif; ?>
                                    <a href="javascript:void(0);" @click="deleteItem($event)" data-id="<?= $row->option_id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                             </div>
                         </td>
                       </tr>
                   <?php endforeach; else: ?>
                       <tr>
                         <td colspan="7"> No option exist at the moment </td>
                       </tr>
                   <?php endif;?>
                     </tbody>
                   </table>
                 </div>
               </div>
             </div>
           </div>
         </section>
<script src="<?= base_url() ?>assets/js/vue.min.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="<?= base_url() ?>assets/js/image-input.js" charset="utf-8"></script>
<script type="text/javascript">
var app = new Vue({
    el : '#app',
    data: {
        Item: {
            option_name:'',
            sub_attribute_id: <?= isset($sub_attr_id) ? $sub_attr_id: 0 ?>,
            option_id: 0,
            discount: 0,
            description: "",
            pocket_side_display: <?= isset($pocket_side_display) ? $pocket_side_display: 0 ?>,
            price: 0,
            is_active: 1
        },
        Image: {
            left:null,
            right:null,
            both:null,
            background: null,
            thumb: null
        }
    },
    created(){
        this.init();
    },
    methods: {
        doAction()
        {
            console.log( this.Item );
        },
        init(){
            app = this;
            axios.get('<?= base_url() ?>/attributes/get_json').then((response) => {

            }).catch(e => console.log(e));
        },
        reset()
        {
            return {
                option_name:'',
                option_id:0,
                sub_attribute_id: <?= isset($sub_attr_id) ? $sub_attr_id: 0 ?>,
                discount: 0,
                description: "",
                pocket_side_display: <?= isset($pocket_side_display) ? $pocket_side_display: 0 ?>,
                price: 0,
                is_active: 1
            }
        },
        resetImage()
        {
            return {
                left:null,
                right:null,
                both:null,
                background: null,
                thumb: null
            }
        },
        submit()
        {
            let app = this;
            formRequest  = new FormData();

            formRequest.append('option_name', app.Item.option_name);
            formRequest.append('sub_attribute_id', app.Item.sub_attribute_id);
            formRequest.append('description', app.Item.description);
            formRequest.append('is_active', app.Item.is_active);
            formRequest.append('discount', app.Item.discount);
            formRequest.append('price', app.Item.price);
            formRequest.append('pocket_side_display', app.Item.pocket_side_display);

            if ( app.Image.left)
            {
                formRequest.append('image_left', app.Image.left.imageFile);
            }
            if (app.Image.right)
            {
                formRequest.append('image_right', app.Image.right.imageFile);
            }
            if (app.Image.both)
            {
                formRequest.append('image_both', app.Image.both.imageFile);
            }
            if (app.Image.background)
            {
                formRequest.append('image_background', app.Image.background.imageFile);
            }
            if (app.Image.thumb)
            {
                formRequest.append('image_thumb', app.Image.thumb.imageFile);
            }


            if ( app.Item.option_id > 0 )
            {
                axios.post('<?= site_url() ?>AttributeOptions/put_option/'+ app.Item.option_id, formRequest).then((response) => {
                    if (response.status == 200)
                    {
                        console.log(response);
                        $.growl.notice({ 'message': "item was updated successfully."});
                        app.Item = this.reset();
                        app.Image = this.resetImage();
                        location.reload();
                    }
                }).catch(e => {
                    console.log(e);
                    $.growl.error({ 'message': "All fields are required"});
                });
            }else{
                axios.post('<?= site_url() ?>AttributeOptions/post_option', formRequest ).then((response) => {
                    if ( response.status == 201 )
                    {
                        console.log(response);
                        $.growl.notice({ 'message': "item was created successfully."});
                        app.Item = this.reset();
                        app.Image = this.resetImage();
                        location.reload();
                    }
                }).catch(e => {
                    console.log(e);
                    $.growl.error({ 'message': "All fields are required"});
                });
            }

        },

        editItem(e)
        {
            let id;
            let app = this;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            var baseURL = '<?= base_url() ?>';

            axios.get( baseURL+'AttributeOptions/single/'+id+'/'+app.Item.pocket_side_display).then((response) => {
                if ( response.status == 200 )
                {
                    app.Item = response.data.data;
                    app.Image = {
                        left: {
                            imageURL:baseURL+response.data.data.image_left,
                        },
                        right: {
                            imageURL:baseURL+response.data.data.image_right,
                        },
                        both: {
                            imageURL:baseURL+response.data.data.image_both,
                        },
                        background: {
                            imageURL:baseURL+response.data.data.image_background,
                        },
                        thumb: {
                            imageURL:baseURL+response.data.data.image_thumb,
                        }
                    }
                }

            }).catch(e => {
                console.log(e)
                $.growl.error({ 'message': "All fields are required"});
            });
        },

        deleteItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>AttributeOptions/trash/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was deleted successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        },

        toggleActivation(e)
        {
            let status = $(e.target).closest('tr').children()[4];
            console.log($(status).find('span').text());
            if ( $(status).find('span').text() == 'active' )
            {
                this.deactivateItem(e);
            }else{
                this.activateItem(e);
            }
        },

        deactivateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>AttributeOptions/deactivate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[4];
                    $.growl.notice({ 'message': "item was deactivated successfully."});
                    $(badge_parent).find('span').removeClass("badge-success").addClass("badge-danger").text('inactive');
                    $(e.target).closest('tr').children().find('i.fa-times').removeClass('fa-times').addClass('fa-check');
                }
            }).catch(e => console.log(e));
        },

        activateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>AttributeOptions/activate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[4];
                    $.growl.notice({ 'message': "item was activated successfully."});
                    $(badge_parent).find('span').removeClass("badge-danger").addClass("badge-success").text('active');
                    $(e.target).closest('tr').children().find('i.fa-check').removeClass('fa-check').addClass('fa-times');
                }
            }).catch(e => console.log(e));
        },

        toggleFeatured(e)
        {
            let status = $(e.target).closest('tr').children()[3];
            if ( $(status).find('span').text() == "Yes" )
            {
                this.unfeatureItem(e);
            }else{
                this.featureItem(e);
            }
        },

        unfeatureItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>AttributeOptions/unfeature/'+id).then((response) => {
                 badge_parent = $(e.target).closest('tr').children()[4];
                $.growl.notice({ 'message': "item was unfeatured successfully."});
                $(badge_parent).find('span').removeClass("badge-success").addClass("badge-secondary").text('No');
                $(e.target).closest('tr').children().find('i.fa-thumbs-up').removeClass('fa-thumbs-up').addClass('fa-thumbs-down');

            }).catch(e => console.log(e));
        },

        featureItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>AttributeOptions/feature/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[4];
                    console.log(badge_parent)
                    $.growl.notice({ 'message': "item was featured successfully."});
                    $(badge_parent).find('span').removeClass("badge-secondary").addClass("badge-success").text('Yes');
                    $(e.target).closest('tr').children().find('i.fa-thumbs-down').removeClass('fa-thumbs-down').addClass('fa-thumbs-up');
                }
            }).catch(e => console.log(e));
        }
    }
})
</script>
<style>
.select2-container--default .select2-selection--single, .v-select .dropdown-toggle{
    border: 1px solid #ced4da;
    border-radius: 2px;
    height: calc(2.15rem + 2px);
}

.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: calc(2.15rem + 2px);
}

</style>
