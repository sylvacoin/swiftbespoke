<section class="py-5">
           <div class="row">
             <div class="col-lg-12">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0">Mail template list</h6>
                 </div>
                 <div class="card-body">
                   <table class="table table-striped table-sm card-text">
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>Subject</th>
                         <th>Action </th>
                         <th>Options</th>
                       </tr>
                     </thead>
                     <tbody>

                    <?php if ( !empty($data) && $data->num_rows() > 0 ): foreach( $data->result() as $row): ?>
                      <tr>
                         <th scope="row">6</th>
                         <td> <?= isset($row->subject)?$row->subject:'' ?></td>
                         <td><?= isset($row->action)?$row->action:'' ?></td>
                         <td>
                             <?= anchor('mailer/modify/'.$row->mail_id, 'modify', 'class="btn btn-success btn-xs"') ?>
                             <?= anchor('mailer/view/'.$row->mail_id, 'view', 'class="btn btn-primary btn-xs"') ?>
                         </td>
                       </tr>
                   <?php endforeach; else: ?>
                       <tr>
                          <th scope="row" colspan="4">
                              <p> No mail template has been added</p>
                          </th>
                        </tr>
                   <?php endif; ?>
                     </tbody>
                   </table>
                 </div>
               </div>
             </div>
           </div>
         </section>
