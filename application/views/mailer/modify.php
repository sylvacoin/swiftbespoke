<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/summernote-bs3.css" rel="stylesheet">

<section class="py-5">
            <div class="row">
            <?= validation_errors('<div class="alert alert-danger">','</div>') ?>
              <!-- Form Elements -->
              <div class="col-lg-12 mb-5">
                <div class="card">
                  <div class="card-header">
                    <h3 class="h6 text-uppercase mb-0">Mail Editor</h3>
                  </div>
                  <div class="card-body">
                    <?= form_open('mailer/submit/'.$this->uri->segment(3), 'class="form-horizontal"') ?>
                      <div class="form-group row">
                        <label class="col-md-3 form-control-label">Mail subject</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="subject" value="<?= isset($data->subject)?$data->subject:'' ?>">
                        </div>
                      </div>
                      <div class="line"></div>
                      <div class="form-group row">
                        <label class="col-md-3 form-control-label">Mail Content</label>
                        <div class="col-md-9">
                          <small class="form-text text-muted ml-3">A block of help text that breaks onto a new line and may extend beyond one line.</small>
                          <textarea name="body" id="summernote"><?= isset($data->body)?$data->body:'' ?></textarea>
                        </div>
                      </div>
                      <div class="line"></div>

                      <div class="form-group row">
                        <label class="col-md-3 form-control-label">Key word mappings</label>
                        <div class="col-md-9">
                          <textarea name="keyword_mapping" rows="8" cols="80" class="form-control" disabled><?= isset($data->keyword_mapping) ?$data->keyword_mapping:'' ?></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-md-9 ml-auto">
                          <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
