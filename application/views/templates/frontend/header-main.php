<!DOCTYPE html>
<html lang="en">

<head>
     <title>Home</title>
     <!-- Required meta tags -->
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes" />

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
     <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css" />
     <link rel="stylesheet" href="<?= base_url() ?>assets/css/grid.css">
     <link rel="stylesheet" href="<?= base_url() ?>assets/css/responsive.css">
     <link rel="stylesheet" href="<?=base_url()?>assets/css/basket.css">
     <script src="<?= base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
     <script type="text/javascript">
         var base_url = '<?= base_url() ?>';
     </script>
</head>

<body>
     <div class="wrapper">
          <!-- navigation -->
          <?php $this->load->view('Templates/frontend/navigation') ?>
          <!-- end navigation -->
          <!-- Page Content -->
          <div id="content">
               <header>
                    <nav class="navbar navbar-expand-md navbar-light bg-orange bg-faded d-none d-md-block">
                         <a class="navbar-brand d-md-none" href="index.html">
                              <img src="<?= base_url() ?>assets/img/logo.png" alt="logo" />
                         </a>
                         <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                              <span class="navbar-toggler-icon"></span>
                         </button>
                         <div class="d-md-flex d-block w-100">
                              <div class="collapse navbar-collapse mx-auto w-auto justify-content-center" id="navbarNavAltMarkup">
                                   <div class="navbar-nav">
                                        <a class="nav-item nav-link active" href="<?= site_url('product-categories') ?>">Men's Garment <span class="sr-only">(current)</span></a>
                                        <a class="nav-item nav-link" href="<?= site_url('about') ?>">Women's Garment</a>
                                        <a class="nav-item nav-link" href="<?= site_url('reviews') ?>">About us</a>
                                        <a class="navbar-brand mx-2 d-none d-md-inline" href="<?= site_url('') ?>"><img src="<?= base_url() ?>assets/img/logo.png" alt="logo" /></a>
                                        <a class="nav-item nav-link" href="<?= site_url('guarantee') ?>">Our Gurantee</a>
                                        <a class="nav-item nav-link" href="<?= site_url('faq') ?>">FAQ</a>
                                        <a class="nav-item nav-link" href="<?= site_url('contact-us') ?>">Contact us</a>
                                   </div>
                              </div>
                         </div>
                    </nav>
                    <a href="javascript:void(0)" id="sidebarCollapse" class="sticky-button top left">
                         <img src="<?= base_url() ?>assets/img/menu.png" alt="menu" class="menu-button" />
                         <span>Menu</span>
                    </a>
                    <div class="container d-flex h-100 hero">
                         <div class="row w-100">
                              <div class="col-12 col-md-6 hero-content align-self-center" style="color: #ffffff">
                                   <h1>Luxury Shirts</h1>
                                   <h4>Made for you... by you</h4>
                                   <button class="btn btn-primary bg-orange btn-lg">
                                        DESIGN YOUR SHIRT
                                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                   </button>
                              </div>
                         </div>
                    </div>
               </header>
