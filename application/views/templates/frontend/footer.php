<footer>
                    <div class="container">
                         <div class="row">
                              <div class="col-2_5">
                                   <h4>Shop</h4>
                                   <ul class="nav flex-column">
                                        <li class="nav-item"><a href="" class="nav-link">Suits</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Shirts</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Jackets</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Pants</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Vests</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Overcoats</a></li>
                                   </ul>
                              </div>
                              <div class="col-2_5">
                                   <h4>Design</h4>
                                   <ul class="nav flex-column">
                                        <li class="nav-item"><a href="" class="nav-link">Suits</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Shirts</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Jackets</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Pants</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Vests</a></li>
                                   </ul>
                              </div>
                              <div class="col-2_5">
                                   <h4>About</h4>
                                   <ul class="nav flex-column">
                                        <li class="nav-item"><a href="" class="nav-link">How it works</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Perfect fit guarantee</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Our fabrics</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">About</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Careers</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Privacy policy</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Terms and conditions</a></li>
                                   </ul>
                              </div>
                              <div class="col-2_5">
                                   <h4>Support</h4>
                                   <ul class="nav flex-column">
                                        <li class="nav-item"><a href="" class="nav-link">Help / FAQ</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Shipping</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Track your order</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Re-order an item</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Alterations</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Remakes</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Returns</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Book a fitting</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Weddings</a></li>
                                   </ul>
                              </div>
                              <div class="col-2_5">
                                   <h4>Showrooms</h4>
                                   <ul class="nav flex-column">
                                        <li class="nav-item"><a href="" class="nav-link">Kaduna</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Port Harcourt</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Lagos</a></li>
                                        <li class="nav-item"><a href="" class="nav-link">Abuja</a></li>
                                   </ul>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-12">
                                   <h4>Customer Support</h4>
                                   <p>Get in touch and talk with us</p>
                              </div>
                              <div class="col-12">
                                   <h4><i class="fa fa-phone" aria-hidden="true"></i> <span> +2348166897526</span></h4>
                                   <p>MON - FRI 9AM - 5pm GMT</p>
                              </div>
                         </div>
                    </div>
               </footer>
          </div>
          <?php if($this->uri->segment(1) != 'cart'){require('basket.php');}  ?>

          <!-- Dark Overlay element -->

          <div class="overlay"></div>
     </div>
     <script src="<?= base_url() ?>assets/vendor/popper.js/umd/popper.min.js"> </script>
     <script src="<?= base_url() ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
     <?php if($this->uri->segment(1) != 'cart'): ?>
     <script src="<?= base_url() ?>assets/js/basket.js"></script>
 <?php endif ?>
     <!-- Optional JavaScript -->
     <!-- jQuery first, then Popper.js, then Bootstrap JS -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>
     <script src="<?= base_url() ?>./assets/js/script.js"></script>
</body>

</html>
