<?php

if ($this->uri->segment(1) == 'home' || $this->uri->segment(1) == NULL) {
    $this->load->view('Templates/frontend/header-main');
} else {
    $this->load->view('Templates/frontend/header');
}


if (!isset($viewFile)) {
    $viewFile = "";
}

if (!isset($body)) {
    $body = NULL;
}

if ($viewFile != "") {
    $this->load->view($viewFile, $body);
} else {
    echo nl2br("Houston we have a problem. You omitted a viewFile");
}

$this->load->view('Templates/frontend/footer');
