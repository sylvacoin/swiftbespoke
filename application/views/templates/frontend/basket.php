<div class="cd-cart-container empty">
	<a href="#0" class="cd-cart-trigger">
		<ul class="count"> <!-- cart items count -->
			<li>0</li>
			<li>0</li>
		</ul> <!-- .count -->
	</a>

	<div class="cd-cart">
		<div class="wrapper">
			<div class="cart-header">
				<h2>Cart</h2>
				<span class="undo">Item removed. <a href="#0">Undo</a></span>
			</div>

			<div class="body">
				<ul>
					<!-- products added to the cart will be inserted here using JavaScript -->
				</ul>
			</div>

			<div class="cart-footer">
				<a href="<?= site_url('cart') ?>" class="checkout btn"><em>Checkout - $<span>0</span></em></a>
			</div>
		</div>
	</div> <!-- .cd-cart -->
</div> <!-- cd-cart-container -->
