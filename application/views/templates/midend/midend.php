<?php $this->load->view('Templates/midend/header')?>
<?php
if (!isset($viewFile)) {
	$viewFile = "";
}

if (!isset($body)) {
	$body = NULL;
}

if ($viewFile != "") {
	$this->load->view($viewFile, $body);
} else {
	echo nl2br("Houston we have a problem. You omitted a viewFile");
}
?>

<?php $this->load->view('Templates/midend/footer')?>