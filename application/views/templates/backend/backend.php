<?php $this->load->view('Templates/backend/header') ?>
<div class="d-flex align-items-stretch">
<!-- Sidebarstart -->
<?php $this->load->view('Templates/backend/sidebar') ?>
<!-- sidebar end -->
<div class="page-holder w-100 d-flex flex-wrap">
<div class="container-fluid px-xl-5">
	<?php
  if (!isset($viewFile)) {
    $viewFile = "";
}

if (!isset($body)) {
    $body = NULL;
}

if ($viewFile != "" ) {
    $this->load->view($viewFile, $body);
} else {
    echo nl2br("Houston we have a problem. You omitted a viewFile");
}
?>
</div>
<?php $this->load->view('Templates/backend/footer') ?>