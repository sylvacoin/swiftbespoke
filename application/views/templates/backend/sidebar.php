<div id="sidebar" class="sidebar py-3">
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item"><a href="<?= site_url('dashboard') ?>" class="sidebar-link text-muted  <?= set_active('dashboard') ?>"><i class="o-home-1 mr-3 text-gray"></i><span>Home</span></a></li>
              <!-- <li class="sidebar-list-item"><a href="charts.html" class="sidebar-link text-muted"><i class="o-sales-up-1 mr-3 text-gray"></i><span>Charts</span></a></li>

               -->

              <li class="sidebar-list-item"><a href="<?= site_url('mailer') ?>" class="sidebar-link <?= set_active('mailer/') ?>"><i class="o-survey-1 mr-3 text-gray"></i><span>Mails</span></a></li>
              <li class="sidebar-list-item"><a href="#" data-toggle="collapse" data-target="#products" aria-expanded="false" aria-controls="categories" class="sidebar-link text-muted <?= set_active(['products','attributes','sub-attributes']) ?>"><i class="fa fa-3x flaticon-rating mr-3 text-gray"></i><span>Products</span></a>
                <div id="products" class="collapse">
                  <ul class="sidebar-menu list-unstyled border-left border-primary border-thick">
                    <li class="sidebar-list-item"><a href="<?= site_url('products') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('products/') ?>">Manage Products</a></li>
                    <li class="sidebar-list-item"><a href="<?= site_url('products/trash') ?>" class="sidebar-link text-muted pl-lg-5  <?= set_active('products/trash', 1) ?>">Trashed Products</a></li>
                    <li class="sidebar-list-item"><a href="<?= site_url('attributes') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('attributes/') ?>">Product Attributes</a></li>
                    <li class="sidebar-list-item"><a href="<?= site_url('attributes/trash') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('attributes/trash') ?>">Trashed Attributes</a></li>
                    <li class="sidebar-list-item"><a href="<?= site_url('sub-attributes') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('sub-attributes/') ?>">Sub Attributes</a></li>
                    <li class="sidebar-list-item"><a href="<?= site_url('sub-attributes/trash') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('sub-attributes/trash') ?>">Trashed Sub Attributes</a></li>
                  </ul>
                </div>
              </li>
              <li class="sidebar-list-item"><a href="#" data-toggle="collapse" data-target="#categories" aria-expanded="false" aria-controls="categories" class="sidebar-link text-muted <?= set_active(['categories']) ?>"><i class="fa fa-3x flaticon-rating mr-3 text-gray"></i><span>Categories</span></a>
                <div id="categories" class="collapse">
                  <ul class="sidebar-menu list-unstyled border-left border-primary border-thick">
                    <li class="sidebar-list-item"><a href="<?= site_url('categories') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('categories/') ?>">Manage Categories</a></li>
                    <li class="sidebar-list-item"><a href="<?= site_url('categories/trash') ?>" class="sidebar-link text-muted pl-lg-5  <?= set_active('categories/trash', 1) ?>">Trashed Categories</a></li>
                  </ul>
                </div>
              </li>
              <li class="sidebar-list-item"><a href="<?= site_url('orders') ?>" class="sidebar-link text-muted <?= set_active('orders/') ?>"><i class="o-table-content-1 mr-3 text-gray"></i><span>Orders</span></a></li>
              <li class="sidebar-list-item"><a href="#" data-toggle="collapse" data-target="#measurements" aria-expanded="false" aria-controls="measurements" class="sidebar-link text-muted <?= set_active(['measurementtypes','units']) ?>"><i class="fa fa-3x flaticon-rating mr-3 text-gray"></i><span>Measurements</span></a>
                <div id="measurements" class="collapse">
                  <ul class="sidebar-menu list-unstyled border-left border-primary border-thick">
                    <li class="sidebar-list-item"><a href="<?= site_url('measurementtypes') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('measurementtypes/') ?>">Measurement Types</a></li>
                    <li class="sidebar-list-item"><a href="<?= site_url('measurementtypes/trash') ?>" class="sidebar-link text-muted pl-lg-5  <?= set_active('measurementtypes/trash', 1) ?>">Trashed Types</a></li>
                    <li class="sidebar-list-item"><a href="<?= site_url('units') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('units/') ?>">Manage Units</a></li>
                    <li class="sidebar-list-item"><a href="<?= site_url('units/trash') ?>" class="sidebar-link text-muted pl-lg-5  <?= set_active('units/trash', 1) ?>">Trashed Units</a></li>
                  </ul>
                </div>
              </li>
              <li class="sidebar-list-item"><a href="#" data-toggle="collapse" data-target="#styles" aria-expanded="false" aria-controls="styles" class="sidebar-link text-muted <?= set_active(['styles']) ?>"><i class="fa fa-3x flaticon-clothes mr-3 text-gray"></i><span>Styles</span></a>
                <div id="styles" class="collapse">
                  <ul class="sidebar-menu list-unstyled border-left border-primary border-thick">
                    <li class="sidebar-list-item"><a href="<?= site_url('styles') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('styles/') ?>">Manage Styles</a></li>
                    <li class="sidebar-list-item"><a href="<?= site_url('styles/trash') ?>" class="sidebar-link text-muted pl-lg-5  <?= set_active('styles/trash', 1) ?>">Trash</a></li>
                  </ul>
                </div>
              </li>
          <li class="sidebar-list-item"><a href="#" data-toggle="collapse" data-target="#pages" aria-expanded="false" aria-controls="pages" class="sidebar-link text-muted <?= set_active(['users','user']) ?>"><i class="fa fa-3x flaticon-rating mr-3 text-gray"></i><span>Users</span></a>
            <div id="pages" class="collapse">
              <ul class="sidebar-menu list-unstyled border-left border-primary border-thick">
                <li class="sidebar-list-item"><a href="<?= site_url('users/') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('users/') ?>">Manage Users</a></li>
                <li class="sidebar-list-item"><a href="<?= site_url('users/appointments') ?>" class="sidebar-link text-muted pl-lg-5  <?= set_active('users/appointments') ?>">Appointment Requests</a></li>
                <li class="sidebar-list-item"><a href="<?= site_url('users/administrators') ?>" class="sidebar-link text-muted pl-lg-5 <?= set_active('users/administrators') ?>">Manage Administrators</a></li>
              </ul>
            </div>
          </li>
              <li class="sidebar-list-item"><a href="<?= site_url('logout') ?>" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Log out</span></a></li>
        </ul>
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">EXTRAS</div>
        <!-- <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item"><a href="#" class="sidebar-link text-muted"><i class="o-database-1 mr-3 text-gray"></i><span>Demo</span></a></li>
              <li class="sidebar-list-item"><a href="#" class="sidebar-link text-muted"><i class="o-imac-screen-1 mr-3 text-gray"></i><span>Demo</span></a></li>
              <li class="sidebar-list-item"><a href="#" class="sidebar-link text-muted"><i class="o-paperwork-1 mr-3 text-gray"></i><span>Demo</span></a></li>
              <li class="sidebar-list-item"><a href="#" class="sidebar-link text-muted"><i class="o-wireframe-1 mr-3 text-gray"></i><span>Demo</span></a></li>
        </ul> -->
  </div>
