<section class="py-5">
    <?= show_message($this->session->flashdata('success'), $this->session->flashdata('error')); ?>
           <div class="row">
             <div class="col-lg-12 mb-4">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0"><?= isset($title)?$title:'' ?></h6>
                 </div>
                 <div class="card-body">
                   <table class="table card-text  table-sm">
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>Full Name</th>
                         <th>Email Address</th>
                         <th>Phone no.</th>
                         <th>Joined on</th>
                         <th>Account</th>
                          <th>status</th>
                         <th>Action</th>
                       </tr>
                     </thead>
                     <tbody>
                    <?php if( !empty($data) && $data->num_rows() > 0 ): foreach($data->result() as $row): ?>
                       <tr>
                         <th scope="row">1</th>
                         <td><?= isset($row->firstname) ? $row->firstname: '' ?> <?= isset($row->lastname) ? $row->lastname: '' ?></td>
                         <td><?= isset($row->email) ? $row->email: '' ?></td>
                         <td><?= isset($row->phone) ? $row->phone: '' ?></td>
                         <td><?= isset($row->created) ? $row->created: '' ?></td>
                         <td><?= $row->token == 'active' ? '<span class="badge badge-success">active</span>': '<span class="badge badge-secondary">inactive</span>'.anchor('#', 'activate user', 'class="btn btn-success btn-xs"') ?></td>
                         <td><?= $row->is_flagged ? '<span class="badge badge-success">flagged</span>': '<span class="badge badge-secondary">not flagged</span>' ?></td>
                         <td width="10%"">
                             <div class="btn-group btn-sm">
                                 <?= anchor('users/details/'.$row->user_id, '<i class="fa fa-eye"></i>', 'class="btn btn-xs btn-primary"') ?>
                                 <?php if ($row->is_flagged) : ?>
                                     <?= anchor('users/unflag/'.$row->user_id, '<i class="fa fa-check"></i>', 'class="btn btn-xs btn-primary"') ?>
                                 <?php else: ?>
                                     <?= anchor('users/flag/'.$row->user_id, '<i class="fa fa-times"></i>', 'class="btn btn-xs btn-primary"') ?>
                                <?php endif; ?>
                                 <?= anchor('users/delete/'.$row->user_id, '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-primary"') ?>
                             </div>
                         </td>
                       </tr>
                   <?php endforeach; else: ?>
                       <tr>
                         <td colspan="7"> No users exist at the moment </td>
                       </tr>
                   <?php endif;?>
                     </tbody>
                   </table>
                 </div>
               </div>
             </div>
           </div>
         </section>
