<section class="py-5">
    <div class="container">
    <div class="row m-y-2">
        <div class="col-lg-8 push-lg-4">
            <div class="card profile-card">
                      <div class="card-header">
                          <ul class="nav nav-tabs">
                              <li class="nav-item">
                                  <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
                              </li>
                              <li class="nav-item">
                                  <a href="" data-target="#messages" data-toggle="tab" class="nav-link">Appointments</a>
                              </li>
                              <!-- <li class="nav-item">
                                  <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Edit</a>
                              </li> -->
                          </ul>

                      </div>
                      <div class="card-body">
                    <div class="tab-content p-b-3">
                        <div class="tab-pane active" id="profile">
                            <h4 class="m-y-2">User Profile</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <h6>Email</h6>
                                    <p>
                                        <?= isset($user->email)? $user->email : '' ?>
                                    </p>

                                </div>
                                <div class="col-sm-6">
                                    <h6>Phone</h6>
                                    <p>
                                        <?= isset($user->phone)? $user->phone : '' ?>
                                    </p>
                                    <span class="badge badge-primary badge-md"> <i class="fa fa-user"></i> active</span>
                                    <span class="badge badge-success badge-md"><i class="fa fa-cog"></i> 43 Total Orders</span>
                                    <span class="badge badge-secondary badge-md"><i class="fa fa-eye"></i> 245 Appointments</span>
                                </div>
                                <!-- <div class="col-md-6">
                                    <h6>Recent Orders</h6>
                                    <a href="" class="tag tag-default tag-pill">html5</a>
                                    <a href="" class="tag tag-default tag-pill">react</a>
                                    <a href="" class="tag tag-default tag-pill">codeply</a>
                                    <a href="" class="tag tag-default tag-pill">angularjs</a>
                                    <a href="" class="tag tag-default tag-pill">css3</a>
                                    <a href="" class="tag tag-default tag-pill">jquery</a>
                                    <a href="" class="tag tag-default tag-pill">bootstrap</a>
                                    <a href="" class="tag tag-default tag-pill">responsive-design</a>
                                    <hr>
                                    <span class="tag tag-primary"><i class="fa fa-user"></i> 900 Followers</span>
                                    <span class="tag tag-success"><i class="fa fa-cog"></i> 43 Forks</span>
                                    <span class="tag tag-danger"><i class="fa fa-eye"></i> 245 Views</span>
                                </div> -->
                                <div class="col-md-12">
                                    <h4 class="m-t-2"><span class="fa fa-clock-o ion-clock pull-xs-right"></span> Addresses</h4>
                                    <table class="table table-hover table-striped">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p>No address yet</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--/row-->
                        </div>
                        <div class="tab-pane" id="messages">
                            <h4 class="m-y-2">Recent Messages &amp; Notifications</h4>
                            <div class="alert alert-info alert-dismissable">
                                <a class="panel-close close" data-dismiss="alert">×</a> This is an <strong>.alert</strong>. Use this to show important messages to the user.
                            </div>
                            <table class="table table-hover table-striped">
                                <tbody>
                                    <tr>
                                        <td>
                                           <span class="pull-xs-right font-weight-bold">3 hrs ago</span> No appointments yet
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="edit">
                            <h4 class="m-y-2">Edit Profile</h4>
                            <form role="form">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">First name</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" value="Jane">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Last name</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" value="Bishop">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Email</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="email" value="email@gmail.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Company</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Website</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="url" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Address</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" value="" placeholder="Street">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label"></label>
                                    <div class="col-lg-6">
                                        <input class="form-control" type="text" value="" placeholder="City">
                                    </div>
                                    <div class="col-lg-3">
                                        <input class="form-control" type="text" value="" placeholder="State">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Time Zone</label>
                                    <div class="col-lg-9">
                                        <select id="user_time_zone" class="form-control" size="0">
                                            <option value="Hawaii">(GMT-10:00) Hawaii</option>
                                            <option value="Alaska">(GMT-09:00) Alaska</option>
                                            <option value="Pacific Time (US &amp; Canada)">(GMT-08:00) Pacific Time (US &amp; Canada)</option>
                                            <option value="Arizona">(GMT-07:00) Arizona</option>
                                            <option value="Mountain Time (US &amp; Canada)">(GMT-07:00) Mountain Time (US &amp; Canada)</option>
                                            <option value="Central Time (US &amp; Canada)" selected="selected">(GMT-06:00) Central Time (US &amp; Canada)</option>
                                            <option value="Eastern Time (US &amp; Canada)">(GMT-05:00) Eastern Time (US &amp; Canada)</option>
                                            <option value="Indiana (East)">(GMT-05:00) Indiana (East)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Username</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" value="janeuser">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Password</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="password" value="11111122333">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Confirm password</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="password" value="11111122333">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label"></label>
                                    <div class="col-lg-9">
                                        <input type="reset" class="btn btn-secondary" value="Cancel">
                                        <input type="button" class="btn btn-primary" value="Save Changes">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
              </div>
        </div>
        <div class="col-lg-4 pull-lg-8 text-xs-center">
            <div class="card profile-card-1">
    		        <img src="https://images.pexels.com/photos/946351/pexels-photo-946351.jpeg?w=500&h=650&auto=compress&cs=tinysrgb" alt="profile-sample1" class="background"/>
    		        <img src="https://randomuser.me/api/portraits/women/20.jpg" alt="profile-image" class="profile"/>
                    <div class="card-content">
                    <h2>Savannah Fields<small>Engineer</small></h3>
                    <div class="icon-block"><a href="#"><i class="fa fa-facebook"></i></a><a href="#"> <i class="fa fa-twitter"></i></a><a href="#"> <i class="fa fa-google-plus"></i></a></div>
                    </div>
                </div>
                <p class="mt-3 w-100 float-left text-center"><strong>Basic Profile Card</strong></p>
        </div>
    </div>
</div>
</section>
