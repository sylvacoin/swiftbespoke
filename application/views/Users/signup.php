
<section class="cart" id="app">
    <div class="container">
         <div class="row">
              <div class="col-sm-8 offset-2">
                  <div class="section-title text-center">
                       <h1 class="heading">
                           Create an account
                       </h1>
                  </div>
                  <?php
                  $redirectURL = "";
                  if ($this->input->get('redirect') !== null):
                      $redirectURL = $this->input->get('redirect');
                  ?>
                  <div class="alert alert-info">
                     For better user experience, please signup. Already have an account? <?= anchor('login'.(isset($_GET['redirect'])?'?redirect='.$_GET['redirect']:''), 'Login') ?>
                  </div>
              <?php endif; ?>
                  <form action="" @submit.prevent="signup">
                      <div class="form-row">
                          <div class="form-group col-md-6">
                              <label>First Name</label>
                              <input type="text" class="form-control custom-form-control" v-model="user.firstname">
                          </div>
                          <div class="form-group col-md-6">
                              <label>Last Name</label>
                              <input type="text" class="form-control custom-form-control" v-model="user.lastname">
                          </div>
                      </div>
                      <div class="form-group">
                          <label>Email Address</label>
                          <input type="email" class="form-control custom-form-control" v-model="user.email">
                      </div>
                      <div class="form-group">
                          <label>Phone Number</label>
                          <input type="text" class="form-control custom-form-control" v-model="user.phone" placeholder="080...">
                      </div>
                      <div class="form-group">
                          <label>Address</label>
                          <input type="text" class="form-control custom-form-control" v-model="user.address1" placeholder="1234 Main St">
                      </div>
                      <div class="form-group">
                          <label>Address 2</label>
                          <input type="text" class="form-control custom-form-control" v-model="user.address2" placeholder="secondary address (optional)">
                      </div>
                      <div class="form-row">
                          <input type="hidden" v-model="country">
                          <div class="form-group col-md-4">
                              <label>State</label>
                              <select v-model="user.state" class="form-control custom-form custom-form-control">
                                  <option selected>Choose...</option>
                                  <option>...</option>
                              </select>
                          </div>
                          <div class="form-group col-md-6">
                              <label>City</label>
                              <input type="text" class="form-control custom-form-control" v-model="user.city">
                          </div>
                          <div class="form-group col-md-2">
                              <label>Zip</label>
                              <input type="text" class="form-control custom-form-control" placeholder="" v-model="user.zip">
                          </div>
                      </div>
                      <div class="form-row">
                          <div class="form-group col-md-6">
                              <label>Password</label>
                              <input type="password" class="form-control custom-form-control" placeholder="" v-model="user.password">
                          </div>
                          <div class="form-group col-md-6">
                              <label>Confirm Password</label>
                              <input type="password" class="form-control custom-form-control" placeholder="" v-model="user.confirmPassword">
                          </div>
                      </div>
                      <button type="submit" class="btn btn-primary">Sign in</button>
                  </form>

              </div>
         </div>
    </div>
</section>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js" charset="utf-8"></script>
<script type="text/javascript">
    var app = new Vue({
        el:'#app',
        data: {
            user: {
                firstname:'',
                lastname:'',
                email:'',
                phone:'',
                address1:'',
                address2:'',
                country:'1',
                state:'',
                city:'',
                zip:'',
                password:'',
                confirmPassword: '',
                group: '<?= $default_user ?>'
            },
            redirectURL:'<?= $redirectURL ?>'
        },
        methods: {
            signup(){
                if (this.password == this.confirmPassword)
                {
                    axios.post('users/ajaxSignup', this.user).then((response)=>{
                        if (response.status == 201)
                        {
                            if (this.redirectURL != '')
                            {
                                window.location.href = this.redirectURL;
                            }else{
                                window.location.href = 'accounts';
                            }
                        }
                    })
                }
            },
        }
    })
</script>
