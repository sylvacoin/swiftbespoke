<section class="py-5" id="app">
    <?= show_message($this->session->flashdata('success'), $this->session->flashdata('error')); ?>
           <div class="row">
               <div class="col-lg-4 mb-5">
                 <div class="card">
                   <div class="card-body">
                     <form id="" @submit.prevent="submit">
                    <div class="form-group row">
                    <input v-model="Item.style" type="text" placeholder="Style" class="form-control input-md" required>
                    <input v-model="Item.style_id" type="hidden">
                    </div>
                    <div class="line"></div>

                    <!-- Select Basic -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Is Active</label>
                        <select id="is_active" v-model="Item.is_active" class="form-control">
                         <option value="1">Active</option>
                         <option value="0">InActive</option>
                        </select>
                    </div>
                    <div class="line"></div>

                    <!-- File Button -->
                    <div class="form-group row">
                    <button type="submit" name="button" class="btn btn-primary"> submit </button>
                    <button type="reset" name="button" class="btn btn-secodary" v-if="Item.category_id > 0"> reset </button>
                    </div>
                     </form>
                   </div>
                 </div>
               </div>

             <div class="col-lg-8 mb-4">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0"><?= isset($title) ? $title : '' ?></h6>
                 </div>
                 <div class="card-body">
                   <table class="table card-text  table-sm">
                     <thead>
                       <tr>
                         <th width="50%">Style</th>
                         <th>Status</th>
                         <th width="10%">Action</th>
                       </tr>
                     </thead>
                     <tbody>
                    <?php if( !empty($data) && $data->num_rows() > 0 ): foreach($data->result() as $row): ?>
                       <tr>
                         <td><?= isset($row->style) ? $row->style: '' ?></td>
                         <td><?= $row->is_active == 1 ? '<span class="badge badge-success">active</span>': '<span class="badge badge-danger">inactive</span>' ?></td>
                         <td width="10%">
                             <div class="btn-group btn-sm">
                                 <a href="javascript:void(0);" @click="editItem($event)" data-id="<?= $row->style_id ?>" class="btn btn-xs btn-success"><i class="fa fa-pen"></i> </a>
                                 <?php if ($row->is_active) : ?>
                                     <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->style_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-times"></i> </a>
                                 <?php else: ?>
                                     <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->style_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> </a>
                                <?php endif; ?>
                                    <a href="javascript:void(0);" @click="deleteItem($event)" data-id="<?= $row->style_id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                             </div>
                         </td>
                       </tr>
                   <?php endforeach; else: ?>
                       <tr>
                         <td colspan="7"> No users exist at the moment </td>
                       </tr>
                   <?php endif;?>
                     </tbody>
                   </table>
                 </div>
               </div>
             </div>
           </div>
         </section>
<script src="<?= base_url() ?>assets/js/vue.min.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="<?= base_url() ?>assets/js/image-input.js"></script>
<script type="text/javascript">
var app = new Vue({
    el : '#app',
    data: {
        Item: {
            style:'',
            style_id: 0,
            is_active: 1
        }
    },
    methods: {
        reset()
        {
            return {
                style:'',
                style_id: 0,
                is_active: 1
            }
        },

        submit()
        {
            let app = this;
            formRequest  = new FormData();
            formRequest.append('style', app.Item.style);
            formRequest.append('is_active', app.Item.is_active);
            if ( app.Item.style_id > 0 )
            {
                axios.post('<?= site_url() ?>styles/put_style/'+ app.Item.style_id, formRequest).then((response) => {
                    console.log(response);
                    $.growl.notice({ 'message': "item was updated successfully."});
                    app.Item = this.reset();
                    location.reload();
                }).catch(e => console.log(e));
            }else{
                axios.post('<?= site_url() ?>styles/post_style', formRequest ).then((response) => {
                    console.log(response);
                    $.growl.notice({ 'message': "item was created successfully."});
                    app.Item = this.reset();
                    location.reload();
                }).catch(e => {
                    console.log(e)
                    $.growl.error({ 'message': "All fields are required"});
                } );
            }

        },

        editItem(e)
        {
            let id;
            let app = this;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.get('<?= base_url() ?>styles/single/'+id).then((response) => {
                console.log(response.data.data);
                if ( response.status == 200 )
                {
                    this.Item = response.data.data;
                }
            }).catch(e => console.log(e));
        },

        deleteItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>styles/trash/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was deleted successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        },

        toggleActivation(e)
        {
            let status = $(e.target).closest('tr').children()[2];
            console.log($(status).find('span').text());
            if ( $(status).find('span').text() == 'active' )
            {
                this.deactivateItem(e);
            }else{
                this.activateItem(e);
            }
        },

        deactivateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>styles/deactivate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[2];
                    $.growl.notice({ 'message': "item was deactivated successfully."});
                    $(badge_parent).find('span').removeClass("badge-success").addClass("badge-danger").text('inactive');
                    $(e.target).closest('tr').children().find('i.fa-times').removeClass('fa-times').addClass('fa-check');
                }
            }).catch(e => console.log(e));
        },

        activateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>styles/activate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[2];
                    $.growl.notice({ 'message': "item was activated successfully."});
                    $(badge_parent).find('span').removeClass("badge-danger").addClass("badge-success").text('active');
                    $(e.target).closest('tr').children().find('i.fa-check').removeClass('fa-check').addClass('fa-times');
                }
            }).catch(e => console.log(e));
        }
    }
})
</script>

<style media="screen">
    .w-50 {
        width: 50% !important;
    }
</style>
