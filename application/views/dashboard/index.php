<section class="py-5">
      <div class="row">
        <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
          <div
            class="bg-white shadow  p-4 h-100 d-flex align-items-center justify-content-between"
          >
            <div class="flex-grow-1 d-flex align-items-center">
              <div class="dot mr-3 bg-violet"></div>
              <div class="text">
                <h6 class="mb-0">Orders</h6>
                <span class="text-gray">12</span>
              </div>
            </div>
            <div class="icon">
              <i class="fa fa-3x flaticon-order-1"></i>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
          <div
            class="bg-white shadow  p-4 h-100 d-flex align-items-center justify-content-between"
          >
            <div class="flex-grow-1 d-flex align-items-center">
              <div class="dot mr-3 bg-green"></div>
              <div class="text">
                <h6 class="mb-0">Outfits</h6>
                <span class="text-gray">32</span>
              </div>
            </div>
            <div class="icon">
              <i class="fa fa-3x flaticon-clothes"></i>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
          <div
            class="bg-white shadow  p-4 h-100 d-flex align-items-center justify-content-between"
          >
            <div class="flex-grow-1 d-flex align-items-center">
              <div class="dot mr-3 bg-blue"></div>
              <div class="text">
                <h6 class="mb-0">Customers</h6>
                <span class="text-gray">400</span>
              </div>
            </div>
            <div class="icon">
              <i class="fa fa-3x flaticon-rating"></i>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
          <div
            class="bg-white shadow  p-4 h-100 d-flex align-items-center justify-content-between"
          >
            <div class="flex-grow-1 d-flex align-items-center">
              <div class="dot mr-3 bg-red"></div>
              <div class="text">
                <h6 class="mb-0">Sales</h6>
                <span class="text-gray">123</span>
              </div>
            </div>
            <div class="icon">
              <i class="fa fa-3x flaticon-analytics"></i>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="row mb-4">
        <div class="col-lg-7 mb-4 mb-lg-0">
          <div class="card">
            <div class="card-body">
              <img src="<?= base_url() ?>assets/img/BG.jpg" class="img-fluid w-100">
            </div>
          </div>
        </div>
        <div class="col-lg-5 mb-4 mb-lg-0 pl-lg-0">
          <div class="card mb-3">
            <div class="card-body">
              <div class="row align-items-center flex-row">
                <div class="col-lg-5">
                  <h2 class="mb-0 d-flex align-items-center">
                    <span>86.4%</span>
                    <span class="dot bg-green d-inline-block ml-3"></span>
                  </h2>
                  <span class="text-muted text-uppercase small">Completed</span>
                  <hr>
                  <small class="text-muted">Overall completed orders</small>
                </div>
                <div class="col-lg-7">
                  <canvas id="pieChartHome1"></canvas>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center flex-row">
                <div class="col-lg-5">
                  <h2 class="mb-0 d-flex align-items-center">
                    <span>12.6%</span>
                    <span class="dot bg-violet d-inline-block ml-3"></span>
                  </h2>
                  <span class="text-muted text-uppercase small">Orders Pending</span>
                  <hr>
                  <small class="text-muted">Overall pending orders</small>
                </div>
                <div class="col-lg-7">
                  <canvas id="pieChartHome2"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
