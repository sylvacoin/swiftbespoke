
<section class="cart" id="app">
    <div class="container">
         <div class="row">
              <div class="col-12">
                   <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                             <li class="breadcrumb-item"><a href="#">Home</a></li>
                             <li class="breadcrumb-item active" aria-current="page">cart</li>
                        </ol>
                   </nav>
              </div>
         </div>
         <div class="row">
              <div class="col-sm-12">
                  <div class="table-responsive">
                      <table class="table table-striped table-condensed">
                          <thead>
                              <th>Item</th>
                              <th>Description</th>
                              <th>Unit Price</th>
                              <th>Quantity</th>
                              <th>Total</th>
                          </thead>
                          <tbody>

                             <tr v-for="(item, index) in cart" v-if="cart.length > 0">
                                  <td class="bg-dark">
                                      <div class="preview" style="width:80px; height:80px; margin: 10px auto;">
                                          <img :src="item.image" class="w-100 img-fluid" />
                                      </div>
                                  </td>
                                  <td>
                                      <ul>

                                          <li v-for="selection in item.customizations">
                                              <b>{{selection.option}}:</b>
                                              {{selection.option_value}} (NGN{{selection.price}})</li>

                                      </ul>
                                  </td>
                                  <td>
                                      NGN<span class="price">{{item.cost}}</span>
                                  </td>
                                  <td>
                                      <span>{{ item.quantity}}</span>
                                  </td>
                                  <td>
                                      <span>NGN{{ item.quantity * item.cost }}</span>
                                  </td>
                              </tr>
                              <tr v-if="cart.length == 0" >
                                  <td colspan="5" class="text-center"> Cart is empty </td>
                              </tr>
                          </tbody>
                          <tfoot>
                              <tr>
                                   <td colspan="2" rowspan="3">
                                       &nbsp;
                                   </td>
                                   <th colspan="2">
                                       <b>Total</b>
                                   </th>
                                   <td class="checkout">
                                       <span>NGN{{total}}</span>
                                   </td>
                               </tr>
                               <tr>
                                    <th colspan="2">
                                        <b>Discount</b>
                                    </th>
                                    <td class="checkout">
                                        <span>NGN 0</span>
                                    </td>
                                </tr>
                                <tr>
                                     <th colspan="2">
                                         <b>Grand Total</b>
                                     </th>
                                     <td class="checkout">
                                         <span>NGN{{total}}</span>
                                     </td>
                                 </tr>
                          </tfoot>
                      </table>
                  </div>
              </div>
         </div>
         <div class="row">
             <div class="col-sm-7">
                 <form class="" @submit.prevent="submit" method="post">
                 <h4>Shipping Address</h4>
                 <div class="form-row">
                     <div class="form-group col-md-6">
                         <label for="name">First Name*</label>
                         <input type="text" class="form-control custom-form-control" placeholder="First Name" v-model="Item.shipping.firstname">
                     </div>
                     <div class="form-group col-md-6">
                         <label for="name">Last Name*</label>
                         <input type="text" class="form-control custom-form-control" placeholder="Last Name" v-model="Item.shipping.lastname">
                     </div>
                 </div>
                 <div class="form-row">
                     <div class="form-group col-md-4">
                         <label for="address">Street Address</label>
                         <input type="text" class="form-control custom-form-control" v-model="Item.shipping.address" placeholder="Address">
                     </div>
                     <div class="form-group col-md-4">
                         <label for="name">State*</label>
                         <select class="custom-select custom-form-control" v-model="Item.shipping.state">
                             <option value="">select</option>
                         </select>
                     </div>
                     <div class="form-group col-md-4">
                         <label for="address">City*</label>
                         <input type="text" class="form-control custom-form-control" v-model="Item.shipping.city" placeholder="City">
                     </div>
                 </div>

                 <div class="form-row">
                     <div class="form-group col-md-12">
                         <label for="inputCity">Other details (closes landmarks if any )</label>
                         <input type="text" class="form-control custom-form-control" v-model="Item.other">
                     </div>
                 </div>

                 <h4>Billing Address</h4>
                 <div class="form-group">
                     <div class="form-check">
                         <input class="form-check-input" v-model="isChecked" type="checkbox" id="gridCheck" @click="toggleBillingAddress()">
                         <label class="form-check-label" for="gridCheck">
                             same as shipping address
                         </label>
                     </div>
                 </div>
                 <div id="billing-address" v-show="!isChecked">
                     <div class="form-row">
                         <div class="form-group col-md-6">
                             <label for="name">First Name*</label>
                             <input type="text" class="form-control custom-form-control" placeholder="First Name" v-model="Item.billing.firstname">
                         </div>
                         <div class="form-group col-md-6">
                             <label for="name">Last Name*</label>
                             <input type="text" class="form-control custom-form-control" placeholder="Last Name" v-model="Item.billing.lastname">
                         </div>
                     </div>
                     <div class="form-row">
                         <div class="form-group col-md-4">
                             <label for="address">Street Address</label>
                             <input type="text" class="form-control custom-form-control" v-model="Item.billing.address" placeholder="Address">
                         </div>
                         <div class="form-group col-md-4">
                             <label for="name">State*</label>
                             <select class="custom-select" v-model="Item.billing.state">
                                 <option value="">select</option>
                             </select>
                         </div>
                         <div class="form-group col-md-4">
                             <label for="address">City*</label>
                             <input type="text" class="form-control custom-form-control" v-model="Item.billing.city" placeholder="City">
                         </div>
                     </div>

                     <div class="form-row">
                         <div class="form-group col-md-12">
                             <label for="inputCity">Other details (closes landmarks if any )</label>
                             <input type="text" class="form-control custom-form-control" v-model="Item.billing.other">
                         </div>
                     </div>
                 </div>
                 <div id="payments">
                     <h4>Total Payment</h4>
                     <table class="table">
                         <tr>
                              <th>
                                  <b>Total</b>
                              </th>
                              <th class="checkout">
                                  <span>NGN{{total}}</span>
                              </th>
                          </tr>
                          <tr>
                               <th>
                                   <b>Discount</b>
                               </th>
                               <th class="checkout">
                                   <span>NGN 0</span>
                               </th>
                           </tr>
                           <tr>
                                <th>
                                    <b>Grand Total</b>
                                </th>
                                <th class="checkout">
                                    <span>NGN{{total}}</span>
                                </th>
                            </tr>
                     </table>
                 </div>
                 <div id="payment-type">
                     <h4>Payment Method</h4>
                     <div class="d-block my-3">
                         <div class="" v-for="method in payment_methods">
                             <label class="">
                                  <input v-model="payment_type" type="radio" name="paymentMethod" required v-bind:value="method.payment_type_id" :key="method.payment_type_id">
                                 {{ method.payment_type }}
                             </label>
                         </div>
                     </div>
                 </div>
                 <button v-if="cart.length > 0" class="btn btn-lg btn-primary float-right"> Pay and complete order </button>
                 </form>
                 <paystack
                    :amount="amount"
                    :email="user.email"
                    :paystackkey="pk"
                    :reference="reference"
                    :callback="callback"
                    :close="close"
                    :embed="false"
                    ref="gateway"
                    type="button"
                    style="display:none"
                />
                    <i class="fas fa-money-bill-alt"></i>
                    Make Payment
                </paystack>
             </div>
             <div class="col-sm-5">
                 <h4>Choose an address</h4>
                 <div class="card border-light mb-3" style="cursor:pointer" v-for="address in addresses" :id="'address_'+address.address_id">
                     <div class="card-body">
                         <h5 class="card-title">{{address.address1}}</h5>
                         <p class="card-text">{{address.address1}}</p>
                     </div>
                 </div>
             </div>

         </div>
    </div>
</section>
<script src="https://unpkg.com/vue-paystack/dist/paystack.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js" charset="utf-8"></script>

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        components:{
          'paystack': VuePaystack.default
        },
        data: {
            cart: [],
            Item: {
                shipping: {
                    firstname:'',
                    lastname: '',
                    address: '',
                    state: '',
                    city: '',
                    other: ''
                },
                billing: {
                    name:'',
                    address: '',
                    state: '',
                    city: '',
                    other: ''
                }
            },
            payment_type:'',
            user:{
                email:''
            },
            addresses:[],
            isChecked: true,
            payment_methods: [],
            pk: 'pk_test_2c02ee916557767252b23f5b0052daaaa3c3a5a9',
            isLoading: false
        },
        created(){
            this.getCartItems();
            this.getUser();
            this.getPaymentMethods();
            $('#billing-address').hide();
        },
        computed:{
            total: function(){
                     let sum = 0;
                    return app.cart.reduce(function(prev, item, idx){
                        return sum += item.total;
                  },0);
              },
            amount: function(){
                return this.total * 100;
            },
            reference(){
              let text = "";
              let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
              for( let i=0; i < 10; i++ )
                text += possible.charAt(Math.floor(Math.random() * possible.length));
              return text;
            }
        },
        methods:{
            getCartItems:function(){
                var app = this;
                axios.get(base_url+'cart/getCartItems').then((response) => {
                    if ( response.status == 200 )
                    {
                        console.log(response);
                        for(let item in response.data)
                        {
                            let data = response.data[item];
                        }
                        app.cart = response.data;
                    }
                })
            },
            getUser()
            {
                    var app = this;
                    axios.get(base_url+'auth/isUserLoggedIn').then((response) => {
                        console.log(response);
                        if ( response.status == 200)
                        {
                            if (response.data.isLogged)
                            {
                                app.user = response.data.user;
                                this.Item = {
                                    shipping: {
                                        firstname:response.data.user.firstname,
                                        lastname: response.data.user.lastname,
                                        address: response.data.addresses[0].address1,
                                        state: response.data.addresses[0].state_id,
                                        city: response.data.addresses[0].city_id,
                                        other: ''
                                    },
                                    billing: {
                                        firstname:response.data.user.firstname,
                                        lastname: response.data.user.lastname,
                                        address: response.data.addresses[0].address1,
                                        state: response.data.addresses[0].state_id,
                                        city: response.data.addresses[0].city_id,
                                        other:''
                                    },
                                },
                                this.addresses = response.data.addresses;
                            }else{
                                window.location.href=base_url+'login?redirect=cart/checkout';
                            }
                        }
                    })
            },
            getPaymentMethods()
            {
                axios.get(base_url+'payments/get_json').then((response) => {
                    if (response.status == 200)
                    {
                        this.payment_methods = response.data.data;
                    }
                })
            },
            toggleBillingAddress:function()
            {
                let Item = this.Item;
                if (this.isChecked)
                {
                    Item.billing = Item;
                }else{
                    Item.billing = {
                        name:'',
                        address: '',
                        state: '',
                        city: '',
                        other: ''
                    }
                }
                $('#billing-address').toggle('slow');
            },
            selectAddress(id)
            {
                let selectedAddress = this.address[id];
                this.Item.address1 = selectedAddress.address1;
                this.Item.city = selectedAddress.city_id;
                this.Item.state= selectedAddress.state_id;
                this.Item.country=selectedAddress.country_id;
            },
            submit()
            {
                let formData = {
                    customer:this.user.user_id,
                    total: this.total,
                    payment_type: this.payment_type,
                    billing_address: this.Item.billing,
                    shipping_address: this.Item.shipping
                }

                var order_id = this.reference;
                axios.post(base_url+'orders/make_order/'+ this.reference, formData).then((response)=>{
                    if ( response.status == 200 && response.data.data == 'success')
                    {
                        this.$refs.gateway.$el.click();
                        return
                    }
                })

            },
            callback(response){
                console.log(response);
                this.isLoading = true;
                axios.post(base_url+'orders/verify_order/'+response.reference).then((response) => {
                    if ( response.status == 200 )
                    {
                        this.isLoading = false;
                        $.growl.notice({'message':'Payment was successfully confirmed. Redirecting ...'});
                        window.location.href = base_url+'/account';
                    }
                })
            },
            close(){
                alert('gate way was closed');
            }

        },
    })
</script>
