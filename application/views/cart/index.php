
<section class="cart" id="app">
                    <div class="container">
                         <div class="row">
                              <div class="col-12">
                                   <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                             <li class="breadcrumb-item"><a href="#">Home</a></li>
                                             <li class="breadcrumb-item active" aria-current="page">cart</li>
                                        </ol>
                                   </nav>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-sm-12">
                                  <div class="table-responsive">
                                      <table class="table table-striped table-condensed">
                                          <thead>
                                              <th>Item</th>
                                              <th>Description</th>
                                              <th>Unit Price</th>
                                              <th>Measurements</th>
                                              <th>Quantity</th>
                                              <th>Total</th>
                                          </thead>
                                          <tbody>

                                             <tr v-for="(item, index) in cart" v-if="cart.length > 0">
                                                  <td>
                                                      <div class="preview" style="width:80px; height:80px;">
                                                          <img :src="item.image" class="w-100 img-fluid" />
                                                      </div>
                                                  </td>
                                                  <td>
                                                      <ul>

                                                          <li v-for="selection in item.customizations">
                                                              <b>{{selection.option}}:</b>
                                                              {{selection.option_value}} (NGN{{selection.price}})</li>

                                                      </ul>
                                                      <button type="button" name="button" @click="remove(item.id, $event)" class="btn-xs btn btn-danger"> remove item</button>
                                                  </td>
                                                  <td>
                                                      NGN<span class="price">{{item.cost}}</span>

                                                  </td>
                                                  <td>
                                                     <table class="w-100 table-condensed" v-if="item.measurement_type == 'standard'">
                                                         <thead>
                                                             <tr style="height: 20px ">
                                                                 <th>Size</th>
                                                                 <th>Quantity</th>
                                                             </tr>
                                                         </thead>
                                                         <tbody>
                                                             <tr v-for="measurement in item.measurement"  style="height: 20px ">
                                                                 <td>{{ measurement.size }}</td>
                                                                 <td>{{ measurement.quantity }}</td>
                                                             </tr>
                                                         </tbody>
                                                     </table>
                                                     <table class="w-100 table-condensed" v-if="item.measurement_type == 'custom'">
                                                         <thead>
                                                             <tr style="height: 20px ">
                                                                 <th>Size</th>
                                                                 <th>Measurement</th>
                                                             </tr>
                                                         </thead>
                                                         <tbody>
                                                             <tr v-for="measurement in item.measurement"  style="height: 20px ">
                                                                 <td>{{ measurement.title }}</td>
                                                                 <td>{{ measurement.value+ ' '+ measurement.unit }}</td>
                                                             </tr>
                                                         </tbody>
                                                     </table>

                                                  </td>
                                                  <td>
                                                     <select class="custom-form" v-model="quantity_n[index].quantity"  v-if="item.measurement_type == 'custom'">
                                                             <option v-for="n in 100" :value="n" > {{ n }}</option>
                                                     </select>
                                                     <span  v-if="item.measurement_type == 'standard'"> {{ quantity_n[index].quantity }} </span>
                                                  </td>
                                                  <td>
                                                      <span>NGN{{ quantity_n[index].quantity * item.cost }}</span>
                                                  </td>
                                              </tr>
                                              <tr v-if="cart.length == 0" >
                                                  <td colspan="6" class="text-center"> Cart is empty </td>
                                              </tr>
                                          </tbody>
                                          <tfoot>
                                              <tr>
                                                   <td colspan="4" rowspan="4">
                                                       &nbsp;
                                                   </td>
                                                   <td>
                                                       <b>Total</b>
                                                   </td>
                                                   <td class="checkout">
                                                       <span>NGN{{total}}</span>
                                                   </td>
                                               </tr>
                                               <tr>
                                                    <td>
                                                        <b>Discount</b>
                                                    </td>
                                                    <td class="checkout">
                                                        <span>NGN 0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                     <td>
                                                         <b>Grand Total</b>
                                                     </td>
                                                     <td class="checkout">
                                                         <span>NGN{{total}}</span>
                                                     </td>
                                                 </tr>
                                          </tfoot>
                                      </table>
                                      <button v-if="cart.length > 0" class="btn btn-lg btn-primary float-right" @click="checkout()"> Proceed to checkout </button>
                                  </div>
                              </div>
                         </div>
                    </div>
       </section>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js" charset="utf-8"></script>
<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            quantity_n:[],
            cart: [],
        },
        created(){
            this.getCartItems();
        },
        computed:{
            total: function(){
                    var app = this;
                     let sum = 0;
                    return app.cart.reduce(function(prev, item, idx){
                        if (typeof app.quantity_n[idx].quantity != undefined)
                        {
                            return sum += item.cost * app.quantity_n[idx].quantity;
                        }
                  },0);
              }
        },
        methods:{
            getCartItems:function(){
                var app = this;
                axios.get(base_url+'cart/getCartItems').then((response) => {
                    if ( response.status == 200 )
                    {
                        for(let item in response.data)
                        {
                            let data = response.data[item];
                            app.quantity_n.push({ 'quantity': data.quantity, 'id':data.id});
                        }

                        app.cart = response.data;
                        console.log(app.quantity_n);
                        console.log(app.cart.measurement);
                    }
                })
            },

            remove(id, e){
                let app = this;
                var result = app.quantity_n.map(function(el) {
                    return el.id;
                  }).indexOf(id);
                axios.delete('cart/deleteEntry/'+id).then((response)=>{
                    if (response.status == 200)
                    {
                        var result = app.quantity_n.findIndex(function(el) {
                          return el.id == id;
                        });
                        app.quantity_n = [];
                        app.getCartItems();

                        $(e.target).closest('tr').remove();
                    }
                })
            },

            checkout()
            {
                axios.get('auth/isUserLoggedIn').then(response => {
                    if ( response.status == 200 )
                    {
                        return response.isLogged;
                    }
                }).then((res)=>{
                    axios.post('cart/updateCart', app.quantity_n).then(response => {
                        if (response.status == 200)
                        {
                            if (res)
                            {
                                window.location.href = 'cart/checkout';
                            }
                            window.location.href = 'signup?redirect=cart/checkout';
                        }
                    })
                }).catch((e)=> console.log(e));

            }

        },
    })
</script>
