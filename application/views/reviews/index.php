<section class="py-5" id="app">
    <?= show_message($this->session->flashdata('success'), $this->session->flashdata('error')); ?>
           <div class="row">
             <div class="col-lg-12 mb-4">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0"><?= isset($title) ? $title : '' ?></h6>
                 </div>
                 <div class="card-body">
                     <button class="btn btn-secondary" onclick=" window.history.back();" type="button"> <i class="fa fa-arrow-left"></i> Back</button>
                     <div class="table-responsive">
                           <table class="table card-text  table-sm">
                             <thead>
                               <tr>
                                 <th>Product</th>
                                 <th>User</th>
                                 <th>Created Date</th>
                                 <th>Rating</th>
                                 <th>Likes</th>
                                 <th>DisLikes</th>
                                 <th width="70px">Action</th>
                               </tr>
                             </thead>
                             <tbody>
                            <?php if( !empty($data) && $data->num_rows() > 0 ): $i = 1; foreach($data->result() as $row): ?>
                               <tr>
                                 <td><?= isset($row->product_name) ? $row->product_name: '' ?></td>
                                 <td ><?= isset($row->firstname) ? $row->firstname: '' ?> <?= isset($row->lastname) ? $row->lastname: '' ?></td>
                                 <td><?= isset($row->created_at) ? $row->created_at: '' ?></td>
                                 <td><?= isset($row->rating) ? $row->rating: '' ?></td>
                                 <td><?= isset($row->likes) ? $row->likes: '0' ?></td>
                                 <td><?= isset($row->dislikes) ? $row->dislikes: '0' ?></td>
                                 <td width="10%">
                                     <div class="btn-group btn-sm">
                                        <a href="javascript:void(0);" @click="viewMessage($event)" data-id="<?= $row->product_id ?>" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> </a>
                                        <a href="javascript:void(0);" @click="deleteItem($event)" data-id="<?= $row->product_id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                                     </div>
                                 </td>
                               </tr>
                           <?php endforeach; else: ?>
                               <tr>
                                 <td colspan="7"> No users exist at the moment </td>
                               </tr>
                           <?php endif;?>
                             </tbody>
                           </table>
                    </div>
                 </div>
               </div>
             </div>
           </div>
           <div class="modal" id="myModal">
             <div class="modal-dialog">
               <div class="modal-content">

                 <!-- Modal Header -->
                 <div class="modal-header">
                   <h4 class="modal-title">Modal Heading</h4>
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                 </div>

                 <!-- Modal body -->
                 <div class="modal-body" >
                   {{review}}
                 </div>

                 <!-- Modal footer -->
                 <div class="modal-footer">
                   <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                 </div>

               </div>
             </div>
           </div>

         </section>
         <!-- The Modal -->
<script src="<?= base_url() ?>assets/js/vue.min.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="<?= base_url() ?>assets/js/image-input.js" charset="utf-8"></script>
<script type="text/javascript">
var app = new Vue({
    el : '#app',
    data: {
        review:'',
        loading: false
    },
    methods: {
        deleteItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>products/trash/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was deleted successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        },

        viewMessage(e)
        {
            let app = this;
            this.loading = true;
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.get('<?= base_url() ?>reviews/get_review/'+id).then((response) => {
                console.log(response);
                if ( response.status == 200 )
                {
                    app.review = response.data.data.message;
                    $('#myModal').modal('show')
                    this.loading = false;
                }
            }).catch(e => console.log(e));
        }
    }
})
</script>
