<section class="py-5" id="app">
    <?= show_message($this->session->flashdata('success'), $this->session->flashdata('error')); ?>
           <div class="row">
               <div class="col-lg-4 mb-5">
                 <div class="card">
                   <div class="card-body">
                     <form id="" @submit.prevent="submit">
                    <input v-model="Item.measurement_id" type="hidden">

                    <div class="form-group row">
                        <label class="label-control col-sm-12">Measurement Type</label>
                        <select id="measurement_type" v-model="Item.measurement_type_id" class="form-control">
                         <option :value="mt.measurement_type_id" v-for="mt in MeasurementTypes">{{mt.measurement_type}}</option>
                        </select>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="label-control col-sm-12">Categories</label>
                        <select id="categories" v-model="Item.category_id" class="form-control">
                         <option :value="cat.category_id" v-for="cat in Categories">{{cat.category}}</option>
                        </select>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="label-control col-sm-12"> Measurement Range </label>
                        <div class="col-6">
                            <span class="label label-default">Max Range</span>
                            <input type="number" v-model="Item.max_range" value="0" min="1" class="form-control" placeholder="Max" required/>
                        </div>
                        <div class="col-6">
                            <span class="label label-default">Min Range</span>
                            <input type="number" v-model="Item.min_range" value="0" min="1" class="form-control" placeholder="Min" required/>
                        </div>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="label-control col-sm-12">Instruction Video Url</label>
                        <input type="text" v-model="Item.video_url" class="form-control" placeholder="http://" />
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="label-control col-sm-12">Unit</label>
                        <select id="unit" v-model="Item.unit_id" class="form-control">
                         <option :value="unit.unit_id" v-for="unit in Units">{{unit.unit}}</option>
                        </select>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="label-control col-sm-12">Is Active</label>
                        <select id="is_active" v-model="Item.is_active" class="form-control">
                         <option value="1">Active</option>
                         <option value="0">InActive</option>
                        </select>
                    </div>

                    <div class="form-group row">
                    <button type="submit" name="button" class="btn btn-primary"> submit </button>
                    </div>
                     </form>
                   </div>
                 </div>
               </div>

             <div class="col-lg-8 mb-4">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0"><?= isset($title) ? $title : '' ?></h6>
                 </div>
                 <div class="card-body">
                     <div class="table-responsive">
                           <table class="table card-text  table-sm">
                             <thead>
                               <tr>
                                 <th>Category</th>
                                 <th>Measurement Type</th>
                                 <th>Unit</th>
                                 <th>Range</th>
                                 <th>Status</th>
                                 <th width="70px">Action</th>
                               </tr>
                             </thead>
                             <tbody>
                            <?php if( !empty($data) && $data->num_rows() > 0 ): $i = 1; foreach($data->result() as $row): ?>
                               <tr>
                                 <td><?= isset($row->category) ? $row->category: '' ?></td>
                                 <td><?= isset($row->measurement_type) ? $row->measurement_type: '' ?></td>
                                 <td><?= isset($row->unit) ? $row->unit: '' ?></td>
                                 <td><?= isset($row->max_range) ? $row->max_range: 0 ?> - <?= isset($row->min_range) ? $row->min_range: 0 ?></td>
                                 <td><?= $row->is_active == 1 ? '<span class="badge badge-success">active</span>': '<span class="badge badge-danger">inactive</span>' ?></td>
                                 <td width="10%">
                                     <div class="btn-group btn-sm">
                                         <a href="javascript:void(0);" @click="editItem($event)" data-id="<?= $row->measurement_id ?>" class="btn btn-xs btn-success"><i class="fa fa-pen"></i> </a>
                                         <?php if ($row->is_active) : ?>
                                             <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->measurement_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-times"></i> </a>
                                         <?php else: ?>
                                             <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->measurement_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> </a>
                                        <?php endif; ?>
                                            <a href="javascript:void(0);" @click="deleteItem($event)" data-id="<?= $row->measurement_id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                                     </div>
                                 </td>
                               </tr>
                           <?php endforeach; else: ?>
                               <tr>
                                 <td colspan="7"> No users exist at the moment </td>
                               </tr>
                           <?php endif;?>
                             </tbody>
                           </table>
                    </div>
                 </div>
               </div>
             </div>
           </div>
         </section>
<script src="<?= base_url() ?>assets/js/vue.min.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="<?= base_url() ?>assets/js/image-input.js" charset="utf-8"></script>
<script type="text/javascript">
var app = new Vue({
    el : '#app',
    data: {
        Item: {
            measurement_id: 0,
            measurement_type_id: 1,
            is_active: 1,
            min_range:1,
            max_range: 1,
            unit_id: 1,
            video_url: ''
        },
        MeasurementTypes:[],
        Categories: [],
        Units:[]
    },
    created(){
            this.getMeasurementTypes();
            this.getCategories();
            this.getUnits();
    },
    methods: {
        reset()
        {
            return {
                measurement_id: 0,
                measurement_type_id: 1,
                is_active: 1,
                min_range:1,
                max_range: 1,
                unit_id: 1,
                video_url: ''
            }
        },

        submit()
        {
            let app = this;
            formRequest  = new FormData();
            formRequest.append('measurement_type_id', app.Item.measurement_type_id);
            formRequest.append('category_id', app.Item.category_id);
            formRequest.append('unit_id', app.Item.unit_id);
            formRequest.append('max_range', app.Item.max_range);
            formRequest.append('min_range', app.Item.min_range);
            formRequest.append('is_active', app.Item.is_active);
            formRequest.append('video_url', app.Item.video_url);

            if ( app.Item.measurement_id > 0 )
            {
                axios.post('<?= site_url() ?>measurements/put_measurement/'+ app.Item.measurement_id, formRequest).then((response) => {
                    console.log(response);
                    $.growl.notice({ 'message': "item was updated successfully."});
                    app.Item = this.reset();
                    location.reload();
                }).catch(e => console.log(e));
            }else{
                axios.post('<?= site_url() ?>measurements/post_measurement', formRequest ).then((response) => {
                    console.log(response);
                    $.growl.notice({ 'message': "item was created successfully."});
                    app.Item = this.reset();
                    location.reload();
                }).catch(e => console.log(e));
            }

        },
        editItem(e)
        {
            let id;
            let app = this;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.get('<?= base_url() ?>measurements/single/'+id).then((response) => {
                console.log(response.data);
                if ( response.status == 200 )
                {
                    this.Item = response.data.data;
                }
            }).catch(e => console.log(e));
        },
        getMeasurementTypes()
        {
            axios.get('<?= base_url() ?>measurementtypes/get_json').then((response) => {
                console.log(response.data);
                if ( response.status == 200 )
                {
                    this.MeasurementTypes = response.data.data;
                }
            }).catch(e => console.log(e));
        },

        getCategories()
        {
            axios.get('<?= base_url() ?>categories/get_json').then((response) => {
                console.log(response.data);
                if ( response.status == 200 )
                {
                    this.Categories = response.data.data;
                }
            }).catch(e => console.log(e));
        },

        getUnits()
        {
            axios.get('<?= base_url() ?>units/get_json').then((response) => {
                console.log(response.data);
                if ( response.status == 200 )
                {
                    this.Units = response.data.data;
                }
            }).catch(e => console.log(e));
        },


        deleteItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>measurements/trash/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was deleted successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        },

        toggleActivation(e)
        {
            let status = $(e.target).closest('tr').children()[1];
            console.log($(status).find('span').text());
            if ( $(status).find('span').text() == 'active' )
            {
                this.deactivateItem(e);
            }else{
                this.activateItem(e);
            }
        },

        deactivateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>measurements/deactivate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[1];
                    $.growl.notice({ 'message': "item was deactivated successfully."});
                    $(badge_parent).find('span').removeClass("badge-success").addClass("badge-danger").text('inactive');
                    $(e.target).closest('tr').children().find('i.fa-times').removeClass('fa-times').addClass('fa-check');
                }
            }).catch(e => console.log(e));
        },

        activateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>measurements/activate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[1];
                    $.growl.notice({ 'message': "item was activated successfully."});
                    $(badge_parent).find('span').removeClass("badge-danger").addClass("badge-success").text('active');
                    $(e.target).closest('tr').children().find('i.fa-check').removeClass('fa-check').addClass('fa-times');
                }
            }).catch(e => console.log(e));
        }
    }
})
</script>
