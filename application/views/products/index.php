<section class="py-5" id="app">
    <?= show_message($this->session->flashdata('success'), $this->session->flashdata('error')); ?>
           <div class="row">
               <div class="col-lg-4 mb-5">
                 <div class="card">
                   <div class="card-body">
                     <form id="" @submit.prevent="submit">
                    <div class="form-group row">
                    <label class="label-control col-sm-12">Product Name</label>
                    <input v-model="Item.product_name" type="text" placeholder="Product" class="form-control input-md" required>
                    <input v-model="Item.product_id" type="hidden">
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                    <label class="label-control col-sm-12">Description</label>
                    <textarea v-model="Item.description" placeholder="Product Description" class="form-control input-md"></textarea>
                    </div>
                    <div class="line"></div>

                    <!-- Select Basic -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Product Category</label>
                        <select v-model="Item.category_id" class="form-control">
                         <option :value="cat.category_id" v-for="cat in categories" :key="cat.category_id">{{cat.category}}</option>
                        </select>
                    </div>
                    <div class="line"></div>
                    <!-- Select Basic -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Product Style</label>
                        <select v-model="Item.style_id" class="form-control">
                         <option :value="s.style_id" v-for="s in styles" :key="s.category_id">{{s.style}}</option>
                        </select>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                    <label class="label-control col-sm-12">Product Price</label>
                    <input v-model="Item.price" type="text" placeholder="0.00" class="form-control input-md" required>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                    <label class="label-control col-sm-12">Product Discount Price</label>
                    <input v-model="Item.discount" type="text" placeholder="0.00" class="form-control input-md" required>
                    </div>
                    <div class="line"></div>

                    <!-- Select Basic -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Is product customizable?</label>
                        <select id="Select" v-model="Item.is_customizable" class="form-control">
                         <option value="1">Yes</option>
                         <option value="0">No</option>
                        </select>
                    </div>
                    <div class="line"></div>
                    <!-- Select Basic -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Is Active</label>
                        <select id="is_active" v-model="Item.is_active" class="form-control">
                         <option value="1">Active</option>
                         <option value="0">InActive</option>
                        </select>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Is product stock?</label>
                        <select v-model="Item.in_stock" class="form-control">
                         <option value="1">Yes</option>
                         <option value="0">No</option>
                        </select>
                    </div>
                    <div class="line"></div>

                    <!-- File Button -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Product display image</label>
                        <div class="col-sm-6">
                            <image-input  v-model="myFile.productImage1" name="file">
                                <div slot="activator">
                                    <div class="card w-100">
                                        <div class="card-body" v-if="!myFile.productImage1" style="width:100%; height:100%; padding: 0;" >
                                            <img class="img-fluid" src="<?= base_url() ?>assets/img/placeholder.jpg" alt="Alternate Text" />
                                        </div>
                                        <div class="card-image" v-else>
                                            <img class="img-fluid" :src="myFile.productImage1.imageURL" alt="Alternate Text" />
                                        </div>
                                    </div>
                                </div>
                            </image-input>
                        </div>
                        <div class="col-sm-6">
                            <image-input  v-model="myFile.productImage2" name="file">
                                <div slot="activator">
                                    <div class="card w-100">
                                        <div class="card-body" v-if="!myFile.productImage2" style="width:100%; height:100%; padding: 0;" >
                                            <img class="img-fluid" src="<?= base_url() ?>assets/img/placeholder.jpg" alt="Alternate Text" />
                                        </div>
                                        <div class="card-image" v-else>
                                            <img class="img-fluid" :src="myFile.productImage2.imageURL" alt="Alternate Text" />
                                        </div>
                                    </div>
                                </div>
                            </image-input>
                        </div>
                    </div>
                    <div class="form-group row">
                    <button type="submit" name="button" class="btn btn-primary"> submit </button>
                    </div>
                     </form>
                   </div>
                 </div>
               </div>

             <div class="col-lg-8 mb-4">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0"><?= isset($title) ? $title : '' ?></h6>
                 </div>
                 <div class="card-body">
                     <div class="table-responsive">
                           <table class="table card-text  table-sm">
                             <thead>
                               <tr>
                                 <th>&nbsp;</th>
                                 <th class="w-150">Product</th>
                                 <th class="w-200">Description</th>
                                 <th class="w-150">Category</th>
                                 <th class="w-100">Style</th>
                                 <th>Price</th>
                                 <th>In stock</th>
                                 <th>Status</th>
                                 <th width="70px">Action</th>
                               </tr>
                             </thead>
                             <tbody>
                            <?php if( !empty($data) && $data->num_rows() > 0 ): $i = 1; foreach($data->result() as $row): ?>
                               <tr>
                                 <td><img src="<?= isset($row->product_image_1) ? $row->product_image_1: '' ?>" width="40" height="40"/></td>
                                 <td><?= isset($row->product_name) ? $row->product_name: '' ?></td>
                                 <td ><?= isset($row->description) ? word_limiter($row->description, 9): '' ?></td>
                                 <td><?= isset($row->category) ? $row->category: '' ?></td>
                                 <td><?= isset($row->style) ? $row->style: '' ?></td>
                                 <td><?= isset($row->price) ? $row->price: 'N/A' ?></td>
                                 <td><?= $row->in_stock == 1? '<span class="badge badge-success"> Yes</span>': '<span class="badge badge-secondary"> No</span>' ?></td>
                                 <td><?= $row->is_active == 1 ? '<span class="badge badge-success">active</span>': '<span class="badge badge-danger">inactive</span>' ?></td>
                                 <td width="10%">
                                     <div class="btn-group btn-sm">
                                         <a href="javascript:void(0);" @click="editItem($event)" data-id="<?= $row->product_id ?>" class="btn btn-xs btn-success"><i class="fa fa-pen"></i> </a>
                                         <a href="<?= site_url('products/reviews/'.$row->product_id) ?>" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> </a>
                                         <?php if ($row->in_stock == 1) : ?>
                                             <a href="javascript:void(0);" @click="toggleStock($event)" data-id="<?= $row->product_id ?>" class="btn btn-xs btn-secondary"><i class="fa fa-thumbs-up"></i> </a>
                                         <?php else: ?>
                                             <a href="javascript:void(0);" @click="toggleStock($event)" data-id="<?= $row->product_id ?>" class="btn btn-xs btn-secondary"><i class="fa fa-thumbs-down"></i> </a>
                                        <?php endif; ?>
                                         <?php if ($row->is_active) : ?>
                                             <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->product_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-times"></i> </a>
                                         <?php else: ?>
                                             <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->product_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> </a>
                                        <?php endif; ?>
                                            <a href="javascript:void(0);" @click="deleteItem($event)" data-id="<?= $row->product_id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                                     </div>
                                 </td>
                               </tr>
                           <?php endforeach; else: ?>
                               <tr>
                                 <td colspan="7"> No users exist at the moment </td>
                               </tr>
                           <?php endif;?>
                             </tbody>
                           </table>
                    </div>
                 </div>
               </div>
             </div>
           </div>
         </section>
<script src="<?= base_url() ?>assets/js/vue.min.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="<?= base_url() ?>assets/js/image-input.js" charset="utf-8"></script>
<script type="text/javascript">
var app = new Vue({
    el : '#app',
    data: {
        Item: {
            product_name:'',
            product_id: 0,
            description: '',
            price: 0,
            discount: 0,
            category_id: 0,
            style_id:0,
            is_active: 1,
            is_customizable: 1,
            in_stock: 1,
            image: ''
        },
        myFile: {
            productImage1:null,
            productImage2:null
        },
        categories:[],
        styles:[]
    },
    created(){
        this.getCategories();
        this.getStyles();
    },
    methods: {
        reset()
        {
            return {
                product_name:'',
                product_id: 0,
                description: '',
                price: 0,
                discount: 0,
                category_id: 0,
                style_id:0,
                is_active: 1,
                is_customizable: 1,
                in_stock: 1,
                image: ''
            }
        },

        submit()
        {
            let app = this;
            formRequest  = new FormData();
            formRequest.append('product_name', app.Item.product_name);
            formRequest.append('description', app.Item.description);
            formRequest.append('price', app.Item.price);
            formRequest.append('discount', app.Item.discount);
            formRequest.append('style_id', app.Item.style_id);
            formRequest.append('category_id', app.Item.category_id);
            formRequest.append('is_customizable', app.Item.is_customizable);
            formRequest.append('in_stock', app.Item.in_stock);
            formRequest.append('is_active', app.Item.is_active);
            formRequest.append('product', app.Item.product);
            if (app.myFile.productImage1 != null )
            {
                formRequest.append('productImage1', app.myFile.productImage1.imageFile);
            }
            if (app.myFile.productImage2 != null )
            {
                formRequest.append('productImage2', app.myFile.productImage2.imageFile);
            }

            if ( app.Item.product_id > 0 )
            {
                axios.post('<?= site_url() ?>products/put_product/'+ app.Item.product_id, formRequest).then((response) => {
                    console.log(response);
                    $.growl.notice({ 'message': "item was updated successfully."});
                    app.Item = this.reset();
                    location.reload();
                }).catch(e => console.log(e));
            }else{
                axios.post('<?= site_url() ?>products/post_product', formRequest ).then((response) => {
                    console.log(response);
                    $.growl.notice({ 'message': "item was created successfully."});
                    app.Item = this.reset();
                    location.reload();
                }).catch(e => console.log(e));
            }

        },
        editItem(e)
        {
            let id;
            let app = this;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.get('<?= base_url() ?>products/single/'+id).then((response) => {
                console.log(response.data);
                if ( response.status == 200 )
                {
                    this.Item = response.data.data;
                    this.myFile.productImage1 = { imageURL:response.data.data.product_image_1};
                    this.myFile.productImage2 = {imageURL:response.data.data.product_image_2};
                }
            }).catch(e => console.log(e));
        },

        deleteItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>products/trash/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was deleted successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        },

        toggleActivation(e)
        {
            let status = $(e.target).closest('tr').children()[4];
            console.log($(status).find('span').text());
            if ( $(status).find('span').text() == 'active' )
            {
                this.deactivateItem(e);
            }else{
                this.activateItem(e);
            }
        },

        deactivateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>products/deactivate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[4];
                    $.growl.notice({ 'message': "item was deactivated successfully."});
                    $(badge_parent).find('span').removeClass("badge-success").addClass("badge-danger").text('inactive');
                    $(e.target).closest('tr').children().find('i.fa-times').removeClass('fa-times').addClass('fa-check');
                }
            }).catch(e => console.log(e));
        },

        activateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>products/activate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[4];
                    $.growl.notice({ 'message': "item was activated successfully."});
                    $(badge_parent).find('span').removeClass("badge-danger").addClass("badge-success").text('active');
                    $(e.target).closest('tr').children().find('i.fa-check').removeClass('fa-check').addClass('fa-times');
                }
            }).catch(e => console.log(e));
        },

        toggleStock(e)
        {
            let status = $(e.target).closest('tr').children()[3];
            if ( $(status).find('span').text() == "Yes" )
            {
                this.outOfStock(e);
            }else{
                this.InStock(e);
            }
        },

        outOfStock(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>products/outOfStock/'+id).then((response) => {
                 badge_parent = $(e.target).closest('tr').children()[3];
                $.growl.notice({ 'message': "item was unfeatured successfully."});
                $(badge_parent).find('span').removeClass("badge-success").addClass("badge-secondary").text('No');
                $(e.target).closest('tr').children().find('i.fa-thumbs-up').removeClass('fa-thumbs-up').addClass('fa-thumbs-down');

            }).catch(e => console.log(e));
        },

        InStock(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>products/instock/'+id).then((response) => {
                 badge_parent = $(e.target).closest('tr').children()[3];
                $.growl.notice({ 'message': "item was unfeatured successfully."});
                $(badge_parent).find('span').removeClass("badge-success").addClass("badge-secondary").text('No');
                $(e.target).closest('tr').children().find('i.fa-thumbs-up').removeClass('fa-thumbs-up').addClass('fa-thumbs-down');

            }).catch(e => console.log(e));
        },

        getCategories()
        {
            let app =this;
            axios.get('<?= base_url() ?>categories/get_categories').then((response) => {
                console.log(response);
                if ( response.status == 200 )
                {
                    app.categories = response.data.data;
                }
            }).catch(e => console.log(e));
        },
        getStyles()
        {
            let app =this;
            axios.get('<?= base_url() ?>styles/get_styles').then((response) => {
                console.log(response);
                if ( response.status == 200 )
                {
                    app.styles = response.data.data;
                }
            }).catch(e => console.log(e));
        }
    }
})
</script>
