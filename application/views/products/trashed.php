<section class="py-5" id="app">
    <?= show_message($this->session->flashdata('success'), $this->session->flashdata('error')); ?>
           <div class="row">
             <div class="col-lg-12 mb-4">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0"><?= isset($title) ? $title : '' ?></h6>
                 </div>
                 <div class="card-body">
                     <div class="table-responsive">
                           <table class="table card-text  table-sm">
                             <thead>
                               <tr>
                                 <th>&nbsp;</th>
                                 <th class="w-150">Product</th>
                                 <th class="w-200">Description</th>
                                 <th class="w-150">Category</th>
                                 <th class="w-100">Style</th>
                                 <th>Price</th>
                                 <th>In stock</th>
                                 <th>Status</th>
                                 <th width="70px">Action</th>
                               </tr>
                             </thead>
                             <tbody>
                            <?php if( !empty($data) && $data->num_rows() > 0 ): $i = 1; foreach($data->result() as $row): ?>
                               <tr>
                                 <td><img src="<?= base_url().(isset($row->product_image_1) ? $row->product_image_1: '') ?>" width="40" height="40"/></td>
                                 <td><?= isset($row->product_name) ? $row->product_name: '' ?></td>
                                 <td ><?= isset($row->description) ? word_limiter($row->description, 9): '' ?></td>
                                 <td><?= isset($row->category) ? $row->category: '' ?></td>
                                 <td><?= isset($row->style) ? $row->style: '' ?></td>
                                 <td><?= isset($row->price) ? $row->price: 'N/A' ?></td>
                                 <td><?= $row->in_stock == 1? '<span class="badge badge-success"> Yes</span>': '<span class="badge badge-secondary"> No</span>' ?></td>
                                 <td><?= $row->is_active == 1 ? '<span class="badge badge-success">active</span>': '<span class="badge badge-danger">inactive</span>' ?></td>
                                 <td width="10%">
                                     <div class="btn-group btn-sm">
                                         <a href="javascript:void(0);" @click="restoreItem($event)" data-id="<?= $row->product_id ?>" class="btn btn-xs btn-success"><i class="fa fa-undo"></i> </a>
                                         <a href="javascript:void(0);" @click="deleteItem($event)" data-id="<?= $row->product_id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                                     </div>
                                 </td>
                               </tr>
                           <?php endforeach; else: ?>
                               <tr>
                                 <td colspan="7"> Trash can is clean </td>
                               </tr>
                           <?php endif;?>
                             </tbody>
                           </table>
                    </div>
                 </div>
               </div>
             </div>
           </div>
         </section>
<script src="<?= base_url() ?>assets/js/vue.min.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="<?= base_url() ?>assets/js/image-input.js" charset="utf-8"></script>
<script type="text/javascript">
var app = new Vue({
    el : '#app',
    methods: {

        deleteItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>products/delete/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was deleted successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        },
        restoreItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>products/restore/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was restored successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        }
    }
})
</script>
