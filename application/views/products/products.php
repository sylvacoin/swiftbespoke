<section class="features">
                    <div class="container">
                        <div class="row">
                              <div class="col-12">
                                   <div class="section-title text-center">
                                        <h1 class="heading">
                                             Best tailor made Shirts ever
                                        </h1>
                                        <p><small>Created to your specific body measurement from the earths finest fabrics. Now enjoy our wardrobe.</small></p>
										<div class="filter row">
											<div class="col-sm-5">
												<select name="product" class="form-control custom-select">
													<option value=""> Category 1</option>
												</select>
											</div>
											<div class="col-sm-5">
												<select name="style" class="form-control custom-select">
												<option value=""> Style 1</option>
												</select>
											</div>
											<div class="col-sm-2">
												<a href="#" class="btn btn-filter"> Filter </a>
											</div>
									   </div>

                                   </div>

                              </div>
						</div>
						<div class="row">
                              <div class="col-12">
                                   <div class="grid row">
                                       <?php if ( isset($products) && count($products) > 0 ): foreach($products as $product): ?>
                                        <div class="grid-item grid-25">

                                             <figure class="effect-sadie"><img src="<?= base_url().(isset($product->product_image_1) ? $product->product_image_1: DEFAULT_CATEGORY_IMG) ?>" alt="">
                                                  <figcaption>
                                                       <h2><?= (isset($product->product_name) ? $product->product_name: '')  ?></h2>
                                                       <a href="<?= site_url('customizer/customize/'.$product->product_id.'/'.url_title($product->product_name)) ?>" class="btn">View more</a>
													   <p>NGN <?= $product->price ?> <br> Click to customize product</p>
                                                  </figcaption>
                                             </figure>

                                        </div>
                                    <?php endforeach; endif; ?>
                                   </div>

                              </div>


                         </div>
                    </div>
               </section>
