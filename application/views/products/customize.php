<section class="customizer">
                    <div class="container">
                         <div class="row">
                              <div class="col-12">
                                   <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                             <li class="breadcrumb-item"><a href="#">Home</a></li>
                                             <li class="breadcrumb-item active" aria-current="page">Product Details</li>
                                        </ol>
                                   </nav>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-sm-8 ">
                                <form id="form-customizer" name="formCustomizer">
                                    <input type="hidden" name="product" value="<?= $product->product_id ?>">
                                   <nav class="nav nav-pills flex-column flex-sm-row">

                                       <?php if ( isset($attributes) ): $i =1; foreach( $attributes as $k => $attr): ?>
                                        <a class="nav-link <?= $i == 1 ? 'active':'' ?>" data-toggle="tab" href="#attribute_<?= $i++ ?>" role="tab" aria-controls="tab_<?= $i++ ?>" aria-selected="true"><?= $attr['attribute'] ?></a>
                                    <?php endforeach; endif; ?>
                                    <a class="nav-link" data-toggle="tab" href="#attribute_measurement" role="tab" aria-controls="tab_measurement" aria-selected="true"> Measurement </a>
                                   </nav>
                                   <div class="float-right pre-nxt">
                                        <ul>
                                             <li class="pre"><a onclick="next_pre('1', '9')">Prev</a></li>
                                             <li class="nxt"><a onclick="next_pre('2', '9')">Next</a></li>
                                        </ul>
                                   </div>

                                   <div class="tab-content" id="myTabContent">
                                       <?php if ( isset($attributes) ): $i = 1; foreach( $attributes as $k => $attr): ?>
                                        <div class="tab-pane fade <?= $i == 1 ? 'show active':'' ?>" id="attribute_<?= $i++ ?>" role="tabpanel" aria-labelledby="tab_<?= $i++ ?>-tab">
                                             <h2><?= $attr['attribute'] ?></h2>
                                             <div class="row">
                                                  <div class="col-12 col-sm-3">
                                                      <!-- vertical Tabs -->
                                                       <div class="nav flex-column nav-pills stitchTabs" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                       <?php if( isset($attr['sub_attributes']) && count($attr['sub_attributes'])): $a = 1; foreach( $attr['sub_attributes'] as $s => $sub_attr): ?>
                                                            <a class="nav-link stitchfun <?= $a == 1 ? 'active':'' ?>"
                                                                id="sub_tab-<?= $a ?>"
                                                                data-id="<?= $sub_attr['sub_attribute_id'] ?>"
                                                                data-view="<?= $sub_attr['attribute_view'] == 'back' ? 1 : 0 ?>"
                                                                data-bk-img="<?= base_url().$category->back_preview ?>"
                                                                data-frnt-img="<?= base_url().$category->front_preview ?>"
                                                                data-name="<?= isset($sub_attr['sub_attribute']) ? underscore($sub_attr['sub_attribute']):'' ?>"
                                                                data-toggle="pill" href="#rel_<?= $sub_attr['sub_attribute_id'] ?>" role="tab"
                                                                aria-controls="sub-<?= $a ?>-tab" aria-selected=" <?= $a == 1 ? 'true':'false' ?>"
                                                                rel="rel_<?= $sub_attr['sub_attribute_id'] ?>"
                                                                ><?= isset($sub_attr['sub_attribute']) ? $sub_attr['sub_attribute']:'' ?></a>
                                                        <?php $a++; endforeach;endif; ?>
                                                       </div>
                                                       <!-- //endof vertical tabs -->
                                                  </div>
                                                  <div class="col-12 col-sm-9">
                                                       <div class="tab-content" id="v-pills-tabContent">
                                                           <?php if( isset($attr['sub_attributes']) && count($attr['sub_attributes'])): $s = 0; foreach( $attr['sub_attributes'] as $s => $sub_attr): ?>
                                                            <div class="tab-pane fade <?= ++$s == 1 ? 'show active':'' ?>" id="rel_<?= $sub_attr['sub_attribute_id'] ?>" role="tabpanel" aria-labelledby="sub-<?= $k ?>-tab">
                                                                <?php if ( $sub_attr['pocket_side_display'] ): ?>
                                                                <div class="pattern-view w-100">
                                                                    <span>Left Side</span><input type="radio" name="attrPattern[<?= $product->product_id ?>][<?= $sub_attr['attribute_id'] ?>][<?= $sub_attr['sub_attribute_id']  ?>]" value="1" checked="true" onclick="loadStitchView()">
                                                                    <span>Right Side</span><input type="radio" name="attrPattern[<?= $product->product_id ?>][<?= $sub_attr['attribute_id'] ?>][<?= $sub_attr['sub_attribute_id']  ?>]" value="2" onclick="loadStitchView()">
                                                                    <span>Both Side</span><input type="radio" name="attrPattern[<?= $product->product_id ?>][<?= $sub_attr['attribute_id'] ?>][<?= $sub_attr['sub_attribute_id']  ?>]" value="3" onclick="loadStitchView()">
                                                                </div>
                                                                <?php endif; ?>
                                                                 <div class="parts-imgs">
                                                                      <input type="hidden" name="attr[<?= $sub_attr['attribute_id'] ?>][<?= $sub_attr['sub_attribute_id'] ?>]" id="attr_option_value_<?= $sub_attr['sub_attribute_id'] ?>" value="">

                                                                      <ul class="cl_for_click stitchAttr">

                                                                          <?php if( isset($sub_attr['options']) && count($sub_attr['options'])): $o = 0; foreach( $sub_attr['options'] as $opt => $option): ?>
                                                                           <li class="<?= ++$o == 1 ? 'active':' ' ?>"
                                                                               data-attr="<?= $sub_attr['sub_attribute_id'] ?>"
                                                                               data-option="<?= $option->option_id ?>"
                                                                               data-bg-image="<?= base_url(). (isset($option->image_background) ? $option->image_background:DEFAULT_CATEGORY_IMG) ?>"
                                                                               data-bg-image-right="" data-bg-image-both="" data-text="<?= $option->option_name; ?>"
                                                                               data-fld="selection">
                                                                               <a class="a_cursor" title="<?= $option->option_name ?>" alt="<?= $option->option_name ?>">
                                                                                     <div class="colr-img"><img src="<?= base_url(). (isset($option->image_thumb) ? $option->image_thumb:DEFAULT_CATEGORY_IMG) ?>"></div>
                                                                                     <span><?= character_limiter($option->option_name, 10) ?></span>
                                                                                </a>
                                                                                <div id="opt_det_1" style="display:none;">
                                                                                     <div class="pop_data"><?= $option->option_name ?></div>
                                                                                </div>
                                                                           </li>
                                                                           <?php endforeach;endif; ?>
                                                                      </ul>
                                                                      <div class="acc-container1">
                                                                      </div>
                                                                 </div>

                                                            </div>
                                                            <?php endforeach;endif; ?>
                                                       </div>

                                                  </div>
                                             </div>
                                        </div>
                                        <?php endforeach; endif; ?>
                                        <div class="tab-pane fade" id="attribute_measurement" role="tabpanel" aria-labelledby="tab_measurement-tab">
                                             <h2> Measurements </h2>
                                             <div class="row">
                                                  <div class="col-12 col-sm-12">

                                                      <div class="container">
                                                      	<div class="wrapper">
                                                      		<div class="toggle_radio">
                                                      			<input type="radio" class="toggle_option" id="first_toggle" name="toggle_option" value="custom">
                                                      			<input type="radio" checked class="toggle_option" id="second_toggle" name="toggle_option" value="standard">

                                                      			<label for="first_toggle">
                                                      				<p>Body Size</p>
                                                      			</label>
                                                      			<label for="second_toggle">
                                                      				<p>Standard Measurement</p>
                                                      			</label>

                                                      			<div class="toggle_option_slider"></div>
                                                      		</div>
                                                      		<div class="row py-5" id="custom">

                                                      			<div class="col-sm-7">
                                                                    <?php if (isset($custom_measurements) ): foreach ($custom_measurements  as $key => $measurement): ?>
                                                                        <div class="row">
                                                                                <div class="offset-2">

                                                                                </div>
                                                                                <span class="label label-default col-sm-4"><?= $measurement->measurement_type ?></span>
                                                                                <input type="number"
                                                                                name="measurement[<?= $measurement->measurement_id ?>]" value=""
                                                                                min="<?= $measurement->min_range ?>"
                                                                                max="<?= $measurement->max_range ?>"
                                                                                data-url="<?= $measurement->video_url ?>"
                                                                                class="vslot" required>
                                                                                <span class='col-sm-2 text-center'>
                                                                                    (range)
                                                                                <?= $measurement->min_range ?> - <?= $measurement->max_range ?>
                                                                                </span>
                                                                        </div>
                                                                    <?php endforeach; endif; ?>
                                                                    <div class="row">
                                                                        <div class="offset-2"></div>
                                                                        <span class="label label-default col-sm-4">Quantity</span>
                                                                        <select name="custom_quantity" required class="col-sm-4">
                                                                            <option value="1">1</option>
                                                                            <option value="2">2</option>
                                                                            <option value="3">3</option>
                                                                            <option value="4">4</option>
                                                                            <option value="5">5</option>
                                                                            <option value="6">6</option>
                                                                            <option value="7">7</option>
                                                                            <option value="8">8</option>
                                                                            <option value="9">9</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                            <option value="13">13</option>
                                                                            <option value="14">14</option>
                                                                            <option value="15">15</option>
                                                                            <option value="16">16</option>
                                                                            <option value="17">17</option>
                                                                            <option value="18">18</option>
                                                                            <option value="19">19</option>
                                                                            <option value="20">20</option>
                                                                            <option value="21">21</option>
                                                                            <option value="22">22</option>
                                                                            <option value="23">23</option>
                                                                            <option value="24">24</option>
                                                                            <option value="25">25</option>
                                                                            <option value="26">26</option>
                                                                            <option value="27">27</option>
                                                                            <option value="28">28</option>
                                                                            <option value="29">29</option>
                                                                            <option value="30">30</option>
                                                                            <option value="31">31</option>
                                                                            <option value="32">32</option>
                                                                            <option value="33">33</option>
                                                                            <option value="34">34</option>
                                                                            <option value="35">35</option>
                                                                            <option value="36">36</option>
                                                                            <option value="37">37</option>
                                                                            <option value="38">38</option>
                                                                            <option value="39">39</option>
                                                                            <option value="40">40</option>
                                                                            <option value="41">41</option>
                                                                            <option value="42">42</option>
                                                                            <option value="43">43</option>
                                                                            <option value="44">44</option>
                                                                            <option value="45">45</option>
                                                                            <option value="46">46</option>
                                                                            <option value="47">47</option>
                                                                            <option value="48">48</option>
                                                                            <option value="49">49</option>
                                                                            <option value="50">50</option>
                                                                            <option value="51">51</option>
                                                                            <option value="52">52</option>
                                                                            <option value="53">53</option>
                                                                            <option value="54">54</option>
                                                                            <option value="55">55</option>
                                                                            <option value="56">56</option>
                                                                            <option value="57">57</option>
                                                                            <option value="58">58</option>
                                                                            <option value="59">59</option>
                                                                            <option value="60">60</option>
                                                                            <option value="61">61</option>
                                                                            <option value="62">62</option>
                                                                            <option value="63">63</option>
                                                                            <option value="64">64</option>
                                                                            <option value="65">65</option>
                                                                            <option value="66">66</option>
                                                                            <option value="67">67</option>
                                                                            <option value="68">68</option>
                                                                            <option value="69">69</option>
                                                                            <option value="70">70</option>
                                                                            <option value="71">71</option>
                                                                            <option value="72">72</option>
                                                                            <option value="73">73</option>
                                                                            <option value="74">74</option>
                                                                            <option value="75">75</option>
                                                                            <option value="76">76</option>
                                                                            <option value="77">77</option>
                                                                            <option value="78">78</option>
                                                                            <option value="79">79</option>
                                                                            <option value="80">80</option>
                                                                            <option value="81">81</option>
                                                                            <option value="82">82</option>
                                                                            <option value="83">83</option>
                                                                            <option value="84">84</option>
                                                                            <option value="85">85</option>
                                                                            <option value="86">86</option>
                                                                            <option value="87">87</option>
                                                                            <option value="88">88</option>
                                                                            <option value="89">89</option>
                                                                            <option value="90">90</option>
                                                                            <option value="91">91</option>
                                                                            <option value="92">92</option>
                                                                            <option value="93">93</option>
                                                                            <option value="94">94</option>
                                                                            <option value="95">95</option>
                                                                            <option value="96">96</option>
                                                                            <option value="97">97</option>
                                                                            <option value="98">98</option>
                                                                            <option value="99">99</option>
                                                                            <option value="100">100</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                      			<div class="col-sm-5" id="vurl-slot-1">
                                                                    <video controls preload="metadata" autoplay loop height="168">
                                                                        <source src="" type="video/ogv">
                                                      					<!-- <source id="vurl-slot" src="http://mirror.cessen.com/blender.org/peach/trailer/trailer_iphone.m4v" type="video/mp4"> -->
                                                      					<source src="mov_bbb.ogg" type="video/ogg">
                                                      					Your browser does not support HTML5 video.
                                                      				</video>
                                                                </div>
                                                                <div class="col-sm-12 mt-3">

                                                                </div>
                                                      		</div>
                                                            <div class="row" id="standard" data-mfield-options='{"section": ".multi","btnAdd":".multi-add","btnRemove":".multi-remove"}'>
                                                                <div class="col-sm-12 mt-3">
                                                                    <table class="table table-bordered" id="table-size-inch" style="display: table;">
                                                                    	<tbody>
                                                                    		<tr>
                                                                    			<td data-lang="size">Size</td>
                                                                    			<td>s</td>
                                                                    			<td>m</td>
                                                                    			<td>l</td>
                                                                    			<td>xl</td>
                                                                    			<td>xxl</td>
                                                                    			<td>3xl</td>
                                                                    			<td>4xl</td>
                                                                    		</tr>
                                                                    		<tr>
                                                                    			<td data-lang="neck">Neck</td>
                                                                    			<td>15</td>
                                                                    			<td>16</td>
                                                                    			<td>16.5</td>
                                                                    			<td>17</td>
                                                                    			<td>18</td>
                                                                    			<td>19</td>
                                                                    			<td>20</td>
                                                                    		</tr>
                                                                    		<tr>
                                                                    			<td data-lang="chest">Chest</td>
                                                                    			<td>35-38</td>
                                                                    			<td>38-41</td>
                                                                    			<td>41-44</td>
                                                                    			<td>44-46</td>
                                                                    			<td>46-48</td>
                                                                    			<td>49-51</td>
                                                                    			<td>52-54</td>
                                                                    		</tr>
                                                                    	</tbody>
                                                                    </table>
                                                                </div>

                                                                <div class="container">
                                                                    <div class="row px-5">
                                                                        <button class="btn btn-primary btn-sm multi-add mt-4"> Add more sizes</button>
                                                                    </div>
                                                                </div>
                                                                <div class="row multi px-5">
                                                                    <div class="col-sm-5">
                                                                        <span class="label label-default">Select your size</span>
                                                                        <select class="custom-form-control" name="measurements[]" required>
                                                                            <option value="--" disabled selected>--</option>
                                                                            <option value="S">S</option>
                                                                            <option value="M">M</option>
                                                                            <option value="L">L</option>
                                                                            <option value="XL">XL</option>
                                                                            <option value="XXL">XXL</option>
                                                                            <option value="3XL">3XL</option>
                                                                            <option value="4XL">4XL</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <span class="label label-default">Select quantity</span>
                                                                        <select class="custom-form-control" name="standard_quantity[]" required>
                                                                            <option value="1">1</option>
                                                                            <option value="2">2</option>
                                                                            <option value="3">3</option>
                                                                            <option value="4">4</option>
                                                                            <option value="5">5</option>
                                                                            <option value="6">6</option>
                                                                            <option value="7">7</option>
                                                                            <option value="8">8</option>
                                                                            <option value="9">9</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                            <option value="13">13</option>
                                                                            <option value="14">14</option>
                                                                            <option value="15">15</option>
                                                                            <option value="16">16</option>
                                                                            <option value="17">17</option>
                                                                            <option value="18">18</option>
                                                                            <option value="19">19</option>
                                                                            <option value="20">20</option>
                                                                            <option value="21">21</option>
                                                                            <option value="22">22</option>
                                                                            <option value="23">23</option>
                                                                            <option value="24">24</option>
                                                                            <option value="25">25</option>
                                                                            <option value="26">26</option>
                                                                            <option value="27">27</option>
                                                                            <option value="28">28</option>
                                                                            <option value="29">29</option>
                                                                            <option value="30">30</option>
                                                                            <option value="31">31</option>
                                                                            <option value="32">32</option>
                                                                            <option value="33">33</option>
                                                                            <option value="34">34</option>
                                                                            <option value="35">35</option>
                                                                            <option value="36">36</option>
                                                                            <option value="37">37</option>
                                                                            <option value="38">38</option>
                                                                            <option value="39">39</option>
                                                                            <option value="40">40</option>
                                                                            <option value="41">41</option>
                                                                            <option value="42">42</option>
                                                                            <option value="43">43</option>
                                                                            <option value="44">44</option>
                                                                            <option value="45">45</option>
                                                                            <option value="46">46</option>
                                                                            <option value="47">47</option>
                                                                            <option value="48">48</option>
                                                                            <option value="49">49</option>
                                                                            <option value="50">50</option>
                                                                            <option value="51">51</option>
                                                                            <option value="52">52</option>
                                                                            <option value="53">53</option>
                                                                            <option value="54">54</option>
                                                                            <option value="55">55</option>
                                                                            <option value="56">56</option>
                                                                            <option value="57">57</option>
                                                                            <option value="58">58</option>
                                                                            <option value="59">59</option>
                                                                            <option value="60">60</option>
                                                                            <option value="61">61</option>
                                                                            <option value="62">62</option>
                                                                            <option value="63">63</option>
                                                                            <option value="64">64</option>
                                                                            <option value="65">65</option>
                                                                            <option value="66">66</option>
                                                                            <option value="67">67</option>
                                                                            <option value="68">68</option>
                                                                            <option value="69">69</option>
                                                                            <option value="70">70</option>
                                                                            <option value="71">71</option>
                                                                            <option value="72">72</option>
                                                                            <option value="73">73</option>
                                                                            <option value="74">74</option>
                                                                            <option value="75">75</option>
                                                                            <option value="76">76</option>
                                                                            <option value="77">77</option>
                                                                            <option value="78">78</option>
                                                                            <option value="79">79</option>
                                                                            <option value="80">80</option>
                                                                            <option value="81">81</option>
                                                                            <option value="82">82</option>
                                                                            <option value="83">83</option>
                                                                            <option value="84">84</option>
                                                                            <option value="85">85</option>
                                                                            <option value="86">86</option>
                                                                            <option value="87">87</option>
                                                                            <option value="88">88</option>
                                                                            <option value="89">89</option>
                                                                            <option value="90">90</option>
                                                                            <option value="91">91</option>
                                                                            <option value="92">92</option>
                                                                            <option value="93">93</option>
                                                                            <option value="94">94</option>
                                                                            <option value="95">95</option>
                                                                            <option value="96">96</option>
                                                                            <option value="97">97</option>
                                                                            <option value="98">98</option>
                                                                            <option value="99">99</option>
                                                                            <option value="100">100</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <button class="btn btn-danger btn-sm multi-remove mt-4"> remove</button>
                                                                    </div>
                                                                </div>
                                                      		</div>
                                                            <div class="row mt-5">
                                                                 <div class="col-sm-12">
                                                                      <button type="button" class="btn btn-success cd-add-to-cart">Add to cart</button>
                                                                      <button class="btn btn-success" id="btn-prev" type="button">Add to favorites</button>
                                                                 </div>
                                                            </div>
                                                        </div>
                                                      </div>



                                                  </div>
                                             </div>
                                        </div>


                                   </div>
                               </form>
                              </div>
                              <div class="col-sm-4">
                                   <div id="stitch_display" class="zoom-shirt img-responsive stich_box" style="background-image: url('<?= base_url().(isset($category->front_preview) ? $category->front_preview:'') ?>');">
                                       <?php if( isset($sub_attributes) && count($sub_attributes) > 0 ): foreach($sub_attributes as $subAttrImage): ?>
                                        <div class="stitch-box" id="image_<?= underscore($subAttrImage->sub_attribute) ?>" style="background-image: none;"></div>
                                    <?php endforeach; endif; ?>
                                   </div>
                              </div>
                         </div>
                    </div>
       </section>
               <section class="features">
                    <div class="container">
                         <div class="row">
                              <div class="col-12">
                                   <div class="section-title text-center">
                                        <h1 class="heading">
                                             Other Products
                                        </h1>
                                        <p><small>Created to your specific body measurement from the earths finest fabrics. Now enjoy our wardrobe.</small></p>
                                   </div>

                              </div>
                         </div>
                         <div class="row">
                              <div class="col-12">
                                   <div class="grid row">
                                        <div class="grid-item grid-25">

                                             <figure class="effect-sadie"><img src="<?= base_url() ?>./assets/products/1.jpg" alt="">
                                                  <figcaption>
                                                       <h2>Product Name</h2>
                                                       <a href="#" class="btn">View more</a>
                                                       <p>NGN2000 <br>Click to customize </p>
                                                  </figcaption>
                                             </figure>

                                        </div>
                                        <div class="grid-item grid-25">

                                             <figure class="effect-sadie"><img src="<?= base_url() ?>./assets/products/2.jpg" alt="">
                                                  <figcaption>
                                                       <h2>Product Name</h2>
                                                       <a href="#" class="btn">View more</a>
                                                       <p>NGN2000</p>
                                                  </figcaption>
                                             </figure>

                                        </div>
                                        <div class="grid-item grid-25">

                                             <figure class="effect-sadie">
                                                  <img src="<?= base_url() ?>./assets/products/3.jpg" alt="">
                                                  <figcaption>
                                                       <h2>Product Name</h2>
                                                       <a href="#" class="btn">View more</a>
                                                       <p>NGN2000</p>
                                                  </figcaption>

                                                  </a>
                                        </div>
                                        <div class="grid-item grid-25">
                                             <figure class="effect-sadie"><img src="<?= base_url() ?>./assets/products/4.jpg" alt="">
                                                  <figcaption>
                                                       <h2>Product Name</h2>
                                                       <a href="#" class="btn">View more</a>
                                                       <p>NGN2000</p>
                                                  </figcaption>
                                             </figure>
                                        </div>
                                   </div>

                              </div>


                         </div>
                    </div>
               </section>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js" charset="utf-8"></script>
<script src="<?= base_url() ?>assets/js/jquery.multifield.min.js" charset="utf-8"></script>

<script type="text/javascript">
function backStitchView(img) {
 $('#stitch_display').css('background-image', 'url(' + img + ')');
 setStitchImages(1);
}

function frontStitchView(img)
{
 $('#stitch_display').css('background-image', 'url(' + img + ')');
 setStitchImages(0);
}

$('.nav-link.stitchfun').on('click', function (e) {
  e.preventDefault();
  loadStitchView(this);
})

function setStitchImages(disp)
{
 $('.stitchTabs a').each(function(i){
     var view= parseInt( $(this).attr('data-view') );
     var type=$(this).attr('data-type');
     var name=$(this).attr('data-name');
     var rel=$(this).attr('rel');
     //console.log($("#"+rel+" .stitchAttr li"));
     if($("#"+rel+" .stitchAttr li").hasClass('active')){
         var patrn=$("#"+rel+" .pattern-view input[type=radio]:checked").val();
         var op=$("#"+rel+" .stitchAttr li.active").attr('data-bg-image');
     }else{
         var patrn=$("#"+rel+" .pattern-view input[type=radio]:first").val();
         var op=$("#"+rel+" .stitchAttr li").first().attr('data-bg-image');
     }

     if(typeof patrn!='undefined' && patrn!=''){
         //$("#"+rel+" .pattern-view .attr_selection_pattern").val(patrn);
         if(patrn=='2'){
             var op=$("#"+rel+" .stitchAttr li.active").attr('data-bg-image-right');
         }else if(patrn=='3'){
             var op=$("#"+rel+" .stitchAttr li.active").attr('data-bg-image-both');
         }else{
             var op=$("#"+rel+" .stitchAttr li.active").attr('data-bg-image');
         }
     }

     if( typeof op != 'undefined' && op!='' && (view==disp || view==2)){

         $('#image_'+name).css('background-image', 'url(' + op + ')');
     }else{

         $('#image_'+name).css('background-image', 'none');
     }
 });
}

function loadStitchView( instance = null )
{
    if (instance == null)
    {
        var active=$(".stitchTabs a.active").attr('data-view');
        var frnt_img=$(".stitchTabs a.active").attr('data-frnt-img');
        var bk_img=$(".stitchTabs a.active").attr('data-bk-img');
    }else{
        var active=$(instance).attr('data-view');
        var frnt_img=$(instance).attr('data-frnt-img');
        var bk_img=$(instance).attr('data-bk-img');
    }

 if(active==1){
     backStitchView(bk_img);
     //toImage();
 }else{
     frontStitchView(frnt_img);
     //toImage();
 }
 return false;
}

$(document).ready(function(){
    $("div#custom").hide();
    $("#standard").multifield();
    loadStitchView();

    $('.toggle_option').change(function(){
        if ( $(this).prop('checked') )
        {
            let container = $(this).val();
            $('div#'+container).show('slow');
            $.each( $('.toggle_option'), function(){
                if ( ! $(this).prop('checked') )
                {
                    let div = $(this).val();
                    $('div#'+div).hide('slow');
                }
            } );
        }
    });
});
$('.vslot')[0].focus();
$('.vslot').focus(function(){
    let vurl = $(this).data('url');
    console.log(vurl);
    // .attr('type', 'video/ogv');
    // $('#vurl-slot-1').attr('src', vurl);
    // $('#vurl-slot-1')[0].load();
    var video = $('#vurl-slot-1 video')[0];
    video.src = vurl;
    video.load();
    video.play();

});

$('.cl_for_click li').click(function(){
	var el = $(this);

	el.siblings('li').removeClass('active');
	$('#attr_option_value_'+parseInt(el.attr('data-attr'))).val(parseInt(el.attr('data-option')));

	$('#custom_msg_div').html('');
	$(this).addClass('active');
	loadStitchView();
});

initCart();

function initCart()
{
    if (window.localStorage)
    {
        if (localStorage.cart == undefined)
        {
            localStorage.cart = JSON.stringify([]);
        }
    }else{
        alert('Your browser does not support this service');
    }
}

function addOption(id, subattr, option, price )
{
    var cart = JSON.parse(localStorage.cart);
    if (cart.length == 0 )
    {
        var product = {
            id: id,
            price: <?= $product->price ?>,
            quantity: 0,
            attributes: [
            ]
        };
        cart.push(product);
        localStorage.cart = JSON.stringify(cart);
    }
    for(c in cart){
        //if cart item exists
        if(cart[c].id == id){

            let attributes = cart[c].attributes;
            console.log(id, subattr, option, price);
            if (attributes !== undefined)
            {
                if (_.find(attributes, {id:subattr}))
                {
                    let index = _.findIndex(attributes, function(o){ return o.id == subattr });
                    cart[c].attributes[index].option = option;
                    cart[c].attributes[index].price = price;
                }else{
                    cart[c].attributes.push({
                        id:subattr,
                        option: option,
                        price: price
                    });
                }

            }else{
                alert('Here');
                //if attributes dont exist add attribute
                console.log(cart[c].id, id, cart[c].id == id);
                cart[c].attributes.push({
                    id:subattr,
                    option: option,
                    price: price
                });
            }

        }
    }
    localStorage.cart=JSON.stringify(cart);
    return;
}
//
// function addToCart(el){
//
// 	/*if(window.adding_in_progress == true) return false;*/
// 	var logged_user_id = $('#logged_user_id').val();
// 	if(logged_user_id == ''){
// 		$('#right-shrt-customize').show();
// 		$('#right-shrt-product-slider').hide();
// 		showHideDivsForAddToCartForNotLoggedIn();
// 		return false;
// 	}
//
// 	window.adding_in_progress = true;
// 	$('.error').removeClass('error');
// 	showCssElementLoading($('#loaderfav').parent(), 1);
// 	if(validateCustomData(document.formCustomizer) === false || validateCustomData(document.formCustomizer) == 'undefined'){
// 		if(logged_user_id == ''){
// 			showHideDivsForAddToCartForNotLoggedIn();
// 		}else{
// 			openTab1('product-description', 'custamize-product');
// 			$('#right-shrt-customize').show();
// 			$('#customize_shrt').show();
// 			$('#right-shrt-product-slider').hide();
// 			$('.description').hide();
// 			$(".custamize-product").trigger('click');
// 		}
// 		$('#loader').remove();
// 		return false;
// 	}
//
//
//     return false;
// }

function toImage(){
    var element = $("#stitch_display"); // global variable
    var getCanvas; // global variable
    html2canvas(element, {
     onrendered: function (canvas) {
             var imgageData = canvas.toDataURL("image/png");
             var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
             if ($(".stitchTabs a.active").attr('data-view') == 1)
             {
                 // Now browser starts downloading it instead of just showing
                 $("#lilprev_front img").attr("src", newData);
              }else{
                  // Now browser starts downloading it instead of just showing it
                  $("#lilprev_back img").attr("src", newData);
              }
              console.log('alo');
              return false;
         }
     });
}

    //      var imgageData = getCanvas.toDataURL("image/png");
    // // Now browser starts downloading it instead of just showing it
    // var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
    // $("#btn-Convert-Html2Image").attr("download", "your_pic_name.png").attr("href", newData);
    //});


function showCssElementLoading(el, append){
	if(append == 1)
		el.append('<div id="loader"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>');
	else
		el.html('<div id="loader"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>');
}

function validateCustomData(frm){
	var isValid = true;
	$(frm).find('input.required').each(function(){
		if(this.type == 'text' && (typeof this.value == 'undefined' || this.value == '' || this.value == null)){
			isValid = false;
			$(this).addClass('error');
		}else if(typeof this.value == 'undefined' || this.value == '' || this.value == null || parseInt(this.value) < 1){
			isValid = false;
		}
		if(isValid === false){
			var tab_rel_id = $(this).parents('.parts-type:first').attr('id');
			var el_tab_a = $('#tabs a[rel="'+tab_rel_id+'"]');
			var el_obj = el_tab_a.parents('li:first');
			if(el_obj.hasClass('active') == false){
				el_tab_a.trigger('click');
			}
			$('#custom_msg_div').html('<div class="warning">Please Select '+el_obj.text()+'</div>');
			el_obj.addClass('error');
			$("html, body").animate({ scrollTop: 0 }, "slow");
			/* $('html, body').animate({
				scrollTop: $('#tabs').offset().top
			},'slow'); */
			return false;
		}

	});
	if(isValid === false) return false;
	return true;
}





</script>
