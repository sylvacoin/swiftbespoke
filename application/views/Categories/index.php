<section class="py-5" id="app">
    <?= show_message($this->session->flashdata('success'), $this->session->flashdata('error')); ?>
           <div class="row">
               <div class="col-lg-4 mb-5">
                 <div class="card">
                   <div class="card-body">
                     <form id="" @submit.prevent="submit">
                    <div class="form-group row">
                    <input v-model="Item.category" type="text" placeholder="Category" class="form-control input-md" required>
                    <input v-model="Item.category_id" type="hidden">
                    </div>
                    <div class="line"></div>

                    <!-- Select Basic -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Feature category on homepage?</label>
                        <select id="Select" v-model="Item.is_featured" class="form-control">
                         <option value="1">Yes</option>
                         <option value="0">No</option>
                        </select>
                    </div>
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="label-control col-sm-12">Gender</label>
                        <select id="Select" v-model="Item.gender" class="form-control">
                         <option value="male">Male</option>
                         <option value="female">Female</option>
                        </select>
                    </div>
                    <div class="line"></div>

                    <!-- Select Basic -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Is Active</label>
                        <select id="is_active" v-model="Item.is_active" class="form-control">
                         <option value="1">Active</option>
                         <option value="0">InActive</option>
                        </select>
                    </div>
                    <div class="line"></div>

                    <!-- File Button -->
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Product Display Image</label>
                        <image-input  v-model="myFile.displayImage" name="file">
                            <div slot="activator">
                                <div class="card w-50 mx-auto">
                                    <div class="card-body" v-if="!myFile.displayImage" style="width:100%; height:100%; padding: 0;" >
                                        <img class="img-fluid" src="<?= base_url() ?>assets/img/placeholder.jpg" alt="Alternate Text" />
                                    </div>
                                    <div class="card-image" v-else>
                                        <img class="img-fluid" :src="myFile.displayImage.imageURL" alt="Alternate Text" />
                                    </div>
                                </div>
                            </div>
                        </image-input>
                    </div>
                    <div class="line"></div>
                    <div class="form-group row">
                        <label class="label-control col-sm-12">Product Base Image</label>
                        <div class="col-sm-6">
                            <label class="label-control col-sm-12">Front Image</label>
                            <image-input  v-model="myFile.front" name="file">
                                <div slot="activator">
                                    <div class="card w-100">
                                        <div class="card-body" v-if="!myFile.front" style="width:100%; height:100%; padding: 0;" >
                                            <img class="img-fluid" src="<?= base_url() ?>assets/img/placeholder.jpg" alt="Alternate Text" />
                                        </div>
                                        <div class="card-image" v-else>
                                            <img class="img-fluid" :src="myFile.front.imageURL" alt="Alternate Text" />
                                        </div>
                                    </div>
                                </div>
                            </image-input>
                        </div>
                        <div class="col-sm-6">
                            <label class="label-control col-sm-12">Back Image</label>
                            <image-input  v-model="myFile.back" name="file">
                                <div slot="activator">
                                    <div class="card w-100">
                                        <div class="card-body" v-if="!myFile.back" style="width:100%; height:100%; padding: 0;" >
                                            <img class="img-fluid" src="<?= base_url() ?>assets/img/placeholder.jpg" alt="Alternate Text" />
                                        </div>
                                        <div class="card-image" v-else>
                                            <img class="img-fluid" :src="myFile.back.imageURL" alt="Alternate Text" />
                                        </div>
                                    </div>
                                </div>
                            </image-input>
                        </div>

                    </div>
                    <div class="form-group row">
                    <button type="submit" name="button" class="btn btn-primary"> submit </button>
                    </div>
                     </form>
                   </div>
                 </div>
               </div>

             <div class="col-lg-8 mb-4">
               <div class="card">
                 <div class="card-header">
                   <h6 class="text-uppercase mb-0"><?= isset($title) ? $title : '' ?></h6>
                 </div>
                 <div class="card-body">
                   <table class="table card-text  table-sm">
                     <thead>
                       <tr>
                         <th>#</th>
                         <th>&nbsp;</th>
                         <th width="50%">Category</th>
                         <th>Gender</th>
                         <th>Featured</th>
                         <th>Status</th>
                         <th width="10%">Action</th>
                       </tr>
                     </thead>
                     <tbody>
                    <?php if( !empty($data) && $data->num_rows() > 0 ): $i = 1; foreach($data->result() as $row): ?>
                       <tr>
                         <th scope="row"> <?= $i++ ?></th>
                         <td><img src="<?= isset($row->display_image) ? $row->display_image: '' ?>" width="40" height="40"/></td>
                         <td><?= isset($row->category) ? $row->category: '' ?></td>
                         <td><?= isset($row->gender) ? $row->gender: '' ?></td>
                         <td><?= $row->is_featured == 1? '<span class="badge badge-success"> Yes</span>': '<span class="badge badge-secondary"> No</span>' ?></td>
                         <td><?= $row->is_active == 1 ? '<span class="badge badge-success">active</span>': '<span class="badge badge-danger">inactive</span>' ?></td>
                         <td width="10%">
                             <div class="btn-group btn-sm">
                                 <a href="javascript:void(0);" @click="editItem($event)" data-id="<?= $row->category_id ?>" class="btn btn-xs btn-success"><i class="fa fa-pen"></i> </a>
                                 <?php if ($row->is_featured == 1) : ?>
                                     <a href="javascript:void(0);" @click="toggleFeatured($event)" data-id="<?= $row->category_id ?>" class="btn btn-xs btn-secondary"><i class="fa fa-thumbs-up"></i> </a>
                                 <?php else: ?>
                                     <a href="javascript:void(0);" @click="toggleFeatured($event)" data-id="<?= $row->category_id ?>" class="btn btn-xs btn-secondary"><i class="fa fa-thumbs-down"></i> </a>
                                <?php endif; ?>
                                 <?php if ($row->is_active) : ?>
                                     <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->category_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-times"></i> </a>
                                 <?php else: ?>
                                     <a href="javascript:void(0);" @click="toggleActivation($event)" data-id="<?= $row->category_id ?>" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> </a>
                                <?php endif; ?>
                                    <a href="javascript:void(0);" @click="deleteItem($event)" data-id="<?= $row->category_id ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> </a>
                             </div>
                         </td>
                       </tr>
                   <?php endforeach; else: ?>
                       <tr>
                         <td colspan="7"> No category exist at the moment </td>
                       </tr>
                   <?php endif;?>
                     </tbody>
                   </table>
                 </div>
               </div>
             </div>
           </div>
         </section>
<script src="<?= base_url() ?>assets/js/vue.min.js" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="<?= base_url() ?>assets/js/image-input.js" charset="utf-8"></script>
<script type="text/javascript">
var app = new Vue({
    el : '#app',
    data: {
        Item: {
            category:'',
            category_id: 0,
            is_featured: 1,
            is_active: 1,
            image: '',
            front_preview: '',
            back_preview: '',
            gender: 'male',
        },
        myFile: {
            back:null,
            front:null,
            displayImage: null
        },
    },
    methods: {
        reset()
        {
            return {
                category:'',
                category_id: 0,
                is_featured: 1,
                is_active: 1,
                image: '',
                front_preview: '',
                back_preview: '',
            }
        },

        submit()
        {
            let app = this;
            formRequest  = new FormData();
            formRequest.append('category', app.Item.category);
            formRequest.append('is_featured', app.Item.is_featured);
            formRequest.append('is_active', app.Item.is_active);
            formRequest.append('gender', app.Item.gender);
            if (app.myFile != null )
            {
                formRequest.append('image', app.myFile.displayImage.imageFile);
            }
            if (app.myFile.front != null )
            {
                formRequest.append('front', app.myFile.front.imageFile);
            }
            if (app.myFile.back != null )
            {
                formRequest.append('back', app.myFile.back.imageFile);
            }


            if ( app.Item.category_id > 0 )
            {
                axios.post('<?= site_url() ?>categories/put_category/'+ app.Item.category_id, formRequest).then((response) => {
                    console.log(response);
                    $.growl.notice({ 'message': "item was updated successfully."});
                    app.Item = this.reset();
                    location.reload();
                }).catch(e => console.log(e));
            }else{
                axios.post('<?= site_url() ?>categories/post_category', formRequest ).then((response) => {
                    console.log(response);
                    $.growl.notice({ 'message': "item was created successfully."});
                    app.Item = this.reset();location.reload();
                }).catch(e => console.log(e));
            }

        },

        onFileChange(fieldName, file) {
          const { maxSize } = this
          let imageFile = file[0]

          //check if user actually selected a file
          if (file.length > 0) {
            let size = imageFile.size / maxSize / maxSize
            if (!imageFile.type.match('image.*')) {
              // check whether the upload is an image
              this.errorDialog = true
              this.errorText = 'Please choose an image file'
            } else if (size > 1) {
              // check whether the size is greater than the size limit
              this.errorDialog = true
              this.errorText =
                'Your file is too big! Please select an image under 1MB'
            } else {
              // Append file into FormData & turn file into image URL
              let formData = new FormData()
              let imageURL = URL.createObjectURL(imageFile)
              formData.append(fieldName, imageFile)
              // Emit FormData & image URL to the parent component
              //this.$emit('input', { formData, imageURL, imageFile })
              app.myFile = { formData, imageURL, imageFile };
            }
          }
          console.log(this.myFile);
        }
        ,

        editItem(e)
        {
            let id;
            let app = this;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.get('<?= base_url() ?>categories/single/'+id).then((response) => {
                console.log(response.data);
                if ( response.status == 200 )
                {
                    this.Item = response.data.data;
                    this.myFile = {
                        front: {
                            imageURL:response.data.data.front_preview
                        },
                        back: {
                            imageURL:response.data.data.back_preview
                        },
                        displayImage: {
                            imageURL:response.data.data.display_image
                        }
                    }
                }

            }).catch(e => {
                console.log(e)
                $.growl.error({ 'message': "All fields are required"});
            });
        },

        deleteItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }


            axios.delete('<?= base_url() ?>categories/trash/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    $.growl.notice({ 'message': "item was deleted successfully." });
                    $(e.target).closest('tr').remove();
                }
            }).catch(e => console.log(e));
        },

        toggleActivation(e)
        {
            let status = $(e.target).closest('tr').children()[4];
            console.log($(status).find('span').text());
            if ( $(status).find('span').text() == 'active' )
            {
                this.deactivateItem(e);
            }else{
                this.activateItem(e);
            }
        },

        deactivateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>categories/deactivate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[4];
                    $.growl.notice({ 'message': "item was deactivated successfully."});
                    $(badge_parent).find('span').removeClass("badge-success").addClass("badge-danger").text('inactive');
                    $(e.target).closest('tr').children().find('i.fa-times').removeClass('fa-times').addClass('fa-check');
                }
            }).catch(e => console.log(e));
        },

        activateItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>categories/activate/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[4];
                    $.growl.notice({ 'message': "item was activated successfully."});
                    $(badge_parent).find('span').removeClass("badge-danger").addClass("badge-success").text('active');
                    $(e.target).closest('tr').children().find('i.fa-check').removeClass('fa-check').addClass('fa-times');
                }
            }).catch(e => console.log(e));
        },

        toggleFeatured(e)
        {
            let status = $(e.target).closest('tr').children()[3];
            if ( $(status).find('span').text() == "Yes" )
            {
                this.unfeatureItem(e);
            }else{
                this.featureItem(e);
            }
        },

        unfeatureItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>categories/unfeature/'+id).then((response) => {
                 badge_parent = $(e.target).closest('tr').children()[3];
                $.growl.notice({ 'message': "item was unfeatured successfully."});
                $(badge_parent).find('span').removeClass("badge-success").addClass("badge-secondary").text('No');
                $(e.target).closest('tr').children().find('i.fa-thumbs-up').removeClass('fa-thumbs-up').addClass('fa-thumbs-down');

            }).catch(e => console.log(e));
        },

        featureItem(e)
        {
            let id;
            if ( e.target.nodeName == "I")
            {
                id = $(e.target).closest('a').attr('data-id');
            }else{
                id = $(e.target).attr('data-id');
            }

            axios.post('<?= base_url() ?>categories/feature/'+id).then((response) => {
                if ( response.status == 200 )
                {
                    let badge_parent = $(e.target).closest('tr').children()[3];
                    console.log(badge_parent)
                    $.growl.notice({ 'message': "item was featured successfully."});
                    $(badge_parent).find('span').removeClass("badge-secondary").addClass("badge-success").text('Yes');
                    $(e.target).closest('tr').children().find('i.fa-thumbs-down').removeClass('fa-thumbs-down').addClass('fa-thumbs-up');
                }
            }).catch(e => console.log(e));
        }
    }
})
</script>
