<section class="features">
    <div class="container">
        <div class="row">
              <div class="col-12">
                   <div class="section-title text-center">
                        <h1 class="heading">
                             Categories
                        </h1>
                        <p><small>Created to your specific body measurement from the earths finest fabrics. Now enjoy our wardrobe.</small></p>

                   </div>

              </div>
		</div>
		<div class="row">
              <div class="col-12">
                   <div class="grid row">
                       <?php if ( isset($categories) && count($categories) > 0 ): foreach($categories as $category): ?>
                        <div class="grid-item grid-25">

                             <figure class="effect-sadie"><img src="<?= base_url().(isset($category->display_image) ? $category->display_image: DEFAULT_CATEGORY_IMG) ?>" alt="">
                                  <figcaption>
                                       <h2><?= (isset($category->category) ? $category->category: '')  ?></h2>
                                       <a href="<?= site_url('categories/products/'.$category->slug) ?>" class="btn">View more</a>
									   <p> View products </p>
                                  </figcaption>
                             </figure>

                        </div>
                    <?php endforeach; endif; ?>
                   </div>

              </div>


         </div>
    </div>
</section>
