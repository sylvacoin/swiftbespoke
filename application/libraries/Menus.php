<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends CI_Controller {

	protected $CI;
	protected $db;
	private $table;
	private $result;
	public $config = array();

	public function __construct() {
		$this->CI = &get_instance();
		$this->db = $this->CI->db;
		$this->table = "menus";
		$this->config = array(
			'container' => 'div',
			'container_class' => 'collapse navbar-collapse mx-auto w-auto justify-content-center',
			'container_id' => 'navbarNavAltMarkup',
			'sub_container' => 'div',
			'sub_container_class' => 'navbar-nav',
			'sub_container_id' => '',
			'link_tag' => 'a',
			'link_tag_class' => 'nav-item nav-link',
			'link_tag_id' => '',
		);
	}

	public function get_menu() {
		$menus = array();
		// $this->db->select('*');
		$this->result = $this->db->get($this->table)->result();
		return $this->result;
	}

	public function all($location = "frontend") {
		$html = "";

		$menus = $this->get_menu();

		//echo array_count_values( $menus['location']);
		if ($location == "frontend") {
			$menu_list = array_filter($menus, function ($k) {
				return $k->location == "frontend";

			}, ARRAY_FILTER_USE_BOTH);

			if (count($menu_list) > 2) {
				$midPoint = round(count($menu_list) / 2);
			} else {
				$midPoint = 1;
			}
			$count = 0;

			$html .= "\n" . '<' . $this->config['container'] . ' class="' . $this->config['container_class'] . '" id="' . $this->config['container_id'] . '">' . "\n";
			$html .= "\t\t\t<" . $this->config['sub_container'] . ' class="' . $this->config['sub_container_class'] . '" id="' . $this->config['sub_container_id'] . '">' . "\n";
			foreach ($menu_list as $menu) {
				$count++;

				$html .= '<' . $this->config['link_tag'] . ' class="' . $this->config['link_tag_class'] . '" href="' . base_url('action/') . $menu->url . '">' . $menu->menu . '</' . $this->config['link_tag'] . '>' . "\n";
				if ($midPoint == $count) {
					$html .= '<a class="navbar-brand mx-2 d-none d-md-inline" href="' . base_url() . '"><img src="' . base_url() . '/assets/img/logo.png" alt="logo" /></a>' . "\n";
				}
			}

			$html .= "\t\t\t </" . $this->config['sub_container'] . ">\n";
			$html .= "</" . $this->config['container'] . "> \n\n";

			return $html;
		}

	}

}

/* End of file Menus.php */
/* Location: ./application/libraries/Menus.php */