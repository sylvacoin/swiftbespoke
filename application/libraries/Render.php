<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Render {

    protected $CI;

    const HTTP_STATUS =  array(
        'Ok' => 200,
        'Created' => 201,
        'NoContent' => 204,
        'BadRequest' => 400,
        'UnAuthorized' => 401,
        'NotFound' => 404,
        'MethodNotAllowed' => 405,
        'Forbidden' => 403,
        'UnsupportedMediaType' => 415,
        'InternalServerError' => 500
    );

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
    }

    public function view($viewFile, $pageTitle="", $pageData = NULL, $template = 'frontend') {
        $data['viewFile'] = $viewFile;
        $data['title'] = $pageTitle;
        $data['body'] = $pageData;
        $this->CI->load->view('Templates/' . $template . '/' . $template . '.php', $data);
    }

    public function json($data, $status = HTTP_STATUS['Ok'])
    {
        $this->CI->output->set_status_header($status)
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

}
