<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require APPPATH.'/third_party/vendor/autoload.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailer {

    protected $CI;
    protected $db;
    protected $config;
    protected $mail_config;
    protected $mail;

    public function __construct( array $config) {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
        $this->db = $this->CI->db;
        $this->mail = new PHPMailer(true);
        $this->config = (object) array(
            'customer_name' => $config['customer_name'],
            'activation_link' => isset($config['activation_link']) ? $config['activation_link'] : '',
            'new_password' => isset($config['new_password'])? $config['new_password']:'',
            'to' => $config['to'],
            'mail_option'=> $config['mail_option']
        );

        //Server settings
        $this->mail->SMTPDebug = 0;                                       // Enable verbose debug output
        $this->mail->isSMTP();                                            // Set mailer to use SMTP
        $this->mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $this->mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $this->mail->Username   = 'gandhiberry4u@gmail.com';                     // SMTP username
        $this->mail->Password   = 'codex007';                               // SMTP password
        $this->mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port       = 587;
        $this->mail->setFrom('gandhiberry4u@gmail.com', 'Swiftbespoke');
        $this->mail->isHTML(true);
        $this->mail->addAddress($this->config->to);
    }

    public function get_mail_option() {
        $this->db->where(['action' => $this->config->mail_option]);
        $mail = $this->db->get('mail_templates');

        if ($mail->num_rows() == 0 )
        {
            die('Invalid mail action on get mail option');
        }

        return $mail->row();
    }

    public function add_attachments( array $attachments)
    {
        foreach ( $attachments as $name => $path)
        {
            $this->mail->addAttachment($path, $name);
        }

    }

    public function add_ccs( array $ccs)
    {
        foreach ( $ccs as $email)
        {
            $this->mail->addCC($email);
        }

    }

    public function add_bccs( array $bccs)
    {
        foreach ( $bccs as $email)
        {
            $this->mail->addBCC($email);
        }

    }


    public function add_address( array $addresses )
    {
        foreach ( $addresses as $address )
        {
            $this->mail->addAddress($address);               // Name is optional
        }
    }

    public function send_mail() {
        // add any data you want in this array
        try {
            $data_map = $this->get_data_map();
            $mail_template = $this->get_mail_option();
            $template = $mail_template->body;
            $message = str_replace(array_keys($data_map), array_values($data_map), $template);


            if ( ! $mail_template )
            {
                return false;
            }
            $this->mail->Subject = $mail_template->subject;
            $this->mail->Body    = $message;
            // send email
            $this->mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function get_data_map()
    {
        switch( $this->config->mail_option )
        {
            case 'activate_account':
            return array(
                '[[CUSTOMER_NAME]]' => $this->config->customer_name,
                '[[ACTIVATION_LINK]]' => $this->config->activation_link
            );
            break;
            case 'password_reset':
            return array(
                '[[NEW_PASSWORD]]' => $this->config->new_password,
                '[[CUSTOMER_NAME]]' => $this->config->customer_name,
            );
        }
    }

}
