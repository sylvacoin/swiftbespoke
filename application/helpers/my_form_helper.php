<?php

//site wide constants;
define('DIV_ERROR', '<div class="alert alert-danger">');
define('DIV_SUCCESS', '<div class="alert alert-success">');
define('DIV_CLOSE', '</div>');

function show_message($success, $error) {
    if (isset($success)) {
        echo DIV_SUCCESS . $success . DIV_CLOSE;
    }

    if (isset($error)) {
        echo DIV_ERROR . $error . DIV_CLOSE;
    }

    echo validation_errors(DIV_ERROR, DIV_CLOSE);
}

function set_active($link, $seg = 0) {
    $uri = uri_string();
    $ci = &get_instance();

    if ( !is_array($link) && $link == $ci->uri->segment(1).'/'.$ci->uri->segment(2)) {
        return 'active ';
    }elseif(is_array($link) && in_array($ci->uri->segment(1), $link)){
        return 'active ';
    }
    return '';
}

function get_session_data($key)
{
    $ci = &get_instance();
    if ( array_key_exists(SITENAME, $_SESSION) && array_key_exists($key, $ci->session->userdata(SITENAME)))
    {
        return $_SESSION[SITENAME][$key];
    }

    return [];
}

function set_session_data($key, $value) {
    $ci = &get_instance();
    $_SESSION[SITENAME][$key] = $value;
}
