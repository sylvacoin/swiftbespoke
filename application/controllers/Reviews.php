<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reviews extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->load->library('debugger');
		$this->load->model('Mdl_reviews', 'mdl_reviews');

	}

    function index($id)
    {
        $result = $this->mdl_reviews->get_where_custom(['products.product_id' => $id]);
        $this->render->view('reviews/index', 'Product Reviews', ['data' => $result], 'backend');
    }


    function get_review($id)
    {
        $result = $this->mdl_reviews->get_where($id);
        $this->render->json(['data' => $result->row()], '200');
    }


}
