<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->load->library('debugger');
		$this->load->model('Mdl_auth', 'mdl_auth');
		$this->load->model('Mdl_user_addresses', 'mdl_user_addresses');

	}

	public function index() {
		$this->render->view('auth/index', 'login', null, 'midend');
	}

	function submit() {

		$this->form_validation->set_rules('email', 'Email Address', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() != FALSE) //login logic for login page
		{
			$result = $this->mdl_auth->get_where_custom([
				'email' => $this->input->post('email'),
				'password' => md5($this->input->post('password') . SALT),
			]);

			if ($result->num_rows() > 0) {
				
				set_session_data('name',$result->row()->firstname.' '. $result->row()->lastname);
				set_session_data('user_id',$result->row()->user_id);
				set_session_data('user',$result->row());
				set_session_data('addresses',$this->mdl_user_addresses->get_where_custom('user_id', $result->row()->user_id)->result());

				if ( $this->input->get('redirect') != null )
				{
					//$this->debugger->debug($this->input->get('redirect'));
					redirect(site_url($this->input->get('redirect')));
				}
				redirect('dashboard');
			}
		}
		$this->session->set_flashdata('error', "Invalid email or password");
		redirect('login');

	}


	public function forgot_password() {

		$this->render->view('auth/forgot_password', 'Reset password', null,  'midend');
	}

	function reset() {
		$this->load->helper('string');

		$this->form_validation->set_rules('email', 'Email Address', 'required');

		if ($this->form_validation->run() != FALSE) //login logic for login page
		{
			$result = $this->mdl_auth->get_where_custom([
				'email' => $this->input->post('email')
			]);

			if ($result->num_rows() > 0) {
				//do the reset here
				$user = $result->row();
				$unique_password = random_string();
				$this->load->library('Mailer', array(
					'to' => $this->input->post('email'),
					'customer_name' => $user->firstname.' '.$user->lastname,
					'new_password' => $unique_password,
					'mail_option' => 'password_reset'
				));

				$this->mdl_auth->_update($user->user_id, ['password' => md5($unique_password.SALT)]);
				if ( $this->mailer->send_mail() )
				{
					$this->session->set_flashdata('success', "Please check your email for new password.");
				}else{
					$this->session->set_flashdata('error', "An error occured please try again.");
				}

				redirect('login');
			}
		}

		$this->session->set_flashdata('error', "This email address does not exist");
		redirect('login');

	}

	public function isUserLoggedIn()
	{
		$response = false;
		// code...
		$user = get_session_data('user');
		$addresses = get_session_data('addresses');
		if ( isset($user) && !empty($user))
		{
			$response = true;
		}
		$this->render->json(['isLogged' => $response, 'user' => $user, 'addresses' => $addresses], 200);
		return;
	}

	public function test()
	{
		$this->debugger->debug( $_SESSION );
	}


	public function logout() {
		session_destroy();
		redirect('login');
	}

	private function set_session($key, $value) {
		$data[SITENAME][$key] = $value;
		$this->session->set_userdata($data);
	}
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */
