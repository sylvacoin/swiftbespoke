<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
  function __construct()
  {
    parent::__construct();
    // $this->load->library('debugger');
  }


  function index()
  {
    $this->render->view('dashboard/index', 'Dashboard', null, 'backend');
  }
}
