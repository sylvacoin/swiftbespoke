<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->library('debugger');
        $this->load->model('Mdl_products', 'mdl_products');
        $this->load->model('Mdl_attributes', 'mdl_attributes');
        $this->load->model('Mdl_sub_attributes', 'mdl_sub_attributes');
        $this->load->model('Mdl_attribute_options', 'mdl_attribute_options');
        $this->load->model('Mdl_categories', 'mdl_categories');
        $this->load->model('Mdl_measurements', 'mdl_measurements');
        $this->load->helper('text');
    }

    public function index()
    {
        $data = $this->mdl_products->get_where_custom(['products.is_deleted'=> '0']);
        $this->render->view('products/index', 'Product List', ['data' => $data], 'backend');
    }

    public function products($category_slug)
    {
        $data = $this->mdl_products->get_where_custom(['products.is_deleted'=> '0', 'slug' => $category_slug])->result();
        $this->render->view('products/products', 'Product List', ['products' => $data]);
    }

    public function customize($product_id)
    {
        $product = $this->mdl_products->get_where($product_id)->row();
        $attributes = $this->mdl_attributes->get_where_custom(['attributes.is_deleted'=>0, 'attributes.category_id' => $product->category_id, 'attributes.is_active'=>1])->result_array();
        foreach ($attributes as $k => $attribute) {
            $attributes[$k]['sub_attributes'] = $this->mdl_sub_attributes->get_where_custom(['sub_attributes.is_deleted'=>0, 'sub_attributes.attribute_id' => $attribute['attribute_id'], 'sub_attributes.is_active'=>1])->result_array();

            foreach ($attributes[$k]['sub_attributes'] as $key => $sub_attribute) {
                $attributes[$k]['sub_attributes'][$key]['options'] = $this->mdl_attribute_options->get_where_custom(['attribute_options.is_deleted'=>0, 'sub_attribute_id' => $sub_attribute['sub_attribute_id'],'attribute_options.is_active'=>1])->result();
            }
        }
        $sub_attributes = $this->mdl_attributes->get_all_subattributes($product->category_id)->result();
        $category = $this->mdl_categories->get_where($product->category_id)->row();
        $custom_measurements = $this->mdl_measurements->get_where_custom(['measurements.category_id' => $category->category_id])->result();
        // $this->debugger->debug([
		// 	'product' => $product,
		// 	'attributes' => $attributes,
		// 	'sub_attributes' => $sub_attributes,
		// 	'category'=>$category,
		// 	'custom_measurements' => $custom_measurements
		// ]);
        $this->render->view(
			'products/customize',
			'Customize product',
			[
				'product' => $product,
				'attributes' => $attributes,
				'sub_attributes' => $sub_attributes,
				'category'=>$category,
				'custom_measurements' => $custom_measurements
			]);
    }

    public function create()
    {
        $this->render->view('products/create', 'Create product', null, 'backend');
    }

    public function post_product()
    {
        $this->load->helper('inflector');
        $this->load->library('uploader');
        //$result = json_decode( file_get_contents('php://input') );
        $this->form_validation->set_rules('product', 'Product', 'required');

        if (! isset($_FILES['productImage1'])) {
            $this->form_validation->set_rules('productImage1', 'Product Display Image', 'required');
            exit(0);
        }

        if ($this->form_validation->run()) {
            $data = $this->get_data_from_post();
            $data['product_image_1'] = $this->uploader->upload('productImage1');
            if (isset($_FILES['product_image_2'])) {
                $data['product_image_2'] = $this->uploader->upload('productImage2');
            }

            if ($this->mdl_products->_insert($data)) {
                $data['product_id'] = $this->db->insert_id();
                return $this->render->json(['data' => $data], 201);
                exit(0);
            }
        }
        return $this->render->json(['message' => 'All fields are required'], 400);
        exit(0);
    }

    public function put_product($id)
    {
        $this->load->helper('inflector');
        $this->load->library('uploader');

        $this->form_validation->set_rules('product', 'Product', 'required');

        if ($this->form_validation->run()) {
            $data = $this->get_data_from_post();
            if (isset($_FILES['product_image_1'])) {
                $data['product_image_1'] = $this->uploader->upload('productImage1');
            }
            if (isset($_FILES['product_image_2'])) {
                $data['product_image_2'] = $this->uploader->upload('productImage2');
            }

            if ($this->mdl_products->_update($id, $data)) {
                return $this->render->json(['data' => $data], 200);
                exit(0);
            }
        }
        return $this->render->json(['message' => 'All fields are required'], 400);
        exit(0);
    }

    public function single(int $id)
    {
        $item = $this->mdl_products->get_where($id);
        return $this->render->json(['data'=>$item->row()], 200);
        exit(0);
    }

    public function get_json()
    {
        $item = $this->mdl_products->get();
        return $this->render->json(['data'=>$item->result()], 200);
        exit(0);
    }


    public function deactivate(int $id)
    {
        $item = $this->mdl_products->_update($id, ['is_active' => 0]);
        if ($item) {
            return $this->render->json(['message'=>'item deactivated'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to deactivate'], 404);
        exit(0);
    }

    public function activate(int $id)
    {
        $item = $this->mdl_products->_update($id, ['is_active' => 1]);
        if ($item) {
            return $this->render->json(['message'=>'item activated'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to activate'], 404);
        exit(0);
    }

    public function unfeature(int $id)
    {
        $item = $this->mdl_products->_update($id, ['is_featured' => 0]);
        if ($item) {
            return $this->render->json(['message'=>'item unfeatured'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to unfeature'], 404);
        exit(0);
    }

    public function feature(int $id)
    {
        if ($this->mdl_products->_update($id, ['is_featured' => 1])) {
            return $this->render->json(['message'=>'item featured'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to feature'], 404);
        exit(0);
    }

    public function trashed()
    {
        $data = $this->mdl_products->get_where_custom(['products.is_deleted'=> '1']);
        $this->render->view('products/trashed', 'Product Trash', ['data' => $data], 'backend');
    }


    public function trash(int $id)
    {
        $item = $this->mdl_products->_update($id, ['is_deleted' => 1]);
        if ($item) {
            return $this->render->json(['message'=>'item trashed'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to trash'], 404);
        exit(0);
    }

    public function restore(int $id)
    {
        $item = $this->mdl_products->_update($id, ['is_deleted' => 0]);
        if ($item) {
            return $this->render->json(['message'=>'item restored'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to restore'], 404);
        exit(0);
    }

    public function delete(int $id)
    {
        $item = $this->mdl_products->_delete($id);
        if ($item) {
            return $this->render->json(['message'=>'item deleted'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to delete'], 204);
        exit(0);
    }

    public function get_products()
    {
        $result = $this->mdl_products->get_where_custom('products.is_deleted', 0);
        $this->render->json(['data'=>$result->result()], 200);
    }

    public function get_data_from_post()
    {
        $data['product_name'] = $this->input->post('product_name');
        $data['is_active'] = $this->input->post('is_active');
        $data['is_customizable'] = $this->input->post('is_customizable');
        $data['in_stock'] = $this->input->post('in_stock');
        $data['description'] = $this->input->post('description');
        $data['price'] = $this->input->post('price');
        $data['category_id'] = $this->input->post('category_id');
        $data['style_id'] = $this->input->post('style_id');
        $data['discount'] = $this->input->post('discount');

        return $data;
    }
}
