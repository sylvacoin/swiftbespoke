<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Units extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mdl_units', 'mdl_units');
  }

  public function index()
  {
      $data = $this->mdl_units->get_where_custom(['units.is_deleted'=> '0']);
      $this->render->view('units/index', 'Unit List', ['data' => $data], 'backend');
  }

  public function create()
  {
      $this->render->view('units/create', 'Create unit', null, 'backend');
  }

  public function post_unit()
  {
      $this->load->helper('inflector');
      //$result = json_decode( file_get_contents('php://input') );
      $this->form_validation->set_rules('unit', 'Unit', 'required');

      if ($this->form_validation->run()) {
          $data = $this->get_data_from_post();

          if ($this->mdl_units->_insert($data)) {
              $data['unit_id'] = $this->db->insert_id();
              return $this->render->json(['data' => $data], 201);
              exit(0);
          }

      }
      return $this->render->json(['message' => 'All fields are required'], 400);
      exit(0);
  }

  public function put_unit($id)
  {
      $this->load->helper('inflector');

      $this->form_validation->set_rules('unit', 'Unit', 'required');

      if ($this->form_validation->run()) {
          $data = $this->get_data_from_post();

          if ($this->mdl_units->_update($id, $data)) {
              return $this->render->json(['data' => $data], 200);
              exit(0);
          }
      }
      return $this->render->json(['message' => 'All fields are required'], 400);
      exit(0);
  }

  public function single( int $id )
  {
      $item = $this->mdl_units->get_where($id);
      return $this->render->json(['data'=>$item->row()], 200);
      exit(0);
  }

  public function get_json()
  {
      $item = $this->mdl_units->get();
      return $this->render->json(['data'=>$item->result()], 200);
      exit(0);
  }


  public function deactivate( int $id )
  {
      $item = $this->mdl_units->_update($id, ['is_active' => 0]);
      if ($item)
      {
          return $this->render->json(['message'=>'item deactivated'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to deactivate'], 404);
      exit(0);
  }

  public function activate( int $id )
  {
      $item = $this->mdl_units->_update($id, ['is_active' => 1]);
      if ($item)
      {
          return $this->render->json(['message'=>'item activated'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to activate'], 404);
      exit(0);
  }

  public function trashed()
  {
      $data = $this->mdl_units->get_where_custom(['units.is_deleted'=> '1']);
      $this->render->view('units/trashed', 'Unit Trash', ['data' => $data], 'backend');
  }


  public function trash( int $id )
  {
      $item = $this->mdl_units->_update($id, ['is_deleted' => 1]);
      if ($item)
      {
          return $this->render->json(['message'=>'item trashed'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to trash'], 404);
      exit(0);
  }

  public function restore( int $id )
  {
      $item = $this->mdl_units->_update($id, ['is_deleted' => 0]);
      if ($item)
      {
          return $this->render->json(['message'=>'item restored'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to restore'], 404);
      exit(0);
  }

  public function delete( int $id )
  {
      $item = $this->mdl_units->_delete($id);
      if ($item)
      {
          return $this->render->json(['message'=>'item deleted'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to delete'], 204);
      exit(0);
  }

  public function get_units()
  {
      $result = $this->mdl_units->get_where_custom('units.is_deleted', 0);
      $this->render->json(['data'=>$result->result()], 200);
  }

  public function get_data_from_post()
  {
      $data['unit'] = $this->input->post('unit');
      $data['is_active'] = $this->input->post('is_active');

      return $data;
  }
}
