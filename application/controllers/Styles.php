<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Styles extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->load->library('debugger');
		$this->load->model('Mdl_styles', 'mdl_styles');

	}

    public function index()
    {
		$data = $this->mdl_styles->get_where_custom(['is_deleted'=> '0']);
        $this->render->view('styles/index', 'style List', ['data' => $data], 'backend');
    }

	public function trashed()
    {
		$data = $this->mdl_styles->get_where_custom(['is_deleted'=> 1]);
        $this->render->view('styles/trashed', 'Trashed Styles', ['data' => $data], 'backend');
    }

	public function create()
	{
		$this->render->view('styles/create', 'Create style', null, 'backend');
	}

	public function post_style()
	{
		$this->load->helper('inflector');
		$this->load->library('uploader');
		//$result = json_decode( file_get_contents('php://input') );
		$this->form_validation->set_rules('style', 'style', 'required');

        if ($this->form_validation->run()) {
            $data = $this->get_data_from_post();

            if ($this->mdl_styles->_insert($data)) {
				$data['style_id'] = $this->db->insert_id();
                return $this->render->json(['data' => $data], 201);
				exit(0);
            }

        }
		return $this->render->json(['message' => 'All fields are required'], 400);
		exit(0);
	}

	public function put_style($id)
	{
		$this->load->helper('inflector');
		$this->load->library('uploader');

		$this->form_validation->set_rules('style', 'style', 'required');

        if ($this->form_validation->run()) {
			$data = $this->get_data_from_post();
            if ($this->mdl_styles->_update($id, $data)) {
                return $this->render->json(['data' => $data], 200);
				exit(0);
            }
        }
		return $this->render->json(['message' => 'All fields are required'], 400);
		exit(0);
	}

	public function single( int $id )
	{
		$item = $this->mdl_styles->get_where($id);
		return $this->render->json(['data'=>$item->row()], 200);
		exit(0);
	}


	public function deactivate( int $id )
	{
		$item = $this->mdl_styles->_update($id, ['is_active' => 0]);
		if ($item)
		{
			return $this->render->json(['message'=>'item deactivated'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to deactivate'], 404);
		exit(0);
	}

	public function activate( int $id )
	{
		$item = $this->mdl_styles->_update($id, ['is_active' => 1]);
		if ($item)
		{
			return $this->render->json(['message'=>'item activated'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to activate'], 404);
		exit(0);
	}



	public function trash( int $id )
	{
		$item = $this->mdl_styles->_update($id, ['is_deleted' => 1]);
		if ($item)
		{
			return $this->render->json(['message'=>'item trashed'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to trash'], 404);
		exit(0);
	}

	public function restore( int $id )
	{
		$item = $this->mdl_styles->_update($id, ['is_deleted' => 0]);
		if ($item)
		{
			return $this->render->json(['message'=>'item restored'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to restore'], 404);
		exit(0);
	}

	public function delete( int $id )
	{
		$item = $this->mdl_styles->_delete($id);
		if ($item)
		{
			return $this->render->json(['message'=>'item deleted'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to delete'], 204);
		exit(0);
	}

	public function get_styles()
	{
		$result = $this->mdl_styles->get_where_custom(['is_deleted' => 0]);
		return $this->render->json(['data'=>$result->result()], 200);
		exit(0);
	}

	public function get_data_from_post()
	{
		$data['style'] = $this->input->post('style');
		$data['is_active'] = $this->input->post('is_active');
		$data['updated_at'] = date("Y-m-d H:i:s");
		return $data;
	}
}
