<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->library('debugger');
    $this->load->model('Mdl_orders','mdl_orders');
    $this->load->model('Mdl_order_contents','mdl_order_contents');
  }

  function index()
  {
      $this->render->view('orders/index', 'Orders', null, 'backend');
  }

  public function make_order( $ref )
  {
      // insert into orders tables
      $cart = get_session_data('cart');

      $data = $this->get_orders_from_post();
      $this->db->trans_start();
      $result = $this->mdl_orders->get_where_custom($data);
      if ( $result->num_rows() == 0 )
      {
          if ($this->mdl_orders->_insert($data))
          {
              $oid = $this->db->insert_id();
              set_session_data('oid', $oid);
              $this->addTransaction($oid, $ref);
              $data_content = $this->get_order_contents_from_session( $oid );
              $this->mdl_order_contents->_insert_batch( $data_content );
              $this->db->trans_complete();
          }
      }else{
          $oid = get_session_data('oid');
          $trans = $this->db->get_where('transactions', [
              'order_id' => $oid,
              'refno' => $ref,
              'is_verified' => 0,
              'user_id' => get_session_data('user_id')
          ]);
          if ( $trans->num_rows() == 0 )
          {
              $this->addTransaction($oid, $ref);
          }
          $this->db->trans_complete();
      }

      return $this->render->json(['data' => 'success'], 200);
  }

  private  function addTransaction($oid, $ref)
  {
      return $this->db->insert('transactions', [
          'order_id' => $oid,
          'refno' => $ref,
          'is_verified' => 0,
          'user_id' => get_session_data('user_id')
      ]);
  }

  public function get_orders_from_post()
  {
      $user_input = json_decode(file_get_contents('php://input'));
      $cart = get_session_data('cart');
      // code...
      $data = array(
          'customer_id' => get_session_data('user_id'),
          'total' => array_sum( array_column($cart, 'total' )),
          'payment_type_id' => $user_input->payment_type,
          'shipping_address'=> json_encode($user_input->shipping_address),
          'billing_address'=> json_encode($user_input->billing_address)
      );

      return $data;
  }

  public function get_order_contents_from_session($order_id)
  {
      $data = [];
      $cart = get_session_data('cart');

      foreach ( $cart as $product )
      {
          $id = explode( '_', $product['id'] )[0];
          $data[] = array(
              'order_id' => $order_id,
              'product_id' => $id,
              'order_data' => json_encode($product['customizations']),
              'preview_image' => '',
              'quantity' => $product['quantity'],
              'measurements' => json_encode($product['measurement']),
              'total_cost' => $product['total'],
              'unit_cost' => $product['cost']
          );
      }

      return $data;
  }

  public function verify_order( $refno ) {
      $result = $this->db->get_where('transactions', ['refno'=> $refno]);
      if ( $result->num_rows() > 0 )
      {
          $this->db->where(['refno'=> $refno]);
          $this->db->update('transactions', ['is_verified' => true]);
          $this->mdl_orders->_update($result->row()->order_id, ['is_paid' => true]);
          unset($_SESSION[SITENAME]['oid']);
          unset($_SESSION[SITENAME]['cart']);
      }
      return $this->render->json(['message' => 'success'], 200);
  }


}
