<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->library('debugger');
        $this->load->model(['Mdl_cart','Mdl_products']);
    }

    public function index()
    {
        $data = !empty(get_session_data('cart')) ? get_session_data('cart') : [];
        $this->render->view('cart/index', 'Cart', ['data' => $data]);
    }

    public function checkout()
    {
        $data = !empty(get_session_data('cart')) ? get_session_data('cart') : [];
        $this->render->view('cart/checkout', 'Check out', ['data' => $data]);
    }

    public function add()
    {
        $toggleOpt = $this->input->post('toggle_option');
        if (  $toggleOpt == 'custom')
        {
            $measurement = $this->Mdl_cart->getCustomMeasurementKeysFromDB($this->input->post('measurement'));
            $quantity = $this->input->post('custom_quantity');
        }else{
            $measurement = $this->Mdl_cart->getStandardMeasurement($this->input->post('measurements'), $this->input->post('standard_quantity'));
            $quantity = array_sum(array_column($measurement, 'quantity'));
        }

        $cart = [];
        $product = $this->Mdl_products->get_where($this->input->post('product'))->row();
        foreach ($this->input->post('attr') as $id => $items) {
            $dbCartItems = $this->Mdl_cart->getCartItemsFromDB($items);
            $sessItem = $this->Mdl_cart->createCartFromDBItems($dbCartItems);
            $cart[] = $this->addCartItemToSession($product, $sessItem, $measurement, $toggleOpt, $quantity);
        }

        $this->render->json($cart, 200);
    }

    public function getCartItems()
    {
        $cartItems = get_session_data('cart');
        $this->render->json($cartItems, 200);
    }

    // public function addCartItemToSession($product, $customizations)
    // {
    //     $cartUpdateKey = '';
    //     $this->load->helper('string');
    //     $cart = get_session_data('cart');
    //     //get total cost of customizations
    //     $customizationCost = array_reduce($customizations, function ($output, $input) {
    //         return $output += $input['price'];
    //     });
    //
    //     //check if same product exists
    //
    //     if (count($cart) > 0) {
    //         foreach ($cart as  $k => $value) {
    //             $arrdiff = $this->array_diff_assoc_recursive($customizations, $value['customizations']);
    //
    //             if (count($arrdiff) == 0) {
    //                 $cartUpdateKey = $k;
    //                 break;
    //             }
    //         }
    //
    //         //update quantity if exist.
    //         if (is_numeric($cartUpdateKey)) {
    //             ++$cart[$cartUpdateKey]['quantity'];
    //             set_session_data('cart', $cart);
    //             return $cart[$cartUpdateKey];
    //         }
    //     }
    //     //check for discount on product
    //     $productPrice = !empty($product->discount) ? $product->discount : $product->price;
    //     $sessItem = array(
    //         'product' => $product->product_name,
    //         'customizations' => $customizations,
    //         'price' => $productPrice,
    //         'quantity' => 1,
    //         'cost' => $customizationCost + $productPrice,
    //         'total' => ($customizationCost + $productPrice) * 1,
    //         'image' => base_url() . (isset($product->product_image_1) ? $product->product_image_1 : DEFAULT_CATEGORY_IMG),
    //         'id' => $product->product_id."_".random_string('numeric', 9)
    //     );
    //     $cart[] = $sessItem;
    //     set_session_data('cart', $cart);
    //
    //     return $sessItem;
    // }

    public function addCartItemToSession($product, $customizations, $measurement, $toggleOpt, $quantity)
    {
        $cartUpdateKey = '';

        $this->load->helper('string');
        $cart = get_session_data('cart');
        //get total cost of customizations
        $customizationCost = array_reduce($customizations, function ($output, $input) {
            return $output += $input['price'];
        });

        //check if same product exists
        /*
        *   since we are doing custom measurements and standard measurements, to
        * avoid confusion. always see each customization as a new product.
        *********************************************************************/
        // if (count($cart) > 0) {
        //     foreach ($cart as  $k => $value) {
        //
        //         //get differences between array if there is a difference arrdiff will be greater than 0
        //         $arrdiff = $this->array_diff_assoc_recursive($customizations, $value['customizations']);
        //         $arrdiff = $this->array_diff_assoc_recursive($customizations, $value['customizations']);
        //         if (count($arrdiff) == 0) {
        //             $cartUpdateKey = $k;
        //             break;
        //         }
        //
        //     }
        //
        //     //update quantity if exist.
        //     if (is_numeric($cartUpdateKey)) {
        //         ++$cart[$cartUpdateKey]['quantity'];
        //         set_session_data('cart', $cart);
        //         return $cart[$cartUpdateKey];
        //     }
        // }
        //check for discount on product
        $productPrice = !empty($product->discount) ? $product->discount : $product->price;
        $sessItem = array(
            'product' => $product->product_name,
            'customizations' => $customizations,
            'price' => $productPrice,
            'quantity' => $quantity,
            'measurement' => $measurement,
            'measurement_type' => $toggleOpt,
            'cost' => $customizationCost + $productPrice,
            'total' => ($customizationCost + $productPrice) * $quantity,
            'image' => base_url() . (isset($product->product_image_1) ? $product->product_image_1 : DEFAULT_CATEGORY_IMG),
            'id' => $product->product_id."_".random_string('numeric', 9)
        );
        $cart[] = $sessItem;
        set_session_data('cart', $cart);

        return $sessItem;
    }

    public function deleteEntry($id)
    {
        $cart = get_session_data('cart');
        $uid = array_search($id, array_column($cart, 'id'));
        unset($cart[$uid]);
        set_session_data('cart', array_values($cart));
    }

    public function updateCart()
    {
        // code...
        $resp = json_decode(file_get_contents('php://input'));
        if (!empty($resp)) {
            $cart = get_session_data('cart');
            foreach ($resp as $cart_item) {
                $uid = array_search($cart_item->id, array_column($cart, 'id'));
                $cart[$uid]['quantity'] = $cart_item->quantity;
                $cart[$uid]['total'] = $cart[$uid]['cost'] * $cart_item->quantity;
            }
            set_session_data('cart', $cart);
        }
        $this->render->json(['message'=>'ok'], 200);
    }

    private function array_diff_assoc_recursive($array1, $array2)
    {
        $differences = [];
        foreach ($array1 as $key => $value) {
            $differences = array_merge(array_diff_assoc($array1[$key], $array2[$key]), $differences);
        }

        return $differences;
    }
}
