<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attributes extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->load->library('debugger');
		$this->load->model('Mdl_attributes', 'Mdl_attributes');
        $this->load->model('Mdl_attributes', 'Mdl_attributes');

	}

    public function index()
    {
        $data = $this->Mdl_attributes->get_where_custom(['attributes.is_deleted'=>0]);
        $this->render->view('attributes/index', 'Product Attributes', ['data'=>$data], 'backend');
    }

	public function trashed()
    {
        $data = $this->Mdl_attributes->get_where_custom(['attributes.is_deleted'=>1]);
        $this->render->view('attributes/trashed', 'Trashed Attributes', ['data'=>$data], 'backend');
    }

    function post_attribute()
    {
		//$result = json_decode( file_get_contents('php://input') );
		$this->form_validation->set_rules('attribute', 'Attribute', 'required');
        if ($this->form_validation->run()) {
            $data = $this->get_data_from_post();
            if ($this->Mdl_attributes->_insert($data)) {
				$data['attribute_id'] = $this->db->insert_id();
                return $this->render->json(['data' => $data], 201);
				exit(0);
            }

        }
		return $this->render->json(['message' => 'All fields are required'], 400);
		exit(0);
    }

    function put_attribute( int $id )
    {
		//$result = json_decode( file_get_contents('php://input') );
		$this->form_validation->set_rules('attribute', 'Attribute', 'required');
        if ($this->form_validation->run()) {
            $data = $this->get_data_from_post();
            if ($this->Mdl_attributes->_update($id, $data)) {
				$data['attribute_id'] = $this->db->insert_id();
                return $this->render->json(['data' => $data], 201);
				exit(0);
            }

        }
		return $this->render->json(['message' => 'All fields are required'], 400);
		exit(0);
    }

    public function single( int $id )
	{
		$item = $this->Mdl_attributes->get_where($id);
		return $this->render->json(['data'=>$item->row()], 200);
		exit(0);
	}

    public function deactivate( int $id )
    {
        $item = $this->Mdl_attributes->_update($id, ['is_active' => 0]);
        if ($item)
        {
            return $this->render->json(['message'=>'item deactivated'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to deactivate'], 404);
        exit(0);
    }

    public function activate( int $id )
    {
        $item = $this->Mdl_attributes->_update($id, ['is_active' => 1]);
        if ($item)
        {
            return $this->render->json(['message'=>'item activated'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to activate'], 404);
        exit(0);
    }

    public function trash( int $id )
	{
		$item = $this->Mdl_attributes->_update($id, ['is_deleted' => 1]);
		if ($item)
		{
			return $this->render->json(['message'=>'item trashed'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to trash'], 404);
		exit(0);
	}

	public function restore( int $id )
	{
		$item = $this->Mdl_attributes->_update($id, ['is_deleted' => 0]);
		if ($item)
		{
			return $this->render->json(['message'=>'item restored'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to restore'], 404);
		exit(0);
	}

	public function delete( int $id )
	{
		$item = $this->Mdl_attributes->_delete($id);
		if ($item)
		{
			return $this->render->json(['message'=>'item deleted'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to delete'], 204);
		exit(0);
	}

	public function get_json()
	{
		$item = $this->Mdl_attributes->get_where_custom(['attributes.is_deleted' => 0]);
		return $this->render->json(['data'=>$item->result()], 200);
		exit(0);
	}


	public function get_data_from_post()
	{
		$data['attribute'] = $this->input->post('attribute');
		$data['category_id'] = $this->input->post('category_id');
		$data['discount'] = $this->input->post('discount');
		$data['is_active'] = $this->input->post('is_active');

		return $data;
	}


}
