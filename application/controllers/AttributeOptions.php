<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AttributeOptions extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->load->library('debugger');
		$this->load->model('Mdl_attribute_options', 'Mdl_attribute_options');
        $this->load->model('Mdl_sub_attributes', 'mdl_sub_attributes');
		$this->load->library(['Render','Uploader']);
		$this->load->helper('text');
	}

    public function index($id)
    {
        $data = $this->Mdl_attribute_options->get_where_custom(['sub_attribute_id' => $id]);
		$display_pocket_side = $this->mdl_sub_attributes->get_where($id)->row();
        $this->render->view('attributeOptions/index', 'Product Attribute Options', [
			'data'=>$data,
			'pocket_side_display'=> $display_pocket_side->pocket_side_display,
			'sub_attr_id' => $id
		], 'backend');
    }

    function post_option()
    {
		//$result = json_decode( file_get_contents('php://input') );
		$this->form_validation->set_rules('option_name', 'Option name', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');

		if ( ! isset($_FILES['image_left']) &&  $this->input->post('pocket_side_display') == 1) {
            $this->form_validation->set_rules('image_left', 'Left Pocket Image', 'required');
        }
		if ( ! isset($_FILES['image_right'])  &&  $this->input->post('pocket_side_display') == 1) {
            $this->form_validation->set_rules('image_right', 'Right Pocket Image', 'required');
        }

		if ( ! isset($_FILES['image_both'])  &&  $this->input->post('pocket_side_display') == 1) {
			$this->form_validation->set_rules('image_both', 'Both Pocket Image', 'required');
		}

		if ( ! isset($_FILES['image_thumb']) ) {
			$this->form_validation->set_rules('image_thumb', 'Thumbnail Image', 'required');
		}

		if ( ! isset($_FILES['image_background'])  &&  $this->input->post('pocket_side_display') == 0) {
			$this->form_validation->set_rules('image_background', 'Background Pocket Image', 'required');
		}

        if ($this->form_validation->run()) {
            $data = $this->get_data_from_post();

			if ( $this->input->post('pocket_side_display') == 1 ) {
	           $data['image_left'] = $this->uploader->upload('image_left');
			   $data['image_right'] = $this->uploader->upload('image_right');
			   $data['image_both'] = $this->uploader->upload('image_both');
		   }else{
			    $data['image_background'] = $this->uploader->upload('image_background');
		   }
		   $data['image_thumb'] = $this->uploader->upload('image_thumb');

            if ($this->Mdl_attribute_options->_insert($data)) {
				$data['option_id'] = $this->db->insert_id();
                return $this->render->json(['data' => $data], 201);
				exit(0);
            }

        }
		return $this->render->json(['message' => $this->form_validation->error_array()], 400);
		exit(0);
    }

    function put_option( $id )
    {
		//$result = json_decode( file_get_contents('php://input') );
		$this->form_validation->set_rules('option_name', 'Option name', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run()) {
			$data = $this->get_data_from_post();
			if ( isset($_FILES['image_left']) &&  $this->input->post('pocket_side_display') == 1) {
	            $data['image_left'] = $this->uploader->upload('image_left');
	        }
			if ( isset($_FILES['image_right'])  &&  $this->input->post('pocket_side_display') == 1) {
	            $data['image_right'] = $this->uploader->upload('image_right');
	        }

			if ( isset($_FILES['image_both'])  &&  $this->input->post('pocket_side_display') == 1) {
				$data['image_both'] = $this->uploader->upload('image_both');
			}

			if ( isset($_FILES['image_background'])  &&  $this->input->post('pocket_side_display') == 0) {
				$data['image_background'] = $this->uploader->upload('image_background');
			}

			if ( isset($_FILES['image_thumb']) ) {
				$data['image_thumb'] = $this->uploader->upload('image_thumb');
			}

            if ($this->Mdl_attribute_options->_update($id, $data)) {
				$data['option_id'] = $id;
                return $this->render->json(['data' => $data], 200);
				exit(0);
            }

        }
		return $this->render->json(['message' => 'All fields are required'], 400);
		exit(0);
    }

    public function single( $id, $sub_attr_id )
	{
		$item = $this->Mdl_attribute_options->get_where($id)->row();
		$display_pocket_side = $this->mdl_sub_attributes->get_where($item->sub_attribute_id)->row();
		$data = $item;
		$data->pocket_side_display = $display_pocket_side->pocket_side_display;
		return $this->render->json(['data'=>$data], 200);
		exit(0);
	}

    public function deactivate( $id )
    {
        $item = $this->Mdl_attribute_options->_update($id, ['is_active' => 0]);
        if ($item)
        {
            return $this->render->json(['message'=>'item deactivated'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to deactivate'], 404);
        exit(0);
    }

    public function activate( $id )
    {
        $item = $this->Mdl_attribute_options->_update($id, ['is_active' => 1]);
        if ($item)
        {
            return $this->render->json(['message'=>'item activated'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to activate'], 404);
        exit(0);
    }

    public function trash( $id )
	{
		$item = $this->Mdl_attribute_options->_update($id, ['is_deleted' => 1]);
		if ($item)
		{
			return $this->render->json(['message'=>'item trashed'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to trash'], 404);
		exit(0);
	}

	public function restore( $id )
	{
		$item = $this->Mdl_attribute_options->_update($id, ['is_deleted' => 0]);
		if ($item)
		{
			return $this->render->json(['message'=>'item restored'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to restore'], 404);
		exit(0);
	}

	public function delete( $id )
	{
		$item = $this->Mdl_attribute_options->_delete($id);
		if ($item)
		{
			return $this->render->json(['message'=>'item deleted'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to delete'], 204);
		exit(0);
	}

	public function get_json()
	{
		$item = $this->Mdl_attribute_options->get_where_custom(['attribute_options.is_deleted' => 0]);
		return $this->render->json(['data'=>$item->result()], 200);
		exit(0);
	}


	public function get_data_from_post()
	{
		$data['option_name'] = $this->input->post('option_name');
		$data['sub_attribute_id'] = $this->input->post('sub_attribute_id');
		$data['discount_price'] = $this->input->post('discount');
		$data['price'] = $this->input->post('price');
		$data['is_active'] = $this->input->post('is_active');
		$data['description'] = $this->input->post('description');

		return $data;
	}


}
