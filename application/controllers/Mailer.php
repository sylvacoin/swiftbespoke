<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailer extends CI_Controller {
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('Mdl_mailer','mdl_mailer');
    $this->load->helper('email');
    $this->load->library('debugger');
  }

  function index()
      {
         $mail_templates = $this->mdl_mailer->get();
         $this->render->view('mailer/index', 'Mail List', ['data' => $mail_templates], 'backend');
      }

      function modify( $id )
      {
        $row = $this->mdl_mailer->get_where($id)->row();

        if ( $row )
        {
            $this->render->view('mailer/modify', 'Mail Settings', ['data' => $row], 'backend');
            return;
        }
        $this->session->set_flashdata('error', 'invalid mail data');
        redirect('/mailer');
      }

      function submit( $id )
      {
        $this->form_validation->set_rules('subject','Mail subject', 'required');
        $this->form_validation->set_rules('body','Mail body', 'required');
        if ($this->form_validation->run())
        {
          $this->submit_data();
      }else{
          $this->session->set_flashdata('error', 'An error occured please try again later');
      }
      redirect('/mailer/modify/'. $id);
      }

      function submit_data(){
        //get data from post
        $data = $this->get_data_from_post();
        $id = $this->uri->segment(3);
    	if (is_numeric($id)){
    	    $this->mdl_mailer->_update($id, $data);
    	}else{
    	    $this->mdl_mailer->_insert($data);
    	}
    }

    private function get_data_from_post()
    {
      return array(
        'subject' => $this->input->post('subject'),
        'body' => $this->input->post('body')
    );
    }



}
