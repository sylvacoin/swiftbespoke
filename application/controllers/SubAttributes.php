<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubAttributes extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->load->library('debugger');
		$this->load->model('Mdl_sub_attributes', 'Mdl_sub_attributes');
        $this->load->model('Mdl_sub_attributes', 'Mdl_sub_attributes');

	}

    public function index()
    {
        $data = $this->Mdl_sub_attributes->get_where_custom(['sub_attributes.is_deleted' => 0]);
        $this->render->view('subAttributes/index', 'Product Sub Attributes', ['data'=>$data], 'backend');
    }

	public function trashed()
    {
        $data = $this->Mdl_sub_attributes->get_where_custom(['sub_attributes.is_deleted' => 1]);
        $this->render->view('subAttributes/trashed', 'Trashed Sub Attributes', ['data'=>$data], 'backend');
    }

    function post_sub_attribute()
    {
		//$result = json_decode( file_get_contents('php://input') );
		$this->form_validation->set_rules('sub_attribute', 'Sub Attribute', 'required');
		$this->form_validation->set_rules('pocket_side_display', '', 'required');
        if ($this->form_validation->run()) {
            $data = $this->get_data_from_post();
            if ($this->Mdl_sub_attributes->_insert($data)) {
				$data['subAttribute_id'] = $this->db->insert_id();
                return $this->render->json(['data' => $data], 201);
				exit(0);
            }

        }
		return $this->render->json(['message' => 'All fields are required'], 400);
		exit(0);
    }

    function put_sub_attribute( int $id )
    {
		//$result = json_decode( file_get_contents('php://input') );
		$this->form_validation->set_rules('subAttribute', 'Sub Attribute', 'required');
        if ($this->form_validation->run()) {
            $data = $this->get_data_from_post();
            if ($this->Mdl_sub_attributes->_update($id, $data)) {
				$data['subAttribute_id'] = $this->db->insert_id();
                return $this->render->json(['data' => $data], 201);
				exit(0);
            }

        }
		return $this->render->json(['message' => 'All fields are required'], 400);
		exit(0);
    }

    public function single( int $id )
	{
		$item = $this->Mdl_sub_attributes->get_where($id);
		return $this->render->json(['data'=>$item->row()], 200);
		exit(0);
	}

    public function deactivate( int $id )
    {
        $item = $this->Mdl_sub_attributes->_update($id, ['is_active' => 0]);
        if ($item)
        {
            return $this->render->json(['message'=>'item deactivated'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to deactivate'], 404);
        exit(0);
    }

    public function activate( int $id )
    {
        $item = $this->Mdl_sub_attributes->_update($id, ['is_active' => 1]);
        if ($item)
        {
            return $this->render->json(['message'=>'item activated'], 200);
            exit(0);
        }

        return $this->render->json(['message' => 'no item to activate'], 404);
        exit(0);
    }

    public function trash( int $id )
	{
		$item = $this->Mdl_sub_attributes->_update($id, ['is_deleted' => 1]);
		if ($item)
		{
			return $this->render->json(['message'=>'item trashed'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to trash'], 404);
		exit(0);
	}

	public function restore( int $id )
	{
		$item = $this->Mdl_sub_attributes->_update($id, ['is_deleted' => 0]);
		if ($item)
		{
			return $this->render->json(['message'=>'item restored'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to restore'], 404);
		exit(0);
	}

	public function delete( int $id )
	{
		$item = $this->Mdl_sub_attributes->_delete($id);
		if ($item)
		{
			return $this->render->json(['message'=>'item deleted'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to delete'], 204);
		exit(0);
	}

	public function get_json()
	{
		$item = $this->Mdl_sub_attributes->get_where_custom(['sub_attributes.is_deleted' => 0]);
		return $this->render->json(['data'=>$item->result()], 200);
		exit(0);
	}


	public function get_data_from_post()
	{
		$data['sub_attribute'] = $this->input->post('sub_attribute');
		$data['attribute_id'] = $this->input->post('attribute_id');
		$data['discount'] = $this->input->post('discount');
		$data['price'] = $this->input->post('price');
		$data['is_active'] = $this->input->post('is_active');
		$data['attribute_view'] = $this->input->post('attribute_view');
		$data['attribute_value'] = $this->input->post('attribute_value');
		$data['pocket_side_display'] = $this->input->post('pocket_side_display');
		return $data;
	}


}
