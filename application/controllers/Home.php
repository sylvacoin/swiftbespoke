<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('render');
		$this->load->library('menus');
		$this->load->library('debugger');
	}

	public function index() {
		$this->render->view('home/index', 'Homepage');
	}

	public function about() {
		$this->render->view('home/about', 'About', null);
	}

	public function contact() {
		$this->render->view('home/contact', 'Contact us', null);
	}

	public function faq() {
		$this->render->view('home/faq', 'Frequently Asked Questions', null);
	}

	public function gurantee() {
		$this->render->view('home/our-guarantee', 'Our guarantee', null);
	}

	public function reviews() {
		$this->render->view('home/reviews', 'Reviews', null);
	}

	public function test() {
		$data = $this->menus->all();
		$this->render->view('home/test', 'Demo', ['menu' => $data]);
	}

}
