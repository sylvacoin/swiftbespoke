<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('Mdl_users','mdl_users');
    $this->load->model('Mdl_user_addresses','mdl_user_addresses');
    $this->load->library('debugger');
  }

  public function index()
  {
      // show all the users...
      $data = $this->mdl_users->get_where_custom('user_group_id', 2);
      $this->render->view('users/index', 'Users', ['data' => $data], 'backend');
  }

  public function signup()
  {
      //TODO: GET default user group and pass it in place of 2
      $this->render->view('users/signup', 'Signup', ['default_user' => 2], 'frontend');
  }

  public function ajaxSignup()
  {
      $data = $this->get_userdata_from_json();
      $address = $this->get_useraddress_from_json();
      $this->db->trans_start();
      $result = $this->mdl_users->_insert($data);
      if ( $result )
      {
          $id = $this->db->insert_id();
          $address['user_id'] = $id;
          $res = $this->mdl_user_addresses->_insert($address);
          if ( $res )
          {
              $this->db->trans_complete();

              //send mail using mail template from DB check mailer library.
              $this->load->library('Mailer', array(
                  'to' => $data['email'],
                  'customer_name' => $data['firstname'].' '.$data['lastname'],
                  'activation_link' => base_url('account/activate/'.$data['token']),
                  'mail_option' => 'activate_account'
              ));
              $this->mailer->send_mail();
              $this->render->json(['user'=> $data], 201);
              return;
          }
      }
      $this->render->json(['user'=> ''], 500);
      return;
  }

  private function get_userdata_from_json()
  {
      $data = [];
      $userdata = json_decode(file_get_contents('php://input'));
      if ( $userdata )
      {
          $data['firstname'] = $userdata->firstname;
          $data['lastname'] = $userdata->lastname;
          $data['email'] = $userdata->email;
          $data['phone'] = $userdata->phone;
          $data['password'] = md5($userdata->password.SALT);
          $data['user_group_id'] = $userdata->group;
          $data['token'] = md5($userdata->email.uniqid());
      }
      return $data;
  }

  private function get_useraddress_from_json()
  {
      $data = [];
      $userdata = json_decode(file_get_contents('php://input'));
      if ( $userdata )
      {
          $data['country_id'] = $userdata->country;
          $data['state_id'] = $userdata->state;
          $data['city'] = $userdata->city;
          $data['zip'] = $userdata->zip;
          $data['address1'] = $userdata->address1;
          $data['address2'] = $userdata->address2;
          $data['closest_landmark'] = isset($userdata->landmark)?$userdata->landmark:'';
      }
      return $data;
  }


  public function administrators()
  {
      // show all the users
      $data = $this->mdl_users->get_where_custom('user_group_id', 1);
      $this->render->view('users/index', 'Administrators', ['data' => $data], 'backend');
  }

  public function appointments()
  {
      // show all pending appointments
      $data = $this->mdl_users->get_where_custom('user_group_id', 1);
      $this->render->view('users/index', 'Scheduled Appointments', ['data' => $data], 'backend');
  }

  public function details( $id )
  {
      // show user profile...
      $data = $this->mdl_users->get_where($id)->row();
      $this->render->view('users/details', 'User Profile', ['user' => $data], 'backend');
  }

  public function flag( $id )
  {
      // show user profile...
      $data = $this->mdl_users->get_where($id)->row();
      $this->mdl_users->_update($data->user_id, ['is_flagged' => true]);
     redirect('users/');
  }

  public function unflag( $id )
  {
      // show user profile...
      $data = $this->mdl_users->get_where($id)->row();
      $this->mdl_users->_update($data->user_id, ['is_flagged' => false]);

      redirect('users/');
  }

  public function delete( $id )
  {
      // show user profile...
      $data = $this->mdl_users->get_where($id)->row();
      $this->mdl_users->_delete($data->user_id);
      redirect('users/');
  }
}
