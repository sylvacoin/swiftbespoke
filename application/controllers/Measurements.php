<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Measurements extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mdl_measurements', 'mdl_measurements');
  }

  public function index()
  {
      $data = $this->mdl_measurements->get_where_custom(['measurements.is_deleted'=> '0']);
      $this->render->view('measurements/index', 'Measurement List', ['data' => $data], 'backend');
  }

  public function create()
  {
      $this->render->view('measurements/create', 'Create measurement', null, 'backend');
  }

  public function post_measurement()
  {
      $this->load->helper('inflector');
      //$result = json_decode( file_get_contents('php://input') );
      $this->form_validation->set_rules('measurement_type_id', 'Measurement type', 'required');
      $this->form_validation->set_rules('category_id', 'Category', 'required');
      $this->form_validation->set_rules('unit_id', 'Unit', 'required');
      $this->form_validation->set_rules('max_range', 'Max range', 'required');
      $this->form_validation->set_rules('min_range', 'Min range', 'required');

      if ($this->form_validation->run()) {
          $data = $this->get_data_from_post();

          if ($this->mdl_measurements->_insert($data)) {
              $data['measurement_id'] = $this->db->insert_id();
              return $this->render->json(['data' => $data], 201);
              exit(0);
          }

      }
      return $this->render->json(['message' => 'All fields are required'], 400);
      exit(0);
  }

  public function put_measurement($id)
  {
      $this->load->helper('inflector');

      $this->form_validation->set_rules('measurement', 'Measurement', 'required');

      if ($this->form_validation->run()) {
          $data = $this->get_data_from_post();

          if ($this->mdl_measurements->_update($id, $data)) {
              return $this->render->json(['data' => $data], 200);
              exit(0);
          }
      }
      return $this->render->json(['message' => 'All fields are required'], 400);
      exit(0);
  }

  public function single( int $id )
  {
      $item = $this->mdl_measurements->get_where($id);
      return $this->render->json(['data'=>$item->row()], 200);
      exit(0);
  }

  public function get_json()
  {
      $item = $this->mdl_measurements->get();
      return $this->render->json(['data'=>$item->result()], 200);
      exit(0);
  }


  public function deactivate( int $id )
  {
      $item = $this->mdl_measurements->_update($id, ['is_active' => 0]);
      if ($item)
      {
          return $this->render->json(['message'=>'item deactivated'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to deactivate'], 404);
      exit(0);
  }

  public function activate( int $id )
  {
      $item = $this->mdl_measurements->_update($id, ['is_active' => 1]);
      if ($item)
      {
          return $this->render->json(['message'=>'item activated'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to activate'], 404);
      exit(0);
  }

  public function trashed()
  {
      $data = $this->mdl_measurements->get_where_custom(['measurements.is_deleted'=> '1']);
      $this->render->view('measurements/trashed', 'Measurement Trash', ['data' => $data], 'backend');
  }


  public function trash( int $id )
  {
      $item = $this->mdl_measurements->_update($id, ['is_deleted' => 1]);
      if ($item)
      {
          return $this->render->json(['message'=>'item trashed'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to trash'], 404);
      exit(0);
  }

  public function restore( int $id )
  {
      $item = $this->mdl_measurements->_update($id, ['is_deleted' => 0]);
      if ($item)
      {
          return $this->render->json(['message'=>'item restored'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to restore'], 404);
      exit(0);
  }

  public function delete( int $id )
  {
      $item = $this->mdl_measurements->_delete($id);
      if ($item)
      {
          return $this->render->json(['message'=>'item deleted'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to delete'], 204);
      exit(0);
  }

  public function get_measurements()
  {
      $result = $this->mdl_measurements->get_where_custom('measurements.is_deleted', 0);
      $this->render->json(['data'=>$result->result()], 200);
  }

  public function get_data_from_post()
  {
      $data['measurement_type_id'] = $this->input->post('measurement_type_id');
      $data['category_id'] = $this->input->post('category_id');
      $data['unit_id'] = $this->input->post('unit_id');
      $data['is_active'] = $this->input->post('is_active');
      $data['max_range'] = $this->input->post('max_range');
      $data['min_range'] = $this->input->post('min_range');
      $data['video_url'] = $this->input->post('video_url');
      return $data;
  }
}
