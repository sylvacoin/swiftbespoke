<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MeasurementTypes extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mdl_measurement_types', 'mdl_measurement_types');
  }

  public function index()
  {
      $data = $this->mdl_measurement_types->get_where_custom(['measurement_types.is_deleted'=> '0']);
      $this->render->view('measurement_types/index', 'Measurement type List', ['data' => $data], 'backend');
  }

  public function create()
  {
      $this->render->view('measurement_types/create', 'Create measurement_type', null, 'backend');
  }

  public function post_measurement_type()
  {
      $this->load->helper('inflector');
      //$result = json_decode( file_get_contents('php://input') );
      $this->form_validation->set_rules('measurement_type', 'Measurement type', 'required');

      if ($this->form_validation->run()) {
          $data = $this->get_data_from_post();

          if ($this->mdl_measurement_types->_insert($data)) {
              $data['measurement_type_id'] = $this->db->insert_id();
              return $this->render->json(['data' => $data], 201);
              exit(0);
          }

      }
      return $this->render->json(['message' => 'All fields are required'], 400);
      exit(0);
  }

  public function put_measurement_type($id)
  {
      $this->load->helper('inflector');

      $this->form_validation->set_rules('measurement_type', 'Measurement type', 'required');

      if ($this->form_validation->run()) {
          $data = $this->get_data_from_post();

          if ($this->mdl_measurement_types->_update($id, $data)) {
              return $this->render->json(['data' => $data], 200);
              exit(0);
          }
      }
      return $this->render->json(['message' => 'All fields are required'], 400);
      exit(0);
  }

  public function single( int $id )
  {
      $item = $this->mdl_measurement_types->get_where($id);
      return $this->render->json(['data'=>$item->row()], 200);
      exit(0);
  }

  public function get_json()
  {
      $item = $this->mdl_measurement_types->get();
      return $this->render->json(['data'=>$item->result()], 200);
      exit(0);
  }


  public function deactivate( int $id )
  {
      $item = $this->mdl_measurement_types->_update($id, ['is_active' => 0]);
      if ($item)
      {
          return $this->render->json(['message'=>'item deactivated'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to deactivate'], 404);
      exit(0);
  }

  public function activate( int $id )
  {
      $item = $this->mdl_measurement_types->_update($id, ['is_active' => 1]);
      if ($item)
      {
          return $this->render->json(['message'=>'item activated'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to activate'], 404);
      exit(0);
  }

  public function trashed()
  {
      $data = $this->mdl_measurement_types->get_where_custom(['measurement_types.is_deleted'=> '1']);
      $this->render->view('measurement_types/trashed', 'Measurement type Trash', ['data' => $data], 'backend');
  }


  public function trash( int $id )
  {
      $item = $this->mdl_measurement_types->_update($id, ['is_deleted' => 1]);
      if ($item)
      {
          return $this->render->json(['message'=>'item trashed'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to trash'], 404);
      exit(0);
  }

  public function restore( int $id )
  {
      $item = $this->mdl_measurement_types->_update($id, ['is_deleted' => 0]);
      if ($item)
      {
          return $this->render->json(['message'=>'item restored'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to restore'], 404);
      exit(0);
  }

  public function delete( int $id )
  {
      $item = $this->mdl_measurement_types->_delete($id);
      if ($item)
      {
          return $this->render->json(['message'=>'item deleted'], 200);
          exit(0);
      }

      return $this->render->json(['message' => 'no item to delete'], 204);
      exit(0);
  }

  public function get_measurement_types()
  {
      $result = $this->mdl_measurement_types->get_where_custom('measurement_types.is_deleted', 0);
      $this->render->json(['data'=>$result->result()], 200);
  }

  public function get_data_from_post()
  {
      $data['measurement_type'] = $this->input->post('measurement_type');
      $data['is_active'] = $this->input->post('is_active');

      return $data;
  }
}
