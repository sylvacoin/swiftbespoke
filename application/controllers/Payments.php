<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//Do your magic here
		$this->load->library('debugger');
		$this->load->model('Mdl_payments', 'mdl_payments');

	}


	public function payments()
	{
		$data = $this->mdl_payments->get_where_custom(['is_active'=> '1']);
        $this->render->view('payments/payments', 'Categories', ['payments' => $data->result()]);
	}

	//==========================================================================

    public function index()
    {
		$data = $this->mdl_payments->get_where_custom(['is_deleted'=> '0']);
        $this->render->view('payments/index', 'Category List', ['data' => $data], 'backend');
    }

	public function create()
	{
		$this->render->view('payments/create', 'Create payment', null, 'backend');
	}

	public function post_payment()
	{
		$this->load->helper('inflector');
		$this->load->library('uploader');
		//$result = json_decode( file_get_contents('php://input') );
		$this->form_validation->set_rules('payment', 'Category', 'required');
		$this->form_validation->set_rules('gender', 'gender', 'required');

        if (!isset($_FILES['image'])) {
            $this->form_validation->set_rules('image', 'Display Image', 'required');
    	}

		if (!isset($_FILES['front']) ){
			$this->form_validation->set_rules('front', 'Front preview', 'required');
		}

		if (!isset($_FILES['back']) ){
			$this->form_validation->set_rules('back', 'Back preview', 'required');
		}


        if ($this->form_validation->run()) {
            $data = $this->get_data_from_post();

			$data['display_image'] = $this->uploader->upload('image');
			$data['front_preview'] = $this->uploader->upload('front');
			$data['back_preview'] = $this->uploader->upload('back');

			$data['slug'] = underscore( strtolower($data['payment']) );

            if ($this->mdl_payments->_insert($data)) {
				$data['payment_id'] = $this->db->insert_id();
                return $this->render->json(['data' => $data], 201);
				exit(0);
            }

        }
		return $this->render->json(['message' => 'All fields are required'], 400);
		exit(0);
	}

	public function put_payment($id)
	{
		$this->load->helper('inflector');
		$this->load->library('uploader');

		$this->form_validation->set_rules('payment', 'Category', 'required');

        if ($this->form_validation->run()) {
			$data = $this->get_data_from_post();

			if (isset($_FILES['image']) ){
				$data['display_image'] = $this->uploader->upload('image');
	        }
			if (isset($_FILES['front']) ){
				$data['front_preview'] = $this->uploader->upload('front');
	        }
			if (isset($_FILES['back']) ){
				$data['back_preview'] = $this->uploader->upload('back');
	        }

			$data['slug'] = underscore( strtolower($data['payment']) );
            if ($this->mdl_payments->_update($id, $data)) {
                return $this->render->json(['data' => $data], 200);
				exit(0);
            }
        }
		return $this->render->json(['message' => 'All fields are required'], 400);
		exit(0);
	}

	public function single( int $id )
	{
		$item = $this->mdl_payments->get_where($id);
		return $this->render->json(['data'=>$item->row()], 200);
		exit(0);
	}

	public function get_json()
	{
		$item = $this->mdl_payments->get_where_custom(['is_active' => 1]);
		$this->render->json(['data'=>$item->result()], 200);
		return;
	}


	public function deactivate( int $id )
	{
		$item = $this->mdl_payments->_update($id, ['is_active' => 0]);
		if ($item)
		{
			return $this->render->json(['message'=>'item deactivated'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to deactivate'], 404);
		exit(0);
	}

	public function activate( int $id )
	{
		$item = $this->mdl_payments->_update($id, ['is_active' => 1]);
		if ($item)
		{
			return $this->render->json(['message'=>'item activated'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to activate'], 404);
		exit(0);
	}

	public function unfeature( int $id )
	{
		$item = $this->mdl_payments->_update($id, ['is_featured' => 0]);
		if ($item)
		{
			return $this->render->json(['message'=>'item unfeatured'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to unfeature'], 404);
		exit(0);
	}

	public function feature( int $id )
	{
		if ($this->mdl_payments->_update($id, ['is_featured' => 1]))
		{
			return $this->render->json(['message'=>'item featured'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to feature'], 404);
		exit(0);
	}

	public function trash( int $id )
	{
		$item = $this->mdl_payments->_update($id, ['is_deleted' => 1]);
		if ($item)
		{
			return $this->render->json(['message'=>'item trashed'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to trash'], 404);
		exit(0);
	}

	public function trashed()
	{
		$data = $this->mdl_payments->get_where_custom(['is_deleted'=> 1]);
        $this->render->view('payments/trashed', 'Trashed Categories', ['data' => $data], 'backend');
	}

	public function restore( int $id )
	{
		$item = $this->mdl_payments->_update($id, ['is_deleted' => 0]);
		if ($item)
		{
			return $this->render->json(['message'=>'item restored'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to restore'], 404);
		exit(0);
	}

	public function delete( int $id )
	{
		$item = $this->mdl_payments->_delete($id);
		if ($item)
		{
			return $this->render->json(['message'=>'item deleted'], 200);
			exit(0);
		}

		return $this->render->json(['message' => 'no item to delete'], 204);
		exit(0);
	}

	public function get_payments()
	{
		$result = $this->mdl_payments->get_where_custom(['is_active' => 1]);
		return $this->render->json(['data'=>$result->result()], 200);
		exit(0);
	}

	public function get_data_from_post()
	{
		$data['payment'] = $this->input->post('payment');
		$data['is_active'] = $this->input->post('is_active');
		$data['is_featured'] = $this->input->post('is_featured');
		$data['gender'] = $this->input->post('gender');
		return $data;
	}
}
